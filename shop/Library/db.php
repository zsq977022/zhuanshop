<?php
 /*----------------------------------------
    
       数据库操作类 底层用PHP内置的mysqli扩展
       @author:sunqi
       email:sunqi_925@163.com


   ----------------------------------------
 */
class DB{
     private static $link=null;

  /*-------------------------
      @params  $sql   原生SQL语句
      @return 返回多维数组结构

    -------------------------- 
  */
	 public static function  select($sql){

              self::connect();
              $result=mysqli_query(self::$link,$sql);
              $selectArr=array();
              while($row=mysqli_fetch_assoc($result)){
                  $selectArr[]=$row;
              }
              return $selectArr;


	 }
    
   /*-------------------------
      @params  $sql   原生SQL语句  sql语句加limit 1 性能更优
      @return 只返回一条记录  

    -------------------------- 
  */
	 public static function find($sql){
	 	            self::connect();
                $result=mysqli_query(self::$link,$sql);
                $dataArr=mysqli_fetch_assoc($result);
                return $dataArr;
	 }
  
   /*-------------------------
      连接数据库
     
    -------------------------- 
  */
	 public static function connect(){
               
	 	       if(!isset(self::$link)){   
                   $DB = require CONF_PATH.'/db.php';
                  
                  self::$link = mysqli_connect($DB['DB_HOST'],$DB['DB_USER'],$DB['DB_PWD'],$DB['DB_NAME']);
                  if(mysqli_connect_errno()){ throw new Exception("链接数据库出错");}
                     mysqli_query(self::$link,'set names utf8');
             }
	 }


    /*-------------------------
         添加数据  $sql "insert into "
         @return 插入的记录id
    -------------------------- 
  */
   public static function insert($sql){
                self::connect();
                if(mysqli_query(self::$link,$sql)==true){
                    $result=mysqli_insert_id(self::$link);
                }else{
                    $result=false;
                }
                return $result;
   }

    /*-------------------------
         更新数据
         @return 插入的记录id
    -------------------------- 
  */
   public static function update($sql){
                self::connect();
                 mysqli_query(self::$link,$sql);
                 $row=mysqli_affected_rows(self::$link);
                if($row){
                     return  $row;
               }else{
                    return false;
               }
   }
   
    /*-------------------------
          sql查询  mysqli_query($sql);
         @return 插入的记录id
    -------------------------- 
  */
   public static function  query($sql){
            self::connect();
            return mysqli_query(self::$link,$sql);
           

   }



}


    


             
            
               
           
            




               
            
             
         
             
   

             
               

               


   

