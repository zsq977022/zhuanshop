<?php
/**
 * ---------------------------------------
 *
 *    Memcache缓存配置
 *    author:sunqi
 *    emil:sunqi_925@163.com
 *
 *
 * ----------------------------------------
 */

return array(
	   'MEMCACHE_HOST' => '127.0.0.1',     //memcached server 地址
       // 'MEMCACHE_HOST' => '123.103.79.106',  
       'MEMCACHE_PORT' => 11211,        //端口
       'MEMCACHE_PREFIX' =>'shop',      //key前缀
       'MEMCACHE_EXPIRE' => 60*60          //缓存时间 目前设为一个小时
	  );