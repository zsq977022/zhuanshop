<?php
/**
 * ---------------------------------------
 *
 *    前台的数据接口的入口文件
 *    author:sunqi
 *    emil:sunqi_925@163.com
 *
 *
 * ----------------------------------------
 */
header('content-type:text/html;charset=utf-8');
date_default_timezone_set("Asia/Shanghai");  //定义时区

//线上关闭错误输出 ，本地调试时输出错误信息
if($_SERVER['SERVER_ADDR'] != '127.0.0.1'){
     ini_set("display_errors",0);
     error_reporting(0);
}
error_reporting(E_ALL);
//定义系统常量
define('APP_PATH',__DIR__);   //应用程序根目录
define('CONF_PATH',APP_PATH.'/Config');     //配置目录
define('VENDOR_PATH',APP_PATH.'/Vendor');    //第三方类库目录
define('MEMCACHE_START',false);         //memcache缓存开关
define('LOG_PATH',APP_PATH.'/Logs');  //支付错误记录日志目录
define('KEY_PATH',APP_PATH.'/Key');    // key目录

 

/*
--------注册类自动加载器---------
*/
spl_autoload_register(function($className){
           require_once  './Api/'.$className.'.php';
});

require_once APP_PATH.'/Library/db.php';     //加载数据库类
require_once APP_PATH.'/Common/functions.php';  //加载公用函数





/*--------组装路由地址---------------------
      http://hostname/bootstrap.php.php?s=/controller/action/varName/varValue/......
*/

if(isset($_GET['s'])){  
	  $tmpArr=explode('/',trim($_GET['s'],'/'));
	  $controller=ucfirst(strtolower($tmpArr[0])); //控制器
	  $action=$tmpArr[1]; 
	  array_shift($tmpArr);
	  array_shift($tmpArr);
	  $params=array();
	  preg_replace_callback('/(\w+)\/(\w+)/',function($match) use(&$params){$params[$match[1]]=$match[2];},implode('/',$tmpArr));
	  $_GET=array_merge($_GET,$params);

//过滤器
function filter($value){
	    return addslashes($value);
}
if(!get_magic_quotes_gpc()){
	    if(!empty($_POST)){
	    	   $_POST=array_map('filter',$_POST);
	    }
	    if(!empty($_GET)){
	    	   $_GET=array_map('filter',$_GET);
	    }
}

//实例化
$obj = new $controller;
$obj->$action();
}	

	  







