<?php 
/*
comment:订单模型(关联3张表)order_finally_info最终订单表  order_check_info未审核订单表
date:2015-6-30
author:hanyu
*/
namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

class Order extends Model {
	
	public $timestamps = false;
	
	//生成订单(init表)
	public function create_Order($data,$table='order_init_info') {
		$this->table = $table;
		if($id = $this->insertGetId($data)) {
			return $id;
		} else {
			return false;
		}
	}
	
	//插入邮寄信息
	public function create_mail_order($data) {
		$this->table = 'order_mail_info';
		if($this->insert($data)) {
			return true;
		} else {
			return false;
		}	
	}
	
	//删除指定表里的订单信息
	public function delete_Order($id,$table = 'order_init_info') {
		$this->table = $table;
		$res = $this->where('order_id','=',$id)
					->delete();
		return $res;
	}
	
	//查询一个订单信息(包含用户信息)
	public function order_info($id,$table) {
		$this->table = $table;
		$res = $this->where($table.'.order_id','=',$id)
					->first();
		if(!empty($res)) {
			return $res->toArray();
		} else {
			return false;	
		}
	}
	
	//查询邮寄信息
	public function mail_info($id) {
		$this->table = 'order_mail_info';	
		$res = $this->where($this->table.'.order_id','=',$id)
					->leftJoin('company_info as i', $this->table.'.company_id', '=', 'i.company_id')
					->first();
		if(!empty($res)) {
			return $res->toArray();
		} else {
			return false;	
		}
	}
	
	//查询一个非删除订单信息(不包含用户信息)
	public function order_detail($id,$table = 'order_init_info',$field = '') {
		$this->table = $table;
		
		if(empty($field)) {
		$field = array('order_id','order_number','is_give','snap_ids','username','user_id','order_explain','orders_time','amount','pattern','is_system','form');
		}
		$res = $this->select($field)
					->where('order_id','=',$id)
					->first();
		if(!empty($res)) {
			return $res->toArray();
		} else {
			return false;	
		}
	}
	
	//将订单移入回收站
	public function order_recycle($data) {
		$this->table = 'order_recycle_info';
		if($this->insert($data)) {
			return true;
		} else {
			return false;
		}	
	}
	
	//将订单移入回收站
	public function order_move($data,$table) {
		$this->table = $table;
		if($this->insert($data)) {
			return true;
		} else {
			return false;
		}	
	}
	
	//查询一个订单的邮寄状态
	public function mail_status($id) {
		$this->table = 'order_mail_info';//order_finally_info
		$res = $this->where('order_id','=',$id)
					->first();
		if(!empty($res)) {
			return $res->toArray();
		} else {
			return false;	
		}	
	}
	
	//过滤多个订单号里已邮寄的订单
	public function filter_mails($order_nums) {
		$this->table = 'order_mail_info as m';
		$res = $this->select('f.order_number','f.order_id')
					->leftJoin('order_finally_info as f', 'm.order_id', '=', 'f.order_id')
					->whereIn('f.order_number',$order_nums)
					->where('m.mail_status','=','0')
					->get();
		if(!empty($res)) {
			$data = array();
			foreach($res as $v) {
				$data[$v['order_id']] = $v['order_number'];	
			}
			return $data;
		} else {
			return false;	
		}	
	}
	
	//修改邮寄订单
	public function edit_mail_order($order_id,$data) {
		$this->table = 'order_mail_info';
		$res = $this->where('order_id','=',$order_id)
					->update($data);
		return $res;	
	}
	
	//修改订单
	public function edit_Order($order_id,$data) {
		$this->table = 'order_init_info';
		$res = $this->where('order_id','=',$order_id)
					->update($data);
		return $res;	
	}
	
	//删除指定表里的订单信息
	public function delete_mail_order($id) {
		$this->table = 'order_mail_info';
		$res = $this->where('order_id','=',$id)
					->delete();
		return $res;
	}

	//还原邮寄订单的邮寄信息
	public function reset_mail_info($ids) {
		$data = array(
			'company_id'=>0,
			'company_num'=>'',
			'mail_status'=>0,
		);
		$this->table = 'order_mail_info';
		$res = $this->whereIn('order_id',$ids)
					->update($data);
		return $res;
	}

	//修改邮寄订单的邮寄信息
	public function update_mail_info($data) {
		$this->table = 'order_mail_info';
		$id = (int)$data['order_id'];
		$data['mail_status'] = 1;
		$res = $this->where('order_id','=',$id)
					->update($data);
		return $res;
	}

	//查询邮寄订单信息
	public function one_mail_info($id) {
		$this->table = 'order_mail_info as m';
		$res = $this->leftJoin('order_finally_info as f', 'm.order_id', '=', 'f.order_id')
					->where('m.order_id','=',$id)
        			->first();

        if(!empty($res)) {
        	 return $res->toArray();
        } else {
        	return false;
        }
	}
	
	//历史总收入
	public function total_amount() {
		$this->table = 'order_finally_info';
		$sum = $this->sum('amount');
		
		return $sum;
	}
	
	//今日收入
	public function today_amount() {
		$today=strtotime("today");
		$this->table = 'order_finally_info';
		$sum = $this->where('orders_time','>=',$today)
					->where('orders_time','<',$today+86400)
					->sum('amount');
		
		return $sum;
	}
	
	//自增或者自减购买人数
	public function update_goodsNum($order_id,$num,$action = 'inc',$table) {
		$this->table = $table;
		$res = $this->select('snap_ids')
					->where('order_id','=',$order_id)
					->first('snap_ids');//['snap_ids']
					
		if(!empty($res['snap_ids'])) {
			$snap_ids = json_decode($res['snap_ids'],true);
		
			$goods_ids = DB::table('snap_info')->whereIn('snap_id',$snap_ids)
									->lists('goods_id');
			
			if($action == 'inc') {
				$res = DB::table('goods')->whereIn('goods_id',$goods_ids)->increment('goods_num',$num);
			} else if($action == 'dec') {
				$res = DB::table('goods')->whereIn('goods_id',$goods_ids)->decrement('goods_num',$num);
			}
		
			return $res;
		} else {
			return false;	
		}
	}
}