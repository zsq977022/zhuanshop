<?php 
/*
comment:物流公司模型
date:2015-7-21
author:hanyu
*/
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class Company extends Model {
	protected $table = 'company_info';

	//获取物流公司列表
	public function Company_list() {
		$res = $this->where('status',1)
					->lists('company_name','company_id');

		if(!empty($res)) {
			return $res;
		} else {
			return false;
		}
	}
}