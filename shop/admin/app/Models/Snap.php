<?php 
/*
comment:快照模型
date:2015-6-30
author:hanyu
*/
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class Snap extends Model {
	protected $table = 'snap_info';
	
	public $timestamps = false;
	
	//生成快照
	public function create_snap($data,$goods_id) {
		$_data = array(
			'snap_info'=>base64_encode(gzcompress(json_encode($data),9)),
			'goods_id'=>$goods_id
		);
		
		if($id = $this->insertGetId($_data)) {
			return $id;
		} else {
			return false;
		}
	}
	
	//删除快照(多个)
	public function delete_snaps($ids) {
		
		$res = $this->whereIn('snap_id',$ids)
					->delete();
		return $res;	
	}
	
	//查询快照(多个
	public function snaps_infos($ids) {
		$ids_ordered = implode(',', $ids);
		$res = $this->whereIn('snap_id',$ids)
					->orderByRaw(DB::raw("FIELD(snap_id, $ids_ordered)"))
					->get();
		if(!empty($res)) {
			return $res->toArray();	
		} else {
			return false;	
		}
	}
	
	//查询多个快照所对因的商品id
	public function snaps_goods($ids) {
		$res = $this->whereIn('snap_id',$ids)
					->lists('goods_id');
		if(!empty($res)) {
			return array_unique($res);
		} else {
			return false;	
		}	
	}
}