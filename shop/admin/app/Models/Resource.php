<?php 
/*
comment:资源模型
date:2015-6-30
author:hanyu
*/
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Resource extends Model {
	
	protected $table = 'resource';
	
	public $timestamps = false;
	
	//获取资源列表
	public function resource_list($start) {
		$where = array(
			'tombstone'=>0
		);
		$res = $this->select('author','resource_id','name','startime','endtime','creatime','resource_type')
					->where($where)
					->orderBy('resource_id','DESC')
					->skip($start)
					->take(config('params.pagesize'))
					->get();
		
		if($res) {
			return $res->toArray();
		} else {
			return false;	
		}
	}
	
	//根据id集合,查询资源
	public function get_resources($ids) {
		$where = array(
			'tombstone'=>0
		);
		$res = $this->select('author','resource_id','name','startime','endtime','creatime','resource_type')
					->where($where)
					->whereIn('resource_id',$ids)
					->get();
		if($res) {
			return $res->toArray();	
		} else {
			return '';	
		}
	}
	
	//获取搜索资源列表
	public function search_resource_list($start,$data) {
		$where = array(
			'tombstone'=> 0
		);
		
		$res = $this->select('author','resource_id','name','startime','endtime','creatime','resource_type','status')
					->Where(function ($query) use ($data){
						if(isset($data['resource_id']) && !empty($data['resource_id'])) {
							$query->where('resource_id','=',$data['resource_id']);	
						}
					})
					->Where(function ($query) use ($data){
						if(isset($data['name']) && !empty($data['name'])) {
							$query->where('name','like','%'.$data['name'].'%');	
						}
					})
					->Where(function ($query) use ($data){
						if(isset($data['resource_type']) && !empty($data['resource_type'])) {
							$query->where('resource_type','=',$data['resource_type']);	
						}
					})
					->where($where)
					->orderBy('resource_id','DESC')
					->skip($start)
					->take(config('params.pagesize'))
					->get();
		
		if($res) {
			return $res->toArray();	
		} else {
			return '';	
		}
	}
	
	//获取搜索资源总数
	public function search_resource_count($data) {
		$where = array(
			'tombstone'=> 0
		);
		
		$count = $this->Where(function ($query) use ($data){
						if(isset($data['resource_id']) && !empty($data['resource_id'])) {
							$query->where('resource_id','=',$data['resource_id']);	
						}
					})
					->Where(function ($query) use ($data){
						if(isset($data['name']) && !empty($data['name'])) {
							$query->where('name','like','%'.$data['name'].'%');	
						}
					})
					->Where(function ($query) use ($data){
						if(isset($data['resource_type']) && !empty($data['resource_type'])) {
							$query->where('resource_type','=',$data['resource_type']);	
						}
					})
					->where($where)
					->count();
		
		return $count;
	}
	
	
	//排除掉id集合后的资源列表
	public function set_resource_list($start,$data) {
		$where = array(
			'tombstone'=>0,
			'status'=>1,
		);
		if(!isset($data['ids'])) $data['ids'] = '';
		$res = $this->select('author','resource_id','name','startime','endtime','creatime','resource_type')
					->Where(function ($query) use ($data){
						if(isset($data['resource_id']) && !empty($data['resource_id'])) {
							$query->where('resource_id','=',$data['resource_id']);	
						}
					})
					->Where(function ($query) use ($data){
						if(isset($data['name']) && !empty($data['name'])) {
							$query->where('name','like','%'.$data['name'].'%');	
						}
					})
					->Where(function ($query) use ($data){
						if(isset($data['ids']) && !empty($data['ids'])) {
							$query->whereNotIn('resource_id', $data['ids']);
						}
					})
					->where($where)
					->orderBy('resource_id','DESC')
					->skip($start)
					->take(config('params.pagesize'))
					->get();
					
		if($res) {
			return $res->toArray();
		} else {
			return false;	
		}	
	}
	
	//排除掉id集合后资源总数
	public function set_resource_count($data) {
		$where = array(
			'tombstone'=>0,
			'status'=>1,
		);
		if(!isset($data['ids'])) $data['ids'] = '';
		$res = $this->Where(function ($query) use ($data){
						if(isset($data['resource_id']) && !empty($data['resource_id'])) {
							$query->where('resource_id','=',$data['resource_id']);	
						}
					})
					->Where(function ($query) use ($data){
						if(isset($data['name']) && !empty($data['name'])) {
							$query->where('name','like','%'.$data['name'].'%');	
						}
					})
					->Where(function ($query) use ($data){
						if(isset($data['ids']) && !empty($data['ids'])) {
							$query->whereNotIn('resource_id', $data['ids']);
						}
					})
					->where($where)
					->count();
		
		return $res;	
	}
	
	
	
	//获取列表总数
	public function resource_count() {
		$where = array(
			'tombstone'=>0
		);
		$count = $this->where($where)
						->count();
		
		return $count;	
	}
	
	//获取资源类型信息
	public function resource_types() {
		$this->table = 'resource_type';
		
		$res = $this->where('status',1)
					->get();
					
		if($res) {
			return $res->toArray();	
		} else {
			return false;	
		}
	}

	//获取资源类型信息(map结构    id=>name)
	public function map_types() {
		$this->table = 'resource_type';
		
		$res = $this->where('status',1)
					->lists('name','id');
		return $res;
	}

	//获取一条资源
	public function get_resource_info($id) {
		$res = $this->where('resource_id',$id)
					->where('tombstone',0)
					->first();

		if($res) {
			//解密属性JSON
			$res = $res->toArray();
			@$res['attrs_list'] = json_decode(gzuncompress(base64_decode($res['attrs_list'])),true);
			return $res;
		} else {
			return false;
		}
	}
	
	//删除资源
	public function delete_resource($id) {
		$res = $this->where('resource_id','=',$id)
					->update(array('tombstone'=>1));
		return $res;		
	}
	
	//审核资源
	public function check_resource($id) {
		$res = $this->where('resource_id','=',$id)
					->update(array('status'=>1));
		return $res;		
	}

	//取消审核资源
	public function repeal_resource($id) {
		$res = $this->where('resource_id','=',$id)
					->update(array('status'=>0));
		return $res;
	}

	//添加资源
	public function create_resource($data,$uid) {

		$_data = array(
			'name'=>trim($data['resource_name']),
			'cover_url'=>(!isset($data['cover_url'])) ? '' : trim($data['cover_url']),
			'resource_type'=>(int)$data['type'],
			'url'=>$data['resource_url'],
			'resource_token'=>$data['token'],
			'download_url'=>$data['download_url'],
			'author'=>$data['author'],
			'creatime'=>time(),
			'startime'=>strtotime($data['startime']),
			'endtime'=>strtotime($data['endtime']),
			'attrs_list'=>(!isset($data['attrs_list'])) ? '' : base64_encode(gzcompress(json_encode($data['attrs_list']),9)),
			'creator'=>$uid,
			'status'=>1,
		);
		
		if($id = $this->insertGetId($_data)) {
			return $id;
		} else {
			return false;
		}
	}
	
	//修改资源
	public function edit_resource($data,$uid,$id) {

		$_data = array(
			'name'=>trim($data['resource_name']),
			'cover_url'=>(!isset($data['cover_url'])) ? '' : trim($data['cover_url']),
			'resource_type'=>(int)$data['type'],
			'url'=>$data['resource_url'],
			'resource_token'=>$data['token'],
			'download_url'=>$data['download_url'],
			'author'=>$data['author'],
			'startime'=>strtotime($data['startime']),
			'endtime'=>strtotime($data['endtime']),
			'attrs_list'=>(!isset($data['attrs_list'])) ? '' : base64_encode(gzcompress(json_encode($data['attrs_list']),9)),
			//'creator'=>$uid,
		);
		
		$res = $this->where('resource_id','=',$id)
					->update($_data);
		return $res;
	}
}
