<?php 
/*
comment:导航模型
date:2015-7-21
author:hanyu
*/
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Nav extends Model {
	protected $table = 'nav_info';

	//获取物流公司列表
	public function nav_list() {
		$res = $this->select('nav_id','name')
					->where('tombstone',0)
					->get();

		if(!empty($res)) {
			return $res->toArray();
		} else {
			return false;
		}
	}
}