<?php 
/*
comment:商品资源关联模型
date:2015-7-6
author:hanyu
*/
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class GoodsRelation extends Model {
	
	protected $table = 'goods_relation';
	
	public $timestamps = false;
	
	//获取关联的资源id集合
	public function get_ids($goods_id) {
		$res = $this->where('goods_id','=',$goods_id)
					->lists('resource_id');
		if(!empty($res)) {	
			return array_unique($res);
		} else {
			return false;	
		}
	}
	
	//获取关联的资源信息
	public function get_relation_ids($goods_ids) {
		$res = $this->select('goods_relation.goods_id','r.resource_id','r.name','r.cover_url','resource_type','resource_token','download_url')
					->whereIn('goods_relation.goods_id',$goods_ids)
					->where('r.tombstone','=',0)
					->leftJoin('resource as r', 'goods_relation.resource_id', '=', 'r.resource_id')
					->orderBy('r.resource_id', 'asc')
					->get();
		
		if(!empty($res)) {
			
			$res = $res->toArray();
			$resources = array();
			
			//重组结构
			foreach($res as $v) {
				$goods_id = $v['goods_id'];
				$resource_id = $v['resource_id'];
				unset($v['goods_id'],$v['resource_id']);
				$resources[$goods_id][$resource_id] = $v;	
			}
			
			return $resources;
		} else {
			return false;	
		}
	}
	
	//添加关联关系到表
	public function insert_relation($data) {
		$ids = $data['ids'];
		
		$_data = array();
		foreach($ids as $k=>$v) {
			$arr = array();
			$arr['goods_id'] = $data['goods_id'];
			$arr['resource_id'] = $v;
			$_data[] = $arr;	
		}
		
		$res = $this->insert($_data);
		return $res;
	}
	
	//查询商品和资源是否有关联关系
	public function select_relation($data) {
	
		$res = $this->select('goods_id','resource_id')
					->where('goods_id','=',$data['goods_id'])
					->whereIn('resource_id',$data['ids'])
					->lists('goods_id','resource_id');
		if(!empty($res)) {
			return $res;
		} else {
			return false;
		}	
	}
	
	//查询商品和资源是否有关联关系
	public function delete_relation($data) {
	
		$res = $this->where('goods_id','=',$data['goods_id'])
					->where('resource_id','=',$data['id'])
					->delete();
		return $res;
	}
	
	//查询商品和资源是否有关联关系
	public function repeal_relation($data) {
	
		$res = $this->where('goods_id','=',$data['goods_id'])
					->whereIn('resource_id',$data['ids'])
					->delete();
		return $res;
	}
}