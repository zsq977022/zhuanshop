<?php
/*
comment:工具类
*/
class Cache {
	private $memcahce;//实例化对象
	private $host = '123.103.79.106';//memcache服务器host
	private $port = '11211';//memcache服务器端口
	
	public function __construct() {
		$this->memcache = new Memcache();
		$this->memcache->connect($this->host,$this->port);	
	}
	
	//设置数据
	public function set($key,$value,$expire = 60) {
		$this->memcache->set($key, $value,MEMCACHE_COMPRESSED,$expire);	
	}
	
	//获取数据
	public function get($key) {
		$this->memcache->get($key);	
	}
	
	//删除数据
	public function del($key) {
		$this->memcache->delete($key);	
	}
	
	//删除所有数据
	public function flushall() {
		$this->memcache->flush();		
	}
	
	//关闭连接
	public function close() {
		$this->memcache->close();		
	}
}
?>