<?php
/*
comment:工具类
*/
namespace App\Libs;
use Config;

class Helper {
	/**
	*检测参数是否为空
	* @param array $config 结构为map  字段名=>对应的提示信息的名字
	* @param array $data 要检测的数据
	*/
	//检测参数是否存在
	static public function empty_args($config,$data) {
		
		foreach($config as $k =>$v) {
			if(isset($data[$k])) {
				if(empty($data[$k])) {
					return $v.'不能为空!';
					break;	
				}
			} else {
				return $v.'不能为空!';
				break;	
			}
		}
		
		return false;
	}
	
	//加密函数
	static public function authcode($string, $operation = 'DECODE', $key = 'tiku', $expiry = 0) { 
			$key = Config::get('params.public_key');
			$ckey_length = 4; // 随机密钥长度 取值 0-32; 
			// 加入随机密钥，可以令密文无任何规律，即便是原文和密钥完全相同，加密结果也会每次不同，增大破解难度。 
			// 取值越大，密文变动规律越大，密文变化 = 16 的 $ckey_length 次方 
			// 当此值为 0 时，则不产生随机密钥 
			
			$key = md5($key); 
			$keya = md5(substr($key, 0, 16)); 
			$keyb = md5(substr($key, 16, 16)); 
			$keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length): substr(md5(microtime()), -$ckey_length)) : ''; 
			
			$cryptkey = $keya.md5($keya.$keyc); 
			$key_length = strlen($cryptkey); 
			
			$string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0).substr(md5($string.$keyb), 0, 16).$string; 
			$string_length = strlen($string); 
			
			$result = ''; 
			$box = range(0, 255); 
			
			$rndkey = array(); 
			for($i = 0; $i <= 255; $i++) { 
				$rndkey[$i] = ord($cryptkey[$i % $key_length]); 
			} 
			
			for($j = $i = 0; $i < 256; $i++) { 
				$j = ($j + $box[$i] + $rndkey[$i]) % 256; 
				$tmp = $box[$i]; 
				$box[$i] = $box[$j]; 
				$box[$j] = $tmp; 
			} 
			
			for($a = $j = $i = 0; $i < $string_length; $i++) { 
				$a = ($a + 1) % 256; 
				$j = ($j + $box[$a]) % 256; 
				$tmp = $box[$a]; 
				$box[$a] = $box[$j]; 
				$box[$j] = $tmp; 
				$result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256])); 
			} 
			
			if($operation == 'DECODE') { 
				if((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26).$keyb), 0, 16)) { 
					return substr($result, 26); 
				} else { 
					return ''; 
				} 
			} else { 
				return $keyc.str_replace('=', '', base64_encode($result)); 
			} 
	}
	
	/**
	 * 二维数组保存成 csv 文件
	 * @param array $data 二维数组
	 * @param string $file 文件路径
	 * @param array $header 表头配置
	 * @param bool $download 是否下载
	 *
	$header = array('name'=>'名字', 'age'=>'年龄');
	$data = array(
		array('name'=>'张三', 'age'=>34),
		array('name'=>'李四', 'age'=>15),
	);
	save_csv($data, 'D:/test.csv', $header, false);
	 *
	 */
	static public function save_csv($data, $file, $header = array(), $download = false){
		
		//$file = iconv('UTF-8', 'GBK', $file);
		if(empty($header)){
			$header = current($data);
		}elseif(!file_exists($file)){
			array_unshift($data, $header);
		}
		
		//如果下载
		if(true == $download){
			
			//2015-3-2 xilei 修改下载文件名(index.php)问题
			//$filename = basename($file);
			$filename = $file;
			$ua = $_SERVER["HTTP_USER_AGENT"];
			header("Content-type:application/octet-stream");
			header("Accept-Ranges:bytes");
			//header("Accept-length:$filesize");
			if (preg_match('/Firefox/i', $ua)) {
				header('Content-Disposition: attachment; filename*="utf8\'\'' . $filename . '"');
			} elseif (preg_match('/Chrome/i', $ua)) {
				header("Content-Disposition:attachment;filename=" . ($filename));
			} else {
				header("Content-Disposition:attachment;filename=" . urlencode($filename));
			}
			
			header("Content-Transfer-Encoding: binary");
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Type:application/octet-stream');
			header("Content-Disposition:attachment;filename=$file");
			$fp = tmpfile();
		}else{
			$fp = fopen($file, 'a');
		}
		//写入 BOM 头
		if(mb_detect_encoding(current(current($data)), array('UTF-8')) == 'UTF-8'){
			fwrite($fp, pack('H*','EFBBBF'));
		}
		//写入数据
		foreach($data as $row){
			$line = array();
			foreach($header as $field => $name){
				$line[] = trim($row[$field]);
			}
			fputcsv($fp, $line);
		}
		
		//下载输出
		if(true == $download){
			fseek($fp, 0);
			echo stream_get_contents($fp);
		}
		fclose($fp);
	}
			
	//php生成GUID
	static public function getHash() {
		 $charid = strtoupper(md5(uniqid(mt_rand(), true))); 
		 
		 //$hyphen = chr(45);// "-" 
		 $hyphen = '';
		 $uuid = substr($charid, 0, 8).$hyphen 
		 .substr($charid, 8, 4).$hyphen 
		 .substr($charid,12, 4).$hyphen 
		 .substr($charid,16, 4).$hyphen 
		 .substr($charid,20,12);
		 return $uuid; 
	}
	
	/**
	*根据字符串和定义的key生成md5字符串
	*@param string $password 密码字符串
	*/
	static function createMd5Pwd($password) {
		$key = config('params.public_key');	
		return md5($password.$key);
	}
	
	/**
	*创建订单号
	*@form ecshop里的方式
	*/
	static function create_order_num(){
    	/* 选择一个随机的方案 */
    	return date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
	}
	
	/**
	*截取字符串(超过长度才截取并在尾部追加省略号)
	*@param string $str 要截取的字符串
	*@param number $size 要截取的字符串
	*/
	
	static function aaa($str,$size) {
		
		if(mb_strlen($str,'utf8') > $size) {
			return mb_substr($str,0,$size,'utf8').'...';
		}
		return $str;
	}
}
?>