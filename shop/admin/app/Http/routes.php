<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//默认访问登录界面
Route::get('/',['middleware'=>'is_login','uses'=>'UserController@login']);

//验证码
Route::any('verify',['as'=>'show_verify','uses'=>'UserController@verify']);
Route::post('checkVerify','UserController@checkVerify');

//用户模块分组
Route::group(['prefix'=>'user','middleware'=>['is_login']],function () {
	//登录页面
	Route::get('login', 'UserController@login');
	//检测登录
	Route::post('login','UserController@_login');
});

//后台页面部分
Route::group(['prefix'=>'admin','middleware'=>['check_login']],function () {
	
	//后台首页
	Route::get('index','IndexController@index');
	//欢迎页面
	Route::get('welcome','IndexController@welcome');
	
	//修改密码
	Route::match(['get','post'],'updatePwd','IndexController@updatePwd');
	
	//登出
	Route::get('logout','IndexController@logout');
	
	//后台操作模块路由群组
	Route::group(['middleware'=>['check_role']],function () {		
		
		//编辑器上传
		Route::any('ueditor','CommonController@ueditor_upload');
	
		/*资源模块开始*/
		//资源列表
		Route::get('resourceList','ResourceController@resourceList');
		
		//ajax获取资源列表
		Route::post('resourceList','ResourceController@_resourceList');
		
		//资源预览
		Route::get('resourceDetail/{id?}','ResourceController@resourceDetail');
		
		//创建资源
		Route::match(['get','post'],'createResource','ResourceController@createResource');
		
		//删除资源
		Route::post('resourceDelete','ResourceController@resourceDelete');
		
		//修改资源(页面)
		Route::get('resourceEdit/{id}','ResourceController@resourceEdit');
		
		//修改资源(页面)
		Route::post('resourceEdit','ResourceController@_resourceEdit');
		
		//审核资源
		Route::post('resourceCheck','ResourceController@resourceCheck');
	
		//取消审核资源
		Route::post('resourceRepeal','ResourceController@resourceRepeal');
		
		//资源封面上传
		Route::any('upload_cover','ResourceController@plup_upload');
		/*资源模块结束*/
		
		/*商品模块开始*/
		//商品列表
		Route::get('goodsList/{page?}',function () {
			return view('goods.list')->withCookie(Cookie::forget('goods_img'));
		});
		//ajax请求商品列表
		Route::post('goodsList','GoodsController@_goodsList');
		
		//删除商品
		Route::post('deleteGoods','GoodsController@deleteGoods');
		
		//上下线商品
		Route::post('lineGoods','GoodsController@lineGoods');
		
		//商品详情Goodsdetail
		Route::get('goodsdetail/{goods_id}','GoodsController@goodsdetail');
		
		//创建商品页面
		Route::get('createGoods','GoodsController@createGoods');
		//创建商品ajax操作
		Route::post('createGoods','GoodsController@_createGoods');
		
		//编辑商品页面
		Route::get('editGoods/{goods_id?}','GoodsController@editGoods');
		
		//修改商品信息
		Route::post('editGoods','GoodsController@_editGoods');
		
		//资源封面上传
		Route::any('goods_cover','GoodsController@plup_upload');
		
		//已选择资源列表
		Route::get('changeResource/{goods_id}','GoodsController@changeResource');
		
		//可选择资源列表
		Route::post('miniResourceList','GoodsController@miniResourceList');
		
		//添加资源商品关系
		Route::post('relationResource','GoodsController@relationResource');
		
		//批量撤出资源repealResource
		Route::post('repealResource','GoodsController@repealResource');
		
		//删除资源商品关系(撤出)
		Route::post('optoutResource','GoodsController@optoutResource');
	
		//审核商品
		Route::post('checkGoods','GoodsController@checkGoods');
		
		//取消审核商品
		Route::post('repealGoods','GoodsController@repealGoods');
		
		//检查用户名
		Route::post('checkUser','CommonController@checkUser');
		
		//商品预览
		Route::get('previewGoods/{goods_id}','GoodsController@previewGoods');
		
		//创建商品成功
		Route::get('Goodssuccess',function () {
			return view('goods.success');	
		});
		/*商品模块结束*/
		
		/*订单模块开始*/
		
		//创建订单
		Route::get('createOrder',function () {
			return view('order.create');	
		});
		//创建订单
		Route::post('createOrder','OrderController@createOrder');
		
		//订单列表
		Route::get('orderList',function () {
			return view('order.list');	
		});
		//订单列表(ajax)
		Route::post('orderList','OrderController@orderList');
		
		//订单详情
		Route::get('orderdetail/{order_id}/{index}','OrderController@orderdetail');
		
		//编辑订单
		Route::get('editOrder/{order_id}','OrderController@editOrder');
		
		//编辑订单(ajax)
		Route::post('editOrder','OrderController@_editOrder');
		
		//删除订单
		Route::post('deleteOrder','OrderController@deleteOrder');
		
		//还原订单
		Route::post('restoreOrder','OrderController@restoreOrder');
		
		//审核订单
		Route::post('checkOrder','OrderController@checkOrder');
		
		//取消审核
		Route::post('cancelOrder','OrderController@cancelOrder');
		
		//选择商品列表
		Route::post('miniGoodsList','OrderController@miniGoodsList');
		
		//导出订单
		Route::any('exportExcel','OrderController@exportExcel');
		
		/*订单模块结束*/
		
		/*统计模块开始*/
		
		//邮寄订单列表
		Route::get('mailList','StatController@mailList');
		
		//请求邮寄订单列表
		Route::post('mailList','StatController@_mailList');
		
		//邮寄订单详情
		Route::get('maildetail/{order_id}','StatController@maildetail');
		
		//删除邮寄信息
		Route::post('deleteMail','StatController@deleteMail');
	
		//修改邮寄信息
		Route::post('updateMail','StatController@updateMail');
		
		//导出邮寄订单
		Route::post('exportMailExcel','StatController@exportMailExcel');
		
		//导入邮寄信息
		Route::any('importMailExcel','StatController@importMailExcel');
		
		/*统计模块结束*/
	});	
});



/*Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);*/
