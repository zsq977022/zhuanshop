<?php 
namespace App\Http\Controllers;
use App\Models\Goods;
use App\Models\GoodsRelation;
use App\Models\User;
use App\Models\Snap;
use App\Models\Order;
use Config,Session,Request,Response,Validator,Cookie,DB;
use App\Libs\Helper;

class OrderController extends Controller {
	
	//创建订单
	public function createOrder() {
		
			//接受数据
			$data = Request::all();
			
			$goods_ids = $data['goods_ids'];
			
			if(!empty($goods_ids)) {
				
				//查询商品信息
				$goods = new Goods();
				$goods_info = $goods->goods_infos($goods_ids);
				
				//检测商品ID是不是有异常
				if(count($goods_ids) !== count($goods_info)) return Response::json(array('status'=>0,'info'=>'商品信息异常!'));
				
				foreach($goods_info as $v) {
					if(!empty($v['quota_num'])) {
						if($v['goods_num'] + 1 > $v['quota_num']) {
							return Response::json(array('status'=>0,'info'=>'商品《'.$v['name'].'》购买人数已经到达上限,请修改其限购人数!'));	
						}	
					}
				}
				
				//检查商品限购人数是否已达上限
				
				//查询关联的资源信息
				$relation = new GoodsRelation();
				$relation_info = $relation->get_relation_ids($goods_ids);
				
				//根据来源判断订单号前缀
				switch($data['form']) {
						case 1:
							$prefix = 'httk_';//网站
							break;
						case 2:
							$prefix = 'httk_MI_';//客户端
							break;
						case 3:
							$prefix = 'httk_H_';//wap
							break;
						case 4:
							$prefix = 'httk_';//其他默认为网站的前缀
							break;
						default:
							$prefix = 'httk_';//默认为网站
							break;
				}
				//是否为邮寄订单
				$is_mail = false;
				
				//重组
				foreach($goods_info as $k=>$v) {
					if($v['mail'] == 1) $is_mail = true; 
					if(isset($relation_info[$v['goods_id']])) {
						$goods_info[$k]['resource_list'] = $relation_info[$v['goods_id']];
					} else {
						$goods_info[$k]['resource_list'] = '';	
					}
				}
			
				//创建快照信息,并记录快照ID
				$snap_ids = array();
				$snap = new Snap();
				foreach($goods_info as $v) {
					$goods_id = (int)$v['goods_id'];unset($v['goods_id']);
					
					//如果创建失败,则删除之前创建的所有快照
					if(!$id = $snap->create_snap($v,$goods_id)) {
						if(!empty($snap_ids)) $snap->delete_snaps($snap_ids);
						return Response::json(array('status'=>0,'info'=>'操作失败!'));
					} else {
						$snap_ids[] = $id;
					}
				}
				
				//生成订单,最开始在v_order_init_info表里(后台生成的全是已支付状态但是未审核通过)
				$_data = array();
				$_data['order_number'] = $prefix.Helper::create_order_num();
				if($data['order_type'] == 1) {
					$_data['is_give'] = 0;
				} else if($data['order_type'] == 2) {
					$_data['is_give'] = 1;
				}
				$_data['is_system'] = 0;
				$_data['snap_ids'] = json_encode($snap_ids);
				$_data['username'] = $data['username'];
				$_data['user_id'] = $data['user_id'];
				$_data['order_explain'] = $data['order_explain'];
				$_data['orders_time'] = strtotime($data['orders_time']);
				$_data['amount'] = (is_numeric($data['amount'])) ? $data['amount'] : 0;
				$_data['pattern'] = (int)$data['pattern'];
				$_data['form'] = (int)$data['form'];
				
				$order = new Order();
				if(!$order_id = $order->create_Order($_data)) {
					if(!empty($snap_ids)) $snap->delete_snaps($snap_ids);
				}
				
				//如果是邮寄订单,则插入邮寄订单表
				if($is_mail) {
					
					$mail_info = array();
					$mail_info['order_id'] = $order_id;
					$mail_info['receiver'] = $data['receiver'];
					$mail_info['telephone'] = $data['telephone'];
					$mail_info['address'] = $data['address'];
					
					if(!$order->create_mail_order($mail_info)) {
						//手动删除快照和订单
						if(!empty($snap_ids)) $snap->delete_snaps($snap_ids);
						$order->delete_Order($order_id);
						
						return Response::json(array('status'=>0,'info'=>'操作失败!'));	
					}	
				}
				return Response::json(array('status'=>1,'info'=>'操作成功!'));
			}

	}
	
	//编辑订单(ajax操作)
	public function _editOrder() {
			//接受数据
			$data = Request::all();
			
			if(!empty($data['goods_infos'])) {
				
					//实例化模型
					$snap = new Snap();
					$goods = new Goods();
					$relation = new GoodsRelation();
					$order = new Order();
					
					//验证订单id是否正确
					$order_id = (int)$data['id'];
					$order_info = $order->order_detail($order_id,'order_init_info');
					if(empty($order_info)) return Response::json(array('status'=>0,'info'=>'订单不存在!'));
					
					//以前的快照ID
					$old_snap_ids = json_decode($order_info['snap_ids'],true);
					$new_snap = $goods_ids = $snap_ids = array();
					
					//如果快照ID已经在订单里,则不查询这个商品
					foreach($data['goods_infos'] as $v) {
						
						//过去的商品
						if(in_array($v['snap_id'],$old_snap_ids)) {
							$snap_ids[] = (int)$v['snap_id'];
						//新添加的商品
						} else {
							$goods_ids[] = (int)$v['goods_id'];	
						}
						
					}
				
					//查询邮寄信息(用来判断之前是否为邮寄订单)
					$mail_info = $order->mail_status($order_id);
					$is_mail = empty($mail_info) ? false : true;
					
					//如果没有添加商品,则要验证是否对商品进行删除并且判断是否删除了邮寄商品
					if(!empty($goods_ids)) {
						//检测商品ID是不是有异常
						$goods_info = $goods->goods_infos($goods_ids);
						
						//如果查询出来的商品个数和ID个数不等,则异常
						if(count($goods_ids) !== count($goods_info)) return Response::json(array('status'=>0,'info'=>'商品信息异常!'));
						foreach($goods_info as $v) {
							if(!empty($v['quota_num'])) {
								if($v['goods_num'] + 1 > $v['quota_num']) {
									return Response::json(array('status'=>0,'info'=>'商品《'.$v['name'].'》购买人数已经到达上限,请修改其限购人数!'));	
								}	
							}
						}
						
						$relation_info = $relation->get_relation_ids($goods_ids);
						
						//重组
						foreach($goods_info as $k=>$v) {
							if(empty($is_mail)) {
								if($v['mail'] == 1) $is_mail = true;
							} else {
								if($v['mail'] == 0) $is_mail = false;	
							}
							if(isset($relation_info[$v['goods_id']])) {
								$goods_info[$k]['resource_list'] = $relation_info[$v['goods_id']];
							} else {
								$goods_info[$k]['resource_list'] = '';	
							}
						}
						
						foreach($goods_info as $v) {
							$goods_id = (int)$v['goods_id'];unset($v['goods_id']);
							
							//如果创建失败,则删除快照
							if(!$id = $snap->create_snap($v,$goods_id)) {
								if(!empty($snap_ids)) $snap->delete_snaps($new_snap);
								return Response::json(array('status'=>0,'info'=>'表单信息无修改!'));
								
							} else {
								//
								$snap_ids[] = $id;
								$new_snap[] = $id;
							}
						}
					} else {
						
						//查询旧快照所对应的商品ID列表
						$goods_ids = $snap->snaps_goods($snap_ids);	
						$goods_info = $goods->goods_infos($goods_ids);
						
						//重组
						foreach($goods_info as $k=>$v) {
							if(empty($is_mail)) {
								if($v['mail'] == 1) {
									$is_mail = true;
									break;
								}
							} else {
								if($v['mail'] == 0) {
									$is_mail = false;
									break;
								}	
							}
						}
					}
					
					//生成订单,最开始在v_order_init_info表里(后台生成的全是已支付状态)
					$_data = array();
					if($data['order_type'] == 1) {
						$_data['is_give'] = 0;
					} else if($data['order_type'] == 2) {
						$_data['is_give'] = 1;
					}
					$_data['is_system'] = 0;
					$_data['snap_ids'] = json_encode($snap_ids);
					$_data['username'] = $data['username'];
					$_data['user_id'] = $data['user_id'];
					$_data['order_explain'] = $data['order_explain'];
					$_data['orders_time'] = strtotime($data['orders_time']);
					$_data['amount'] = (is_numeric($data['amount'])) ? $data['amount'] : 0;
					$_data['pattern'] = (int)$data['pattern'];
					
					
					//修改订单
					$check = false;
					$check = $order->edit_Order($order_id,$_data);
					
					//记录邮寄信息
					$mail_data = array();
					$mail_data['receiver'] = $data['receiver'];
					$mail_data['telephone'] = $data['telephone'];
					$mail_data['address'] = $data['address'];
					
					//如果是邮寄订单,则修改邮寄订单表
					if(!empty($mail_info)) {
						//如果是邮寄订单,并且信息有变化,才去修改信息
						if(!empty($is_mail) &&
							($mail_info['receiver'] !== $data['receiver'] 
							|| $mail_info['telephone'] !== $data['telephone'] 
							|| $mail_info['address'] !== $data['address'])) {
							
							
							//修改邮寄信息
							$check = $order->edit_mail_order($order_id,$mail_data);
								
						} else if(empty($is_mail)) {
							//如果以前是邮寄订单,现在不是了,则删除邮寄信息
							$check = $order->delete_mail_order($order_id);
						}
						
					//如果之前不是邮寄订单,但是现在是邮寄订单,则插入信息
					} else {
						//插入邮寄信息
						if(!empty($is_mail)) {
							$mail_data['order_id'] = $order_id;
							$check = $order->create_mail_order($mail_data);
						}
					}
					
					//操作成功返回信息
					if(!empty($check)) {
						return Response::json(array('status'=>1,'info'=>'操作成功!'));
					//操作失败,如果有新增的快照,则删除快照,并把订单信息还原
					} else {
						$order->edit_Order($order_id,$order_info);
						if(!empty($snap_ids)) $snap->delete_snaps($new_snap);
						return Response::json(array('status'=>0,'info'=>'表单信息无修改!'));
					}
				}
	}
	//编辑订单
	public function editOrder($order_id) {
		$order = new Order();
		//订单信息
		$order_info = $order->order_detail($order_id,'order_init_info');
		
		//如果订单存在
		if(!empty($order_info)) {
			$data = array();
			$data['order_info'] = $order_info;
			
			//查询邮寄状态
			$mail_info = $order->mail_status($order_id);
			$data['mail_info'] = $mail_info;
	
			//快照ID
			$snap_ids = json_decode($order_info['snap_ids']);
			unset($mail_info,$order_info);
			
			//如果不为空则查询快照信息
			if(!empty($snap_ids)) {
				$snap = new Snap();
				$snap_info = $snap->snaps_infos($snap_ids);
				
				//如果快照信息不为空,则解码快照信息
				if(!empty($snap_info)) {
					foreach($snap_info as $k=>$v) {
						$snap_info[$k]['snap_info'] = json_decode(gzuncompress(base64_decode($v['snap_info'])),true);	
					}
					$data['snap_info'] = $snap_info;
				}
			}
			
			return view('order.edit')->with($data);
		//如果订单不存在
		} else {
			echo '订单不存在或者已审核!';	
		}
	}
	
	
	//订单列表(因查询条件太过复杂,使用sql拼接来查询)
	public function orderList() {
			//接受数据
			$data = Request::all();
			
			$table = array('v_order_init_info','v_order_finally_info','v_order_recycle_info');
			$page = (int)$data['page'];
			$pageSize = (int)config('params.pagesize');
			$start = ($page-1)*$pageSize;
			
			$where = array();
			
			//如果是搜索订单号,则其他条件不起作用
			if(isset($data['order_number']) && !empty($data['order_number'])) {
				$where[]= "binary order_number LIKE '%{$data['order_number']}%'";
			} else {
				
				//下单时间
				if(isset($data['order_time'])) {
					$startime = empty($data['order_time']['startime']) ? time() : strtotime($data['order_time']['startime']);
					$endtime = empty($data['order_time']['endtime']) ? time() : strtotime($data['order_time']['endtime']);
					$where[]= "(orders_time >= {$startime} AND orders_time <= {$endtime})";
				}
				
				//下单订单金额
				if(isset($data['amount'])) {
					$min_amount = (int)$data['amount']['min_amount'];
					$max_amount = (int)$data['amount']['max_amount'];
					
					if(empty($min_amount) && !empty($max_amount)) {
						$where[]= "amount <= {$max_amount}";
					} else if(!empty($min_amount) && empty($max_amount)) {
						$where[]= "amount >= {$min_amount}";
					} else {
						$where[]= "(amount >= {$min_amount} AND amount <= {$max_amount})";	
					}
				}
				
				//支付方式
				if(isset($data['pattern'])) $where[]= "pattern = {$data['pattern']}";
				
				//支付来源
				if(isset($data['form'])) $where[]= "form = {$data['form']}";
			}
			
			/*if(!empty($where)) {
				$where = 'WHERE '.implode(' AND ',$where);
			} else {
				$where = '';	
			}*/
			$where = (!empty($where)) ? ('WHERE '.implode(' AND ',$where)) : '';
			
			//查询出列表
			$list_sql = "SELECT o.order_id,o.order_number,o.is_system,o.is_give,o.pattern,o.form,o.amount,o.orders_time,o.username FROM {$table[$data['table_type']]} as o $where ORDER BY order_id DESC LIMIT {$start},{$pageSize}";
			
			//查询出总数
			$count_sql = "SELECT COUNT(*) as count FROM {$table[$data['table_type']]} $where";
			$return = array();
			
			$return['list'] = DB::select($list_sql);//查询搜索订单列表
			$return['count'] = DB::select($count_sql)[0]->count;//查询搜索订单总数
			$return['pageSize'] = $pageSize;//每页显示长度
			$return['page'] = $page;//页码
			
			$pattern = array(
				1 => '支付宝',
				2 => '微信',
				3 => '免支付',
			);
			$form = array(
				1 => 'web',
				2 => 'app',
				3 => 'wap',
				4 => '其他',
			);
			if(!empty($return['list'])) {
				
				//转换时间
				foreach($return['list'] as $k=>$v) {
					$return['list'][$k]->orders_time = date('Y-m-d H:i',$v->orders_time);	
					$return['list'][$k]->form = $form[$v->form];
					$return['list'][$k]->pattern = $pattern[$v->pattern];	
				}
				
			}
			
			return Response::json(array('status'=>1,'info'=>'获取成功','data'=>$return));
	}
	
	//导出excel
	public function exportExcel() {
		set_time_limit(240);
		$data = json_decode(urldecode(Request::input('export_data')),true);
		$table = array('v_order_init_info','v_order_finally_info','v_order_recycle_info');
			$where = array();
			
			//如果是搜索订单号,则其他条件不起作用
			if(isset($data['order_number']) && !empty($data['order_number'])) {
				$where[]= "binary order_number LIKE '%{$data['order_number']}%'";
			} else {
				
				//下单时间
				if(isset($data['order_time'])) {
					$startime = empty($data['order_time']['startime']) ? time() : strtotime($data['order_time']['startime']);
					$endtime = empty($data['order_time']['endtime']) ? time() : strtotime($data['order_time']['endtime']);
					$where[]= "(orders_time >= {$startime} AND orders_time <= {$endtime})";
				}
				
				//下单订单金额
				if(isset($data['amount'])) {
					$min_amount = (int)$data['amount']['min_amount'];
					$max_amount = (int)$data['amount']['max_amount'];
					
					if(empty($min_amount) && !empty($max_amount)) {
						$where[]= "amount <= {$max_amount}";
					} else if(!empty($min_amount) && empty($max_amount)) {
						$where[]= "amount >= {$min_amount}";
					} else {
						$where[]= "(amount >= {$min_amount} AND amount <= {$max_amount})";	
					}
				}
				//支付方式
				if(isset($data['pattern'])) $where[]= "pattern = {$data['pattern']}";
				//支付来源
				if(isset($data['form'])) $where[]= "form = {$data['form']}";
			}
			
			/*if(!empty($where)) {
				$where = 'WHERE '.implode(' AND ',$where);
			} else {
				$where = '';	
			}*/
			$where = (!empty($where)) ? ('WHERE '.implode(' AND ',$where)) : '';
			
			//查询出列表
			$list_sql = "SELECT o.order_id,o.order_number,o.pattern,o.form,o.amount,o.orders_time,o.snap_ids,u.username,u.phone,m.receiver,m.telephone,m.address FROM {$table[1]} as o LEFT JOIN v_user_info as u ON o.user_id = u.uid LEFT JOIN v_order_mail_info as m ON o.order_id = m.order_id $where";
			
			$map_ids = $snap_ids = $orders = array();
			
			$res = DB::select($list_sql);
			
			$pattern = array(
				1 => '支付宝',
				2 => '微信',
				3 => '免支付',
			);
			$form = array(
				1 => 'web',
				2 => 'app',
				3 => 'wap',
				4 => '其他',
			);
			
			//转换时间
			foreach($res as $k=>$v) {
				$ids = array();
				$v->orders_time = date('Y-m-d H:i',$v->orders_time);	
				$v->form = $form[$v->form];
				$v->pattern = $pattern[$v->pattern];
				$ids = json_decode($v->snap_ids,true);
				foreach($ids as $v1) {
					//建立快照ID对订单的映射
					$map_ids[$v1] = $v->order_id;
				}
				
				$orders[$v->order_id] = (array)$v;
				unset($orders[$v->order_id]['order_id']);
			}
			$snap = new Snap();
			$snap_ids = array_keys($map_ids);
			$snap_infos = $snap->snaps_infos($snap_ids);
			
			$data = array();
			foreach($snap_infos as $v) {
				$order_id = $map_ids[$v['snap_id']];
				$v['snap_info']	= json_decode(gzuncompress(base64_decode($v['snap_info'])),true);
				$orders[$order_id]['goods_name'] = $v['snap_info']['name'];
				$data[] = $orders[$order_id];
			}
			//倒叙排列
			
			$header = array(
					'order_number'=>'订单编号',
					'orders_time'=>'下单时间',
					'username'=>'用户名',
					'phone'=>'注册用户电话',
					'goods_name'=>'商品名称',
					'receiver'=>'收货人姓名',
					'address'=>'收货地址',
					'telephone'=>'收件人电话',
					'pattern'=>'支付方式',
					'form'=>'购买来源',
					'amount'=>'金额,
			');
			Helper::save_csv($data,'订单列表.csv',$header,true);
	}
	
	//订单详情
	public function orderdetail($order_id,$index) {
		$table = array('order_init_info','order_finally_info','order_recycle_info');
		
		$order = new Order();
		
		$data = array();
		$order_info = $order->order_info($order_id,$table[$index]);
		if(!empty($order_info)) {
			$data['order_info'] = $order_info;
			$data['mail_info'] = $order->mail_info($order_id);
			$data['order_info']['pay_status'] = ($table[$index] == 'order_finally_info') ? 1 : 0 ;

			//查询快照信息
			$snap = new Snap();
			$snap_ids = json_decode($order_info['snap_ids'],true);
			$snap_data = $snap->snaps_infos($snap_ids);
			
			//解密快照
			$snap_info = array();
			foreach($snap_data as $k=>$v) {
				$snap_info[$v['snap_id']] = json_decode(gzuncompress(base64_decode($v['snap_info'])),true);
			}
			$data['snap_info'] = $snap_info;
			
			//是否禁用按钮
			$data['index'] = $index;
			return view('order.detail')->with($data);
		} else {
			echo '订单不存在!';	
		}
	}
	
	//删除订单
	public function deleteOrder() {
			$data = Request::all();
			$table = array('order_init_info','order_finally_info','order_recycle_info');
			$order_id = (int)$data['id'];
			$index = (int)$data['index'];
			
			
			$order = new Order();
			$order_info = $order->order_detail($order_id,$table[$index]);
			
			//判断订单是否邮寄
			if($table[$index] == 'order_finally_info') {
				$mail_info = $order->mail_status($order_id);
				if(!empty($mail_info['main_status'])) return Response::json(array('status'=>0,'info'=>'此订单已邮寄!不能删除!'));
			}
			
			if(!empty($order_info)) {
				$order_info['plan'] = $index;
				
				//如果不在删除表里才执行删除操作
				if(empty($order->order_detail($order_id,$table[2]))) {
					//如果移到回收站成功,则删除在原来表的记录
					if($order->order_recycle($order_info)) {
						
						//删除在原来表的信息
						$order->delete_Order($order_id,$table[$index]);
						
						if($table[$index] == 'order_finally_info') {
							$order->update_goodsNum($order_id,1,'dec','order_recycle_info');	
						}
						return Response::json(array('status'=>1,'info'=>'操作成功!'));
						
					//如果移动失败,则删除回收站里的订单	
					} else {
						return Response::json(array('status'=>0,'info'=>'操作失败!'));	
					}
				} else {
						//删除在的信息
						$order->delete_Order($order_id,$table[0]);
						$order->delete_Order($order_id,$table[1]);
						
						return Response::json(array('status'=>1,'info'=>'操作成功!'));	
				}
			} else {
				return Response::json(array('status'=>0,'info'=>'订单不存在!'));
			}
	}
	
	//还原订单
	public function restoreOrder() {
			//接受数据
			$data = Request::all();
			
			//接受id
			$order_id = (int)$data['id'];
			
			//查询在回收站的订单信息,然后根据状态返回不同的表
			$order = new Order();
			$fields = array('order_id','order_number','is_give','snap_ids','username','user_id','order_explain','orders_time','amount','pattern','is_system','form','plan');
			
			//查询订单信息
			$order_info = $order->order_detail($order_id,'order_recycle_info',$fields);
			
			//根据情况移动到不同表
			if(!empty($order_info)) {
				
				//如果是系统订单,则根据情况
				if($order_info['is_system'] == 1) {
					$table = ($order_info['plan'] == 0) ? 'order_init_info' : 'order_finally_info';
					
				//手工订单一律还原到order_init_info表
				} else {
					$table = 'order_init_info';	
				}
				unset($order_info['plan']);
				
				//如果插入其他表成功,则删除掉回收站里的信息
				if($order->order_move($order_info,$table)) {
					//删除在回收站的信息
					$order->delete_Order($order_id,'order_recycle_info');
					return Response::json(array('status'=>1,'info'=>'操作成功!'));
				} else {
					return Response::json(array('status'=>0,'info'=>'操作失败!'));		
				}
				
			} else {
				return Response::json(array('status'=>0,'info'=>'订单不存在!'));	
			}
	}
	
	//审核订单
	public function checkOrder() {
			$data = Request::all();
			
			$order_id = (int)$data['id'];
			$index = (int)$data['index'];
			
			
			$order = new Order();
			$order_info = $order->order_detail($order_id,'order_init_info');
			
			if(!empty($order_info)) {
				
				$snap = new Snap();
				$snap_ids = json_decode($order_info['snap_ids']);
				$goods_ids = $snap->snaps_goods($snap_ids);
				
				if(!empty($goods_ids)) {
					//查询商品信息
					$goods = new Goods();
					$goods_info = $goods->goods_infos($goods_ids);
					
					foreach($goods_info as $v) {
						if(!empty($v['quota_num'])) {
							if($v['goods_num'] + 1 > $v['quota_num']) {
								return Response::json(array('status'=>0,'info'=>'商品《'.$v['name'].'》购买人数已经到达上限,请修改其限购人数!'));	
							}	
						}
					}
				}
				
				
				
				//如果移到最终订单表成功,则删除在原来表的记录
				if($order->order_move($order_info,'order_finally_info')) {
					
					//删除在初始化表的记录
					$order->delete_Order($order_id,'order_init_info');
					//增加商品的购买人数
					$order->update_goodsNum($order_id,1,'inc','order_finally_info');
					return Response::json(array('status'=>1,'info'=>'操作成功!'));
					
				//如果移动失败,则删除回收站里的订单	
				} else {
					return Response::json(array('status'=>0,'info'=>'操作失败!'));	
				}
				
			} else {
				return Response::json(array('status'=>0,'info'=>'订单不存在!'));
			}
	}
	
	//取消审核
	public function cancelOrder() {
			$data = Request::all();
			
			$order_id = (int)$data['id'];
			$order = new Order();
			$order_info = $order->order_detail($order_id,'order_finally_info');
			
			//判断订单是否邮寄
			$mail_info = $order->mail_status($order_id);
			
			//已邮寄订单是不能取消审核的
			if(!empty($mail_info['mail_status'])) return Response::json(array('status'=>0,'info'=>'此订单已邮寄!不能取消!'));
			
			if(!empty($order_info)) {
				
				//如果移到初始订单表成功,则删除在原来表的记录
				if($order->order_move($order_info,'order_init_info')) {
					//删除在初始化表的记录
					$order->delete_Order($order_id,'order_finally_info');
					
					//减去商品的购买人数
					$order->update_goodsNum($order_id,1,'dec','order_init_info');
					
					return Response::json(array('status'=>1,'info'=>'操作成功!'));
				//如果移动失败,则删除回收站里的订单	
				} else {
					return Response::json(array('status'=>0,'info'=>'操作失败!'));	
				}
				
			} else {
				return Response::json(array('status'=>0,'info'=>'订单不存在!'));
			}	
	}
	
	//选择商品列表
	public function miniGoodsList() {
			//接受数据
			$data = Request::all();
			
			$page = (int)$data['page'];unset($data['page']);
			$pageSize = (int)config('params.pagesize');
			
			$start = ($page-1)*$pageSize;
			
			$resource = new Goods();
			$return = array();
			$return['list'] = $resource->set_Goods_list($start,$data);//搜索的弹窗商品列表
			$return['count'] = $resource->set_Goods_count($data);//搜索的弹窗商品总数
			$return['pageSize'] = $pageSize;//每页长度
			$return['page'] = $page;//页码
			
			//转换时间
			foreach($return['list'] as $k=>$v) {
				$return['list'][$k]['startime'] = date('Y-m-d H:i',$v['startime']);	
				$return['list'][$k]['endtime'] = date('Y-m-d H:i',$v['endtime']);	
			}
			
			return Response::json(array('status'=>1,'info'=>'获取成功!','data'=>$return));
	}
}