@include('public.header')
<title>查看资源</title>
</head>
<body>
    <div id="container">
    	<div id="header">
            <h2 class="title">查看资源</h2>
            <div class="group_button">
                <a class="cancel fr">返回</a>
            </div>
        </div>
        <div id="content">
        		<div class="form_container">
                        <div class="form_div">
                            <div class="form_list first">
                                <label class="name">资源名称</label>
                                <div class="info">{{$data['name']}}</div>
                            </div>
                            <div class="form_list">
                                <label class="name">资源类型</label>
                                <div class="info">{{$type[$data['resource_type']]}}</div>
                            </div>
                            <div class="form_list">
                                <label class="name">资源地址</label>
                                <div class="info">{{$data['url']}}</div>
                            </div>
                            <div class="form_list">
                                <label class="name">token</label>
                                <div class="info">{{$data['resource_token']}}</div>
                            </div>
                            <div class="form_list">
                                <label class="name">下载地址</label>
                                <div class="info">{{$data['download_url']}}</div>
                            </div>
                            <div class="form_list">
                                <label class="name">授课老师(作者)</label>
                                <div class="info">{{$data['author']}}</div>
                            </div>
                            <div class="form_list">
                                <label class="name">有效期</label>
                                <div class="time_info">
                                    <div class="startime"><?php echo date('Y-m-d H:i',$data['startime']);?></div> 至
                                    <div class="endtime"><?php echo date('Y-m-d H:i',$data['endtime']);?></div>
                                </div>
                            </div>
                        </div>
                 
                        <div class="upload_div">
                            <div class="img">
                            @if(isset($data['cover_url']) && !empty($data['cover_url']))
                                <img src="{{$data['cover_url']}}"/>
                            @else
                            	无封面
                            @endif    
                            </div>
                        </div>
                        <div class = "attr_list">
                            <div class="form_list" >
                                <label class="name">商品扩展</label>
                                @if(isset($data['attrs_list']) && !empty($data['attrs_list']))
                                    <ul class="resource_button">
                                        <?php $i = 1;?>
                                        @foreach($data['attrs_list'] as $k=>$v)
                                            <li class="<?php if($i==1) echo 'first active';?>" data-name="{{$k}}" data-content="{{$v}}"><?php echo $i;?></li>
                                           <?php $i++;?>
                                        @endforeach
                                    </ul>
                                @else
                                	<div class="info">无</div>
                                @endif
                            </div>
                        </div>
                        @if(isset($data['attrs_list']) && !empty($data['attrs_list']))
                        <div class="attr_name">
                            <h2>名称</h2>
                            <div class="name_detail"><?php echo key($data['attrs_list']);?></div>
                        </div>
                        <div class="editor_div">
                            <div id="attr_detail">
                                <?php echo current($data['attrs_list']);?>
                            </div>
                        </div>
                        @endif
            	</div>
        </div>
    </div>
</body>
<script>
	$(function () {
	   preview($,window);
   })

</script>
</html>