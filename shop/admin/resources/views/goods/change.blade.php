@include('public.header')
<title>添加资源</title>
</head>
<body>
    <div id="container">
    	<div id="header">
            <h2 class="title">添加商品</h2>
            <ul class="plan">
                <li class="first">1.创建商品</li>
                <li class="active">2.添加资源</li>
                <li>3.商品预览</li>
                <li>4.提交审核</li>
            </ul>
            <div class="group_button">
                <a class="cancel" data-url = "{{__ROOT__}}/admin/editGoods/{{$goods_id}}">上一步</a>
               	<a href="{{__ROOT__}}/admin/previewGoods/{{$goods_id}}" class="confirm">商品预览</a>
            </div>
        </div>
        <div id="content">
            <div class="list">
                <table>
                    <thead>
                    	<tr>
                        	<th>选择</th>
                            <th>资源ID</th>
                            <th>资源名称</th>
                            <th>资源类型</th>
                            <th>作者(老师)</th>
                            <th>有效期</th>
                            <th>创建时间</th>
                            <th class="handle">操作</th>
                        <tr>
                    </thead>
                    <tbody>
                    	@if(!empty($resources))
                            @foreach($resources as $k=>$v)
                                <tr>
                                	<td><input type="checkbox" class="checkbox" data-id="{{$v['resource_id']}}"/></td>
                                    <td>{{$v['resource_id']}}</td>
                                    <td>{{$v['name']}}</td>
                                    <td>{{$types[$v['resource_type']]}}</td>
                                    <td>{{$v['author']}}</td>
                                    <td><?php echo date('Y-m-d H:i',$v['startime']);?>至<?php echo date('Y-m-d H:i',$v['endtime']);?></td>
                                    <td><?php echo date('Y-m-d H:i',$v['creatime']);?></td>
                                    <td>
                                        <a href="{{__ROOT__}}/admin/resourceDetail/{{$v['resource_id']}}" class="detail">查看</a>
                                        <a class="optout" data-id = "{{$v['resource_id']}}">撤出</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                    	<tr>
                        	<td><input type="checkbox" id="all" class="checkbox"/><label for="all">全选</label></td>
                        	<td colspan="7">
                            	<button id="change">+添加资源</button>
                                <button id="repeal" data-id="{{$goods_id}}">-批量撤出资源</button>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
<div id="dialog">
	
</div>
<div id="change_list">
	<div class="resource_list">
      <div class="header">
      		<h2>资源列表</h2>
            <div class="group_button">
                <button class="cancel">取消</button>
                <button class="confirm" data-id = "{{$goods_id}}">确定</button>
            </div>
      </div>
      <div class="content">
      		<div class="handle">
            	<div class="left_group">
                    <select class="select" name = "search_field">
                            <option value = ''>请选择搜索条件</option>
                            <option value = "resource_id">资源id</option>
                            <option value = "name">资源名称</option>
                    </select>
                    <input type="text" class="search_text text"/>
                    <button type="button" id="search">搜索</button>
                    <a href="{{__ROOT__}}/admin/createResource" class="location">+新增资源</a>
                </div>
                <div class="right_group">
                	
                </div>
            </div>
            <div class="list">
                <table>
                    <thead>
                    	<tr>
                        	<th>操作</th>
                            <th>ID</th>
                            <th>资源名称</th>
                            <th>资源类型</th>
                            <th>作者(老师)</th>
                            <th>资源有效期</th>
                        <tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    	<tr>
                        	<td><input type="checkbox" id="change_all" class="checkbox"/></td>
                            <td><label for="change_all">全选</label></td>
                        	<td colspan="11">
                            	<div class="page fr">
                                    <div class="page_info fl">共1条记录,每页10条,1页</div>
                                    <div class="page_handle fr">
                                        跳转至第 <input class="page" type="text"/> 页,页数<span class="num">1/1</span>
                                        <button class="prev"> < </button>
                                        <button class="next"> > </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
      </div>
     </div>
</div>
<script>
		window.URL = {
			'miniResourceList' : "{{__ROOT__}}/admin/miniResourceList",
			'relationResource' : "{{__ROOT__}}/admin/relationResource",
			'optoutResource' : "{{__ROOT__}}/admin/optoutResource",
			'repealResource' : "{{__ROOT__}}/admin/repealResource"
		}
		change_resource($,window);	
</script>
</body>
</html>