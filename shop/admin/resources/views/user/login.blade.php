@include('user.header')
<title>登录页</title>
</head>
<body>
	<div id="login_header">
		<div class="login_center">
			<div class="title">
				<img src="{{__STATIC__}}/images/logo.png" class="logo">
				<h1>商城系统</h1>
			</div>
		</div>
	</div>
    
	<div id="login_body">
		<div class="login_center">
			<div class="login_div">
				<h1>登录</h1>
				<form action = "{{__ROOT__}}/user/login" method="post">
                	<!--如果之前有勾选记住密码,则这里会显示用户名-->
					<input type="text" name = "username" placeholder='请输入用户名或邮箱' class="form_text" value=""/>
					<input type="password" name = "password"placeholder='请输入密码' class="form_text"/>
					<input type="text" name = "verify" placeholder='请输入验证码' class="code"/><img src = "{{__ROOT__}}/verify" class="verify"/>
					<label for='1'><input id="1" type="checkbox" name = "remember"/> 记住账号</label>
					<!--<a>忘记密码</a>-->
					<button type="button" id="login">立即登录</button>
				</form>
				<h5 class="error_info"></h5>
			</div>
		</div>
	</div>
@include('user.footer')	
</body>
<script>
	
</script>
</html>
