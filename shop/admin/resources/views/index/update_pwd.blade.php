@include('public.header')
<title>修改密码</title>
</head>
<body>
    <div id="container">
    	<div id="header">
            <h2 class="title">修改密码</h2>
            <div class="group_button">
                <a href="{{__ROOT__}}/admin/welcome" type="button" class="cancel">取消</a>
                <button type="button" class="confirm" id="update_pwd">修改密码</button>
            </div>
        </div>
        <div id="content">
        	<div class="order_form">
                    <div class="form_list first">
                        <label class="name">旧密码</label>
                        <input type="password" class="text" name="oldpwd"/>
                    </div>
                    <div class="error_list">
                        <div class="error_div">
                            111
                        </div>
                    </div>
                    <div class="form_list">
                        <label class="name">新密码</label>
                        <input type="password" class="text" name="newpwd"/>
                    </div>
                    <div class="form_list">
                        <label class="name">确认新密码</label>
                        <input type="password" class="text" name="repwd"/>
                    </div>
            </div>
        </div>
    </div>
</body>
<script>
	$(function () {
		//修改密码
		$('#update_pwd').on('click',function () {
			
			var _this = $(this);
			var arr = {
				'oldpwd':'旧密码',
				'newpwd':'新密码'	,
				'repwd':'确认密码'
			};
			var data = {};
			//有错误input元素名字和索引
			var name,index;
			
			//检测表单是否有空值
			$('#content input').each(function (k,v) {
				var __this = $(v);
				
				if($.trim(__this.val()) == '') {
					name = __this.attr('name');
					index = k;
					return false;
				} else {
					data[__this.attr('name')] = $.trim(__this.val());	
				}
			})
			//如果有错误,则显示提示信息
			if(name) {form_error($('input').eq(index),arr[name]+'不能为空!');return ;}
			//验证密码是否一致
			if(data['newpwd'] !== data['repwd']) {form_error($('input[name=repwd]'),'两次密码不一致!');return ;}
			
			$.post('{{__ROOT__}}/admin/updatePwd',data,function (res) {
				
				if(res.status) {
					window.location.href = res.url;
				} else {
					if(res['name']) {
						form_error($('input[name='+res['name']+']'),res['info']);	
					} else {
						alert(res.info);	
					}
				}
			})
		})
	})
</script>
</html>