@include('public.header')
<title>邮寄订单详情</title>
</head>
<body>
    <div id="container">
    	<div id="header">
            <h2 class="title">邮寄订单详情</h2>
            <div class="group_button">
                <button type="button" class="@if(empty($mail_info['mail_status'])) disabled @else delete @endif" data-id="{{$mail_info['order_id']}}">删除邮寄信息</button>
                <a type="button" class="confirm" onClick="javascript :history.back(-1);">返回列表</a>
            </div>
        </div>
        <?php 
            $pattern = array(
				1 => '支付宝',
				2 => '微信',
				3 => '免支付',
			);
			$form = array(
				1 => 'web',
				2 => 'app',
				3 => 'wap',
				4 => '其他',
			);
        ?>
        <div id="content">
        	<div class="form_container">
                <div class="detail_list">
                	<div class="detail_row">
                    	<div class="name">订单号</div>
                        <div class="detail">{{$mail_info['order_number']}}</div>
                    </div>
                    <div class="detail_row">
                        <div class="name">订单类型</div>
                        <div class="detail">
                            @if($mail_info['is_system'] == 1)
                                系统订单
                            @else
                                @if($mail_info['is_give'] == 1)
                                手工订单(赠送)
                                @else
                                手工订单
                                @endif
                            @endif
                        </div>
                    </div>
                    <div class="detail_row">
                    	<div class="name">商品名称</div>
                        <div class="detail">
                            <ul class="goods_detail">
                                @if(isset($snap_info) && !empty($snap_info))
                                    @foreach($snap_info as $v)
                                        <li>{{$v['name']}}</li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="detail_row">
                        <div class="name">用户名</div>
                        <div class="detail">{{$mail_info['username']}}</div>
                    </div>
                    <div class="detail_row">
                            <div class="name">收件人</div>
                            <div class="detail">{{$mail_info['receiver']}}</div>
                    </div>
                    <div class="detail_row">
                        <div class="name">联系电话</div>
                        <div class="detail">{{$mail_info['telephone']}}</div>
                    </div>
                    <div class="detail_row">
                        <div class="name">收件地址</div>
                        <div class="detail">{{$mail_info['address']}}</div>
                    </div>
                    <div class="detail_row">
                        <div class="name">下单时间</div>
                        <div class="detail"><?php echo date('Y-m-d H:i',$mail_info['orders_time']);?></div>
                    </div>
                    <div class="detail_row">
                        <div class="name">订单金额</div>
                        <div class="detail">{{$mail_info['amount']}}</div>
                    </div>
                    <div class="detail_row">
                        <div class="name">支付方式</div>
                        <div class="detail">{{$pattern[$mail_info['pattern']]}}</div>
                    </div>
                    <div class="detail_row">
                        <div class="name">购买来源</div>
                        <div class="detail">{{$form[$mail_info['form']]}}</div>
                    </div>
                    <div class="detail_row">
                        <div class="name">订单说明</div>
                        <div class="detail">
                        @if(!empty($mail_info['order_explain']))
                            {{$mail_info['order_explain']}}
                        @else
                            暂无
                        @endif
                        </div>
                    </div>
                    <div class="detail_row">
                    	<div class="name">快递公司</div>
                        <div class="detail">
                            @if(isset($company_list[$mail_info['company_id']]))
                                {{$company_list[$mail_info['company_id']]}}
                            @endif
                        </div>
                    </div>
                    <div class="detail_row">
                    	<div class="name">快递单号</div>
                        <div class="detail">{{$mail_info['company_num']}}</div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</body>
<script>
    window.URL = {
        'delete_url':"{{__ROOT__}}/admin/deleteMail"  
    };
    //删除订单的邮寄信息
    $('.group_button').on('click','.delete',function () {
        var _this = $(this);
        var ids = [];
        ids.push(_this.data('id'));
        if(confirm('确定要删除这个邮寄订单的邮寄信息吗?')) {
            $.post(URL.delete_url,{ids:ids},function (res) {
                if(res.status) {
                    window.history.back();
                } else {
                    alert(res.info);
                }
            })
        }
    })
</script>
</html>