@include('public.header')
<script type="text/javascript" src="{{__STATIC__}}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="{{__STATIC__}}/css/jquery.datetimepicker.css"/>
<title>修改订单</title>
</head>
<body>
    <div id="container">
    	<div id="header">
            <h2 class="title">修改订单</h2>
            <div class="group_button">
                <a type="button" class="cancel" data-url="{{__ROOT__}}/admin/orderList">取消</a>
                <button type="button" class="confirm handle" id="update_order" data-id = "{{$order_info['order_id']}}" data-is_mail="<?php if(!empty($mail_info)) {echo 1;} else {echo 0;}?>">修改订单</button>
            </div>
        </div>
        <div id="content">
        	<div class="order_form">
                    <div class="form_list first">
                        <label class="name">订单类型</label>
                        <select class="select" name="order_type">
                        	<option value='0'>请选择类型</option>
                            <option value="1" <?php if($order_info['is_give'] == 0) echo 'selected=selected';?>>常规订单</option>
                            <option value="2" <?php if($order_info['is_give'] == 1) echo 'selected=selected';?>>代报订单</option>
                        </select>
                    </div>
                    <div class="goods_list">
                        <label class="name">选择商品</label>
                        <ul class="goods_list" data-status = "<?php if(!empty($mail_info)) {echo $mail_info['mail_status'];} else {echo 0;}?>">
                        	@foreach($snap_info as $k=>$v)
                            	<li title="{{$v['snap_info']['name']}}" data-id="{{$v['goods_id']}}" data-snap_id="{{$v['snap_id']}}" data-mail="{{$v['snap_info']['mail']}}">
                                <!--如果长度超过了10位,则进行截取-->
                                @if(mb_strlen($v['snap_info']['name'],'utf8')>10)
                                	{{mb_substr($v['snap_info']['name'],0,10,'utf8')}}...
                                @else
                                	{{$v['snap_info']['name']}}
                                @endif
                                
                                @if(!empty($v['snap_info']['mail']))
                                <img src="{{__STATIC__}}/images/mail.png" class="mail"/>
                                @endif
                                <img/>
                                </li>
                            @endforeach
                        </ul>
                        <button type="button" id="change_goods"></button>
                        <div class="error_list">
                    		<div class="error_div"></div>
                    	</div>
                    </div>
                    <div class="form_list">
                        <label class="name">用户名</label>
                        <input type="text" class="text" name="username" value="{{$order_info['username']}}" data-user_id="{{$order_info['user_id']}}"/>
                    </div>
                    <div class="form_list <?php if(empty($mail_info)) {echo 'none';}?>">
                        <label class="name">收件人</label>
                        <input type="text" class="text" name="receiver" value="{{$mail_info['receiver']}}"/>
                    </div>
                    <div class="form_list <?php if(empty($mail_info)) {echo 'none';}?>">
                        <label class="name">联系电话</label>
                        <input type="text" class="text" name="telephone" value="{{$mail_info['telephone']}}"/>
                    </div>
                    <div class="form_list <?php if(empty($mail_info)) {echo 'none';}?>">
                        <label class="name">收件地址</label>
                        <input type="text" class="text" name="address" value="{{$mail_info['address']}}"/>
                    </div>
                    <div class="form_list">
                        <label class="name">下单时间</label>
                        <input type="text" class="text selectime" name="orders_time" readonly/>
                    </div>
                    <div class="form_list">
                        <label class="name">订单金额</label>
                        <input type="text" class="text" name="amount" value="{{$order_info['amount']}}"/>
                    </div>
                    <div class="form_list">
                        <label class="name">支付方式</label>
                        <select class="select" name="pattern">
                        	<option value='0'>请选择支付方式</option>
                            <option value="1" <?php if($order_info['pattern'] == 1) echo 'selected=selected';?>>支付宝</option>
                            <option value="2" <?php if($order_info['pattern'] == 2) echo 'selected=selected';?>>微信</option>
                            <option value="3" <?php if($order_info['pattern'] == 3) echo 'selected=selected';?>>免支付</option>
                        </select>
                    </div>
                    <div class="form_list">
                        <label class="name">订单说明</label>
                        <input type="text" class="text" name="order_explain" value="{{$order_info['order_explain']}}"/>
                    </div>
            </div>
        </div>
    </div>
<div id="dialog">
	
</div>
<div id="change_list">
      <div class="header">
      		<h2>商品列表</h2>
            <div class="group_button">
                <button class="cancel">取消</button>
                <button class="confirm">确定</button>
            </div>
      </div>
      <div class="content">
      		<div class="handle">
            	<div class="left_group">
                    <select class="select" name="search_field">
                        <option value = "goods_id">商品ID</option>
                        <option value = "name">商品名称</option>
                    </select>
                    <input type="text" class="search_text text"/>
                    <button type="button" id="search">搜索</button>
                    <a href="{{__ROOT__}}/admin/createGoods" class="location">+新增商品</a>
                </div>
                <div class="right_group">
                	
                </div>
            </div>
            <div class="list">
                <table>
                    <thead>
                    	<tr>
                            <th>商品ID</th>
                            <th>金额</th>
                            <th>商品名称</th>
                            <th>邮寄</th>
                            <th>商品有效期</th>
                            <th>操作</th>
                        <tr>
                    </thead>
                    <tbody>
                       
                    </tbody>
                    <tfoot>
                    	<tr>
                        	<td colspan="11">
                            	<div class="page">
                                    <div class="page_info">共0条记录,每页1条,共1页</div>
                                    <div class="page_handle">
                                        跳转至第 <input type="text" value="1" class="page"/> 页,页数<span class="num">1/1</span>
                                        <button class="prev"> < </button>
                                        <button class="next"> > </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
      </div>
</div>
</body>
<script>
	window.URL = {
		'list_url':	"{{__ROOT__}}/admin/miniGoodsList",
		'edit_url':"{{__ROOT__}}/admin/editOrder",
		'delete_img':"{{__STATIC__}}/images/delete.png",
		'mail_img':"{{__STATIC__}}/images/mail.png",
        'back_list': "{{__ROOT__}}/admin/orderList",
		'check_user': "{{__ROOT__}}/admin/checkUser"
	};
	$('input[name=orders_time]').datetimepicker({
			lang:'ch',
			timepicker:true,//是否精确到时分秒
			format:'Y-m-d H:i',
			formatDate:'Y-m-d',
			formatTime:'H:i',
            step:5,
			validateOnBlur:false,//失去焦点不保存日期
	});
	$('input[name=orders_time]').datetimepicker({value:'<?php echo date('Y-m-d H:i',$order_info['orders_time']);?>'});
	add_order($,window);
</script>
</html>