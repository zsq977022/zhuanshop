$(function () {
	
	//输入框获取焦点后消除报错信息
	$('form input').on('focus',function (e) {
		$('h5.error_info').text('');
		e.stopPropagation(); 
	})
	
	//登录操作
	$('#login').on('click',function () {
		var _this = $(this);
		var form = _this.parent();
		
		var arr = {
			'username':'用户名或邮箱',
			'password':'密码',
			'verify':'验证码'
		};
		var data = {};
		var check = false;
		
		//检测表单是否有空值
		$('form input').each(function () {
			var __this = $(this);
			var name = __this.attr('name');
			//如果有空值得话,显示错误信息
			if($.trim(__this.val()) == '') {
				
				data[name] = __this.val();
				$('h5.error_info').text(arr[name]+'不能为空!');
				reload_verify();
				check = true;
				return false;
				
			//否则把表单的值存入data对象
			} else {
				if(name == 'remember') {
					if($('input[name='+name+']').attr('checked')) {
						data[name] = 1;
					} else {
						data[name] = 0;
					}
				} else {
					data[name] = __this.val();	
				}
			}
		})
		
		//如果有错误信息,则禁止往下执行
		if(check) return ;
		//验证登录信息
		$.post(form.attr('action'),data,function (res) {
			if(res.status) {
				window.location.href = res.url;	
			} else {
				reload_verify()
				$('h5.error_info').text(res.info);	
			}
		})
	})
	
	//回车提交
	$(document).on('keydown',function (e) {
		if(e.keyCode == 13) $('#login').click();
	})
	

	//点击刷新验证码
	$('#login_body').on('click','.verify',reload_verify)
})
//重新载入验证码
function reload_verify() {
	var _this = $('#login_body').find('.verify');
	var src = _this.attr('src');
	/*$.get(verify.data('url'),{},function (res) {
		var data = eval('('+res+')');
		verify.attr('src','data:image/png;base64,'+data.img_url).data('code',data.code);
	})*/
	
	_this.attr('src',src+'?'+Math.random());
}