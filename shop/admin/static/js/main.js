//资源列表
function resource_list($,w) {
	//请求列表地址
	var list_url = URL.list;
	
	//上一页
	$('.page_handle').on('click','button.prev',function () {
		var _this = $(this)
		var page_info = _this.siblings('span.num').text().split('/');
		var page = parseInt(page_info[0]);
		var maxNum = parseInt(page_info[1]);
		
		//如果当前页码是1,则禁止上一页
		if(page == 1 || maxNum == 1) return ;
		var page = page - 1;
		//请求列表数据
		reload_list(page);
	})
	
	
	//下一页
	$('.page_handle').on('click','button.next',function () {
		var _this = $(this)
		var page_info = _this.siblings('span.num').text().split('/');
		var page = parseInt(page_info[0]);
		var maxNum = parseInt(page_info[1]);
		
		//如果已经是最后一页则禁止请求分页属性
		if(page == maxNum || maxNum == 1) return ;
		var page = page + 1;
		reload_list(page);
	})
	
	//搜索
	$('#search').on('click',function () {
		var _this = $(this);
		var search_data = {};
		var search_field = $('select[name=search_field]');
		var search_text = $('.search_text');
		var resource_type = $('select[name=resource_type]');
		
		//如果搜索条件不为空,则放入对象
		if(search_field.val() !== '' && search_text.val() !== '') {
			search_data[search_field.val()]	= search_text.val();
		}
		
		//如果资源类型不为空,则放入对象
		if(resource_type.val() !== '') {
			search_data[resource_type.attr('name')] = resource_type.val();	
		}
		//如果是空对象,则删除掉搜索按钮上的数据
		if(isEmptyObject(search_data)) {
			_this.removeData('search_data');
			return ;
		//否则把搜索数据记录在搜索按钮上
		} else {
			_this.data('search_data',JSON.stringify(search_data));	
		}
		
		request_list(search_data);
	})
	
	//输入框分页
	$('input.page').on('focus',function () {
		$(window).off('keydown').on('keydown',function (e){
			if(e.keyCode == 13) {
				var page_info = $('span.num').text().split('/');
				var page = parseInt(page_info[0]);
				var input_page = parseInt($('input.page').val());
				var maxNum = parseInt(page_info[1]);
				
				if(maxNum == 1) return ;
				
				//如果输入的就是当前页或者最大页数是1,则不请求
				if(input_page == page || maxNum == 1) {
					return ;
					
				//如果输入页数小于1,当1处理	
				} else if(input_page < 1) {
					input_page = 1;	
					
				//如果超过最大页数,则当最后一页处理
				} else if(input_page > maxNum) {
					input_page = maxNum;	
				}
				
				var data = {};
				if($('#search').data('search_data') !== undefined) {
					data = JSON.parse($('#search').data('search_data'));
				}
				data['page'] = input_page;
				//请求列表数据
				request_list(data);
			}	
		})
	})
	
	//更多操作
	$('div.list').on('change','select.handle',function () {
		var _this = $(this);
		var action = _this.val();
		var id = _this.data('id');
		var url = '';
		var info = '';
		
		//如果是编辑,则直接跳转
		switch (action) {
			case 'edit':
				window.location.href = URL.edit_url+id;
				break;
			case 'delete':
				url = URL.delete_url;
				info = "确定要删除这个资源吗?";
				break;
			case 'check':
				url = URL.check_url;
				info = "确定要审核通过这个资源吗?";
				break;
			case 'cancel':
				url = URL.cancle_url;
				info = "确定要取消这个资源的审核吗?";
				break;
			default:return ;
		};
		//IE7会往下执行
		if(action =='edit') return ;
		
		//否则ajax请求
		if(confirm(info)) {
			$.post(url,{resource_id:id},function (res) {
				
				if(res.status) {
					if(action == 'delete') _this.parents('tr').remove();
					var page = parseInt($('span.num').text().split('/')[0]);
					reload_list(page);
				} else {
					alert(res.info);
					_this.find("option:first").attr("selected",true);	
				}	
			})	
		}
	})
	
	
	//根据页码重载页面
	function reload_list(page) {
		//删除后重载列表
		var data = {};
		if($('#search').data('search_data') !== undefined) {
			data = JSON.parse($('#search').data('search_data'));
		}
		data['page'] = page;
		request_list(data);	
	}
	
	//根据页码请求资源列表数据
	function request_list(data) {
		data['page'] = data['page'] ? data['page'] : 1;
		$.post(list_url,data,function (res) {
			
			var response = res['data'];
			if(data['page'] == response.page) {
				//清空tbody
				$('tbody').empty();
				
				//加载列表
				$('tbody').html(load_list(response,'resource_list'));
				
				//加载分页信息
				var pageNum = Math.ceil(response.count/response.pageSize);
				if(pageNum == 0) pageNum = 1;				$('div.page_info').text('共'+response.count+'条记录,每页'+response.pageSize+'条,共'+pageNum+'页');
				$('input.page').val(response.page);
				$('span.num').text(response.page+'/'+pageNum);
			}
		})
	}
	request_list({page:1});
}

/*商品列表*/
function goods_list($,w) {
	var list_url = URL.goodsList;
	
	//上一页
	$('.page_handle').on('click','button.prev',function () {
		var _this = $(this)
		var page_info = _this.siblings('span.num').text().split('/');
		var page = parseInt(page_info[0]);
		var maxNum = parseInt(page_info[1]);
		
		//如果当前页码是1,则禁止上一页
		if(page == 1 || maxNum == 1) return ;
		var page = page - 1;
		//请求列表数据
		reload_list(page);
	})
	
	
	//下一页
	$('.page_handle').on('click','button.next',function () {
		var _this = $(this)
		var page_info = _this.siblings('span.num').text().split('/');
		var page = parseInt(page_info[0]);
		var maxNum = parseInt(page_info[1]);
		
		//如果已经是最后一页则禁止请求分页属性
		if(page == maxNum || maxNum == 1) return ;
		var page = page + 1;
		reload_list(page);
	})
	
	//搜索
	$('#search').on('click',function () {
		var _this = $(this);
		var search_data = {};
		var search_field = $('select[name=search_field]');
		var search_text = $('.search_text');
		
		//如果搜索条件不为空,则放入对象
		if(search_field.val() !== '' && search_text.val() !== '') {
			search_data[search_field.val()]	= search_text.val();
		}
		
		//如果是空对象,则删除掉搜索按钮上的数据
		if(isEmptyObject(search_data)) {
			_this.removeData('search_data');
			return ;
		//否则把搜索数据记录在搜索按钮上
		} else {
			_this.data('search_data',JSON.stringify(search_data));	
		}
		
		request_list(search_data);
	})
	
	//输入框分页
	$('input.page').on('focus',function () {
		$(window).off('keydown').on('keydown',function (e){
			if(e.keyCode == 13) {
				var page_info = $('span.num').text().split('/');
				var page = parseInt(page_info[0]);
				var input_page = parseInt($('input.page').val());
				var maxNum = parseInt(page_info[1]);
				
				if(maxNum == 1) return ;
				
				//如果输入的就是当前页或者最大页数是1,则不请求
				if(input_page == page || maxNum == 1) {
					return ;
					
				//如果输入页数小于1,当1处理	
				} else if(input_page < 1) {
					input_page = 1;	
					
				//如果超过最大页数,则当最后一页处理
				} else if(input_page > maxNum) {
					input_page = maxNum;	
				}
				
				var data = {};
				if($('#search').data('search_data') !== undefined) {
					data = JSON.parse($('#search').data('search_data'));
				}
				data['page'] = input_page;
				//请求列表数据
				request_list(data);
			}	
		})
	})
	
	//更多操作
	$('#content').on('change','select.handle',function () {
		var _this = $(this);
		var action = _this.val();
		var id = _this.data('id');
		var url = '';
		var info = '';
		
		switch(action) {
			case 'edit':
				window.location.href = URL.edit_url+id;
				break;
			case 'delete':
				info = '确定要删除这个商品吗?';
				url = URL.delete_url;
				break;
			case 'online':
				info = '确定要上线这个商品吗?';
				url = URL.line_url;
				break;
			case 'offline':
				info = '确定要下线这个商品吗?';
				url = URL.line_url;
				break;
			case 'check':
				info = '确定要审核这个商品吗?';
				url = URL.check_url;
				break;
			case 'cancel':
				info = '确定要取消审核这个商品吗?';
				url = URL.cancel_url;
				break;
			default:return ;	
		}
		if(action == 'edit') return ;
		
		//发出确认弹出框
		if(confirm(info)) {
			$.post(url,{goods_id:id,handle:action},function (res) {
				if(res.status) {
					if(action == 'delete') _this.parents('tr').remove();	
					var page = parseInt($('span.num').text().split('/')[0]);
					reload_list(page);
				} else {
					alert(res.info);
					_this.find("option:first").attr("selected",true);	
				}
			})
		}
	})
	
	//根据页码重载页面
	function reload_list(page) {
		//删除后重载列表
		var data = {};
		if($('#search').data('search_data') !== undefined) {
			data = JSON.parse($('#search').data('search_data'));
		}
		data['page'] = page;
		request_list(data);	
	}
	
	//根据页码请求资源列表数据
	function request_list(data) {
		data['page'] = data['page'] ? data['page'] : 1;
		$.post(list_url,data,function (res) {
			
			var response = res['data'];
			if(data['page'] == response.page) {
				//清空tbody
				$('tbody').empty();
				
				//加载列表
				$('tbody').html(load_list(response,'goods_list'));
				
				//加载分页信息
				var pageNum = Math.ceil(response.count/response.pageSize);
				if(pageNum == 0) pageNum = 1;
				$('div.page_info').text('共'+response.count+'条记录,每页'+response.pageSize+'条,共'+pageNum+'页');
				$('input.page').val(response.page);
				$('span.num').text(response.page+'/'+pageNum);
			}
		})
	}
	request_list({page:1});
}

/*加载table列表*/
function load_list(data,action) {
	var html = '';
	//如果是加载资源列表
	if(action == 'resource_list') {
		console.log(data['list']);
		$.each(data['list'],function (k,v) {
			html += '<tr>';
			html += '<td>'+v.resource_id+'</td>';
			html += '<td><a href='+URL.detail+v.resource_id+' class="detail">'+v.name+'</a></td>';
			html += '<td>'+data.types[v.resource_type]+'</td>';
			html += '<td title='+v.author+'>'+strSub(v.author,4)+'</td>';
			
			html += '<td>'+v.startime+' 至 '+v.endtime+'</td>';
			html += '<td>'+v.creatime+'</td>';
			html += v.status ? '<td>已审核</td>' : '<td>未审核</td>'; 
			html += '<td>';
			html += '<a href='+URL.detail+v.resource_id+' class="detail">查看</a>　';
			html += '<select class="handle"  data-id = "'+v.resource_id+'">';
			html += '<option>更多</option>';
			html += '<option value = "edit">修改</option>';
			html += '<option value = "delete">删除</option>';
			html += (v.status == 1) ? '<option value = "cancel">取消审核</option>' : '<option value = "check">审核</option>'
			html += '</select>';
			html += '</td>';
			html += '</tr>';
		})
	//选择资源页面加载资源列表
	} else if(action == 'mini_resource_list') {
		var ids = $('#change_list').find('.confirm').data('ids');
		if(ids == undefined) ids = [];
		$.each(data['list'],function (k,v) {
			var checked = '';
			if($.inArray(v.resource_id,ids) !== -1) checked = "checked = checked";
			html += '<tr>';
			html += '<td><input type="checkbox" class="checkbox" '+checked+' data-id = "'+v.resource_id+'"/></td>';	
			html += '<td>'+v.resource_id+'</td>';
			html += '<td title="'+v.name+'">'+strSub(v.name,15)+'</td>';
			html += '<td>'+data.types[v.resource_type]+'</td>';
			html += '<td title="'+v.author+'">'+strSub(v.author,4)+'</td>';
			html += '<td>'+v.startime+' 至 '+v.endtime+'</td>';
			html += '</tr>';
		})
		
	//加载商品列表
	} else if(action == 'goods_list') {
		$.each(data['list'],function (k,v) {
			var status = '';
			var text = '上线';
			var value = 'online';
			
			if(v.puawaytime == 0) {
				status = '未上线';	
			} else {
				if(time() >= v.endtime) {
					status = '已下线';	
				} else {
					if(v.online == 0) {
						status = '已下线';	
					} else {
						status = '已上线';
						text = '下线';
						value = 'offline';
					}
				}	
			}
			//经过函数处理下格式
			v.price = number_format(v.price);
			html += '<tr>';
			html += '<td>'+v.goods_id+'</td>';
			html += '<td><a class="detail" href="'+URL.goodsdetail+v.goods_id+'">'+v.name+'</a></td>';
			html += '<td>'+v.price+'</td>';
			html += '<td>'+v.startime+' 至 '+v.endtime+'</td>';
			html += '<td>'+v.creatime+'</td>';
			html += (v.status == 1) ? '<td>已审核</td>' : '<td>未审核</td>';
			html += '<td>'+status+'</td>';
			html += '<td>';
			html += '<a class="detail" href="'+URL.goodsdetail+v.goods_id+'">查看</a>　';
			html += '<select class="handle" data-id = '+v.goods_id+'>';
			html += '<option value ="">更多</option>';
			html += '<option value="edit">编辑</option>';
			html += '<option value="delete">删除</option>';
			html += '<option value="'+value+'">'+text+'</option>';
			html += (v.status == 1) ? '<option value = "cancel">取消审核</option>' : '<option value = "check">审核</option>'
			html += '</select>';
			html += '</td>';
			html += '</tr>';
		})
	//弹窗商品列表
	} else if(action == 'mini_goods_list') {
		var goods_info = $('#change_list').find('.confirm').data('goods_info');
		if(goods_info == undefined) goods_info = {};
		$.each(data['list'],function (k,v) {
			var checked = '';
			var mail = '否';
			if((goods_info[v.goods_id]) !== undefined) checked = "checked = checked";	
			if(v.mail == 1) mail = '是';
			
			//经过函数处理下格式
			v.price = number_format(v.price);
			
			html += '<tr>';
			html += '<td>'+v.goods_id+'</td>';
			html += '<td>'+v.price+'</td>';
			html += '<td>'+v.name+'</td>';
			html += '<td>'+mail+'</td>';
			html += '<td>'+v.startime+' 至 '+v.endtime+'</td>';
			html += '<td>';
			html += '<input type="checkbox" class="checkbox" '+checked+' data-id = "'+v.goods_id+'" data-name = "'+v.name+'" data-mail = "'+v.mail+'"/>';
			html += '</td>';
			html += '</tr>';
		})
	//订单列表
	} else if(action == "order_list") {
		$.each(data['list'],function (k,v) {
			var type = '';
			if(v.is_system == 1) {
				type = '系统订单';
			} else {
				type = (v.is_give == 1) ? '代报订单' : '手工订单';
			}
			//经过函数处理下格式
			v.amount = number_format(v.amount);
			var table = $('ul.order_tab li.active').index();
			html += '<tr>';
			html += '<td>'+v.order_id+'</td>';
			html += '<td><a class="detail" href="'+URL.detail_url+v.order_id+'/'+table+'">'+v.order_number+'</td></a>';
			html += '<td>'+v.username+'</td>';
			//html += '<td>12345678923</td>';
			html += '<td>'+type+'</td>';
			html += '<td>'+v.amount+'</td>';
			html += '<td>'+v.orders_time+'</td>';
			html += '<td>'+v.pattern+'</td>';
			html += '<td>'+v.form+'</td>';
			html += '<td>';
			html += '<a class="detail" href="'+URL.detail_url+v.order_id+'/'+table+'">查看</a>　';
			html += '<select class="handle" data-id='+v.order_id+'>';
			if(table == 0) {
				html += '<option value ="">更多</option>';
				html += '<option value="edit">修改</option>';
				html += '<option value="delete">删除</option>';
				//系统订单才能做审核操作
				if(v.is_system == 0) html += '<option value="check">审核</option>';
			} else if(table == 1) {
				html += '<option value ="">更多</option>';
				html += '<option value="delete">删除</option>';
				//系统订单才能做取消审核操作
				if(v.is_system == 0) html += '<option value="cancel">取消审核</option>';	
			} else if(table == 2) {
				html += '<option value ="">更多</option>';
				html += '<option value="restore">还原</option>';	
			}
			html += '</select>';
			html += '</td>';
			html += '</tr>';
		})
	} else if(action = "mail_list") {
		$.each(data['list'],function (k,v) {
			var type = '';
			var mail_status = '';
			var disabled = '';
			if(v.is_system == 1) {
				type = '系统订单';
			} else {
				type = (v.is_give == 1) ? '代报订单' : '手工订单';
			}
			
			if(v.mail_status == 1) { 
				mail_status = '已邮寄';
			} else {
				mail_status = '未邮寄';
			};
			//经过函数处理下格式
			v.amount = number_format(v.amount);
			
			html += '<tr>';
			html += '<td><input class="checkbox" type="checkbox" data-id="'+v.order_id+'" data-num='+v.order_number+' data-mail_status="'+v.mail_status+'"/></td>';
			html += '<td>'+v.order_id+'</td>';
			html += '<td><a class="detail" href="'+URL.detail_url+v.order_id+'">'+v.order_number+'</a></td>';
			html += '<td class="username">'+v.username+'</td>';
			html += '<td>'+v.telephone+'</td>';
			html += '<td>'+type+'</td>';
			html += '<td>'+v.amount+'</td>';
			html += '<td>'+v.orders_time+'</td>';
			html += '<td>'+mail_status+'</td>';
			html += '<td>';
			html += '<a class="detail" href="'+URL.detail_url+v.order_id+'">查看</a>　';
			html += (v.mail_status == 1) ? '<a class="delete_mail" data-id="'+v.order_id+'">删除邮寄信息</a>' : '<a class="create_mail" data-id="'+v.order_id+'" data-username='+v.username+'>添加邮寄信息</a>';
			html += '</td>';
			html += '</tr>';
		})
	}
	return html;
}

//添加资源的所有事件逻辑方法(dom加载完执行)
function add_resource($,w) {
	var ueditor = UM.getEditor('attr_content');
	
	/*点击属性标签*/
	$('.resource_button').on('click','li',function () {
		var _this = $(this);
		var name = (_this.data('name') == undefined) ? $('.attr_name div.name').attr('placeholder') : _this.data('name');
		var content = (_this.data('content') == undefined) ? '' : _this.data('content');
		if(_this.hasClass('active')) return ;
		
		
		_this.addClass('active').siblings().removeClass('active');
		_this.parents('.attr_list').next().find('div.name').text(name).next().val(name);
		ueditor.setContent(content);
	})
	
	//编辑器失去焦点
	ueditor.addListener('blur',function () {
		var li = $('.resource_button li.active');
		var content = $.trim(ueditor.getContent());
		//如果编辑器有内容,则保存到标签
		if(ueditor.hasContents()) {
			li.data('content',content);	
		} else {
			li.removeData('content').removeAttr('data-content');;	
		}
	})
	
	//编辑器失去焦点
	ueditor.addListener('focus',function () {
		$('.attr_list .error_list').hide();
	})

	//添加资源(ajax添加)
	$('#add_resource').on('click',function () {

		var _this = $(this);	
		var error_div = $('.upload_div').find('.error_div');
		var arr = {
			'resource_name':'资源名称',
			'type':'资源类型',
			/*'resource_url':'资源地址',
			'token':'token',
			'download_url':'下载地址',*/
			'author':'授课老师',
			'startime':'有效期',
			'endtime':'有效期'
		};
		
		var data = {};
		var coverurl = $('#change_img').data('coverurl');
		//先检查封面
		if(coverurl == undefined || coverurl =='') {
			/*error_div.show().text('请上传封面!');
			return ;*/
		} else {
			data['cover_url'] = coverurl;
		}
		var info = obj ='';
		
		//检查表单
		$('.form_list input,.form_list select').each(function (k,v) {
			obj = $(v);
			var name = obj.attr('name');
			data[name] = obj.val();
			if(arr[name]) {
				if(obj.hasClass('text') || obj.hasClass('time')) {
					if(obj.val() == '') {info = arr[name]+'不能为空!';return false;}
				} else if(obj.hasClass('select')) {
					if(obj.val() == '0') {info = '请选择'+arr[name]+'!';return false;}
				}
			}
		})
		
		if(info) {form_error(obj,info);return ;}
		
		var attr = {};

		//检查资源扩展,必须名称和内容全部填写,要么就不填
		$('.resource_button li').each(function (k,v) {
			var element = $(this);
			var attr_name = (element.data('name') == undefined) ? '' : element.data('name');
			var content = (element.data('content') == undefined) ? '' : element.data('content');
			
			//如果扩展名和扩展内容有一样为空,则弹出提示信息(因为扩展信息要么不填,要么填写完整)
			if((attr_name == '' && content !== '') || (content == '' && attr_name !== '')) {
				info = '请把资源扩展'+(k+1)+'填写完整!';	
				return false;
			} else if(attr_name !=='' && content !== '') {
				attr[attr_name] = content;
			}
			
		})
		if(info) {$('.attr_list .error_list').show().find('.error_div').text(info);return ;}
		
		if(_this.data('isClick')) {
			return ;	
		} else {
			_this.data('isClick',1);
		}
		
		data['attrs_list'] = attr;
		$.post(_this.data('url'),data,function (res) {
				alert(res.info);
				_this.removeData('isClick');
				window.location.href=URL.list_url;
				/*if(res.status) {
					window.location.href = "/admin/resourceList";
					window.history.back()
				} else {
					alert(res.info);
				}*/
		})
	})
	
	
	//添加资源(ajax添加)
	$('#edit_resource').on('click',function () {

		var _this = $(this);	
		var error_div = $('.upload_div').find('.error_div');
		var arr = {
			'resource_name':'资源名称',
			'type':'资源类型',
			/*'resource_url':'资源地址',
			'token':'token',
			'download_url':'下载地址',*/
			'author':'授课老师',
			'startime':'有效期',
			'endtime':'有效期'
		};
		
		var data = {};
		var coverurl = $('#change_img').data('coverurl');
		//先检查封面
		if(coverurl == undefined || coverurl =='') {
			/*error_div.show().text('请上传封面!');
			return ;*/
		} else {
			data['cover_url'] = coverurl;
		}
		var info = obj ='';
		
		//检查表单
		$('.form_list input,.form_list select').each(function (k,v) {
			obj = $(v);
			var name = obj.attr('name');
			data[name] = obj.val();	
			if(arr[name]) {
				if(obj.hasClass('text') || obj.hasClass('time')) {
					if(obj.val() == '') {info = arr[name]+'不能为空!';return false;}
				} else if(obj.hasClass('select')) {
					if(obj.val() == '0') {info = '请选择'+arr[name]+'!';return false;}
				}
			}
		})
		
		if(info) {form_error(obj,info);return ;}
		
		var attr = {};

		//检查资源扩展,必须名称和内容全部填写,要么就不填
		$('.resource_button li').each(function (k,v) {
			var element = $(this);
			var attr_name = (element.data('name') == undefined) ? '' : element.data('name');
			var content = (element.data('content') == undefined) ? '' : element.data('content');
			
			//如果扩展名和扩展内容有一样为空,则弹出提示信息(因为扩展信息要么不填,要么填写完整)
			if((attr_name == '' && content !== '') || (content == '' && attr_name !== '')) {
				info = '请把资源扩展'+(k+1)+'填写完整!';	
				return false;
			} else if(attr_name !=='' && content !== '') {
				attr[attr_name] = content;
			}
			
		})
		if(info) {$('.attr_list .error_list').show().find('.error_div').text(info);return ;}
		
		if(_this.data('isClick')) {
			return ;	
		} else {
			_this.data('isClick',1);
		}
		data['attrs_list'] = attr;
		data['resource_id'] = parseInt(_this.data('id'));
		
		$.post(_this.data('url'),data,function (res) {
				/*if(res.status) {
					window.history.back()
				} else {
					alert(res.info);
				}*/

				alert(res.info);
				_this.removeData('isClick');
				window.location.href=URL.list_url;
		})
	})
}

//添加商品的所有事件逻辑方法(dom加载完执行)
function add_goods($,w) {
	
	var ueditor = UM.getEditor('attr_content');
	
	//点击属性标签
	$('.resource_button').on('click','li',function () {
		var _this = $(this);
		var name = (_this.data('name') == undefined) ? $('.attr_name div.name').attr('placeholder') : _this.data('name');
		var content = (_this.data('content') == undefined) ? '' : _this.data('content');
		if(_this.hasClass('active')) return ;
		_this.addClass('active').siblings().removeClass('active');
		_this.parents('.attr_list').next().find('div.name').text(name).next().val(name);
		ueditor.setContent(content);
	})
	
	//编辑器失去焦点
	ueditor.addListener('blur',function () {
		var li = $('.resource_button li.active');
		var content = $.trim(ueditor.getContent());
		//如果编辑器有内容,则保存到标签
		if(ueditor.hasContents()) {
			li.data('content',content);	
		} else {
			li.removeData('content').removeAttr('data-content');;	
		}
	})
	
	//编辑器获取焦点
	ueditor.addListener('focus',function () {
		$('.attr_list .error_list').hide();
	})
	
	//选择分类时候去掉提示信息
	$('select[name=nav]').on('change',function () {
		var _this = $(this);
		
		//选择了分类,则提示信息才会消失
		if(_this.val !== 0) $(this).parents('.form_list').nextAll('.error_list').hide();
	})
	
	//添加商品(ajax添加)
	$('#change_resource').on('click',function () {
		var _this = $(this);	
		var error_div = $('.upload_div').find('.error_div');
		var arr = {
			'goods_name':'商品名称',
			'type':'商品类型',
			'price':'商品价格',
			/*'hours':'学时',
			'quota_num':'限购数量',*/
			'startime':'有效期',
			'endtime':'有效期',
			'nav':'所属分类'
		};
		
		var data = {};
		var coverurl = $('#change_img').data('coverurl');
		//先检查封面
		if(coverurl == undefined || coverurl =='') {
			error_div.show().text('请上传封面!');
			return ;
		} else {
			data['cover_url'] = coverurl;
		}
		var info = obj ='';
		//检查表单
		$('.form_list input,.form_list select').each(function (k,v) {
			obj = $(v);
			var name = obj.attr('name');
			data[name] = obj.val();	
			if(arr[name]) {
				if(obj.hasClass('text') || obj.hasClass('time')) {
					if(obj.val() == '') {info = arr[name]+'不能为空!';return false;}
				} else if(obj.hasClass('select')) {
					if(obj.val() == '0') {info = '请选择'+arr[name]+'!';return false;}
				}
			}
		})

		data['mail'] = parseInt($('input[name=mail]:checked').val());
		data['stick'] =  parseInt($('input[name=stick]:checked').val());
		
		if(info) {form_error(obj,info);return ;}
		
		var attr = {};

		//检查资源扩展,必须名称和内容全部填写,要么就不填
		$('.resource_button li').each(function (k,v) {
			var element = $(this);
			var attr_name = (element.data('name') == undefined) ? '' : element.data('name');
			var content = (element.data('content') == undefined) ? '' : element.data('content');
			
			if((attr_name == '' && content !== '') || (content == '' && attr_name !== '')) {
				info = '请把商品扩展'+(k+1)+'填写完整!';
				return false;
			} else if(attr_name !=='' && content !== '') {
				attr[attr_name] = content;
			} else {
				info = '商品扩展不能为空!';
				return false;
			}
			
		})
		
		if(info) {$('.attr_list .error_list').show().find('.error_div').text(info);return ;}
		if(_this.data('isClick')) {
			return ;	
		} else {
			_this.data('isClick',1);
		}
		data['attrs_list'] = attr;
	
		$.post(_this.data('url'),data,function (res) {
				if(res.status) {
					window.location.href = URL.change_resource+res.goods_id;
				} else {
					alert(res.info);
				}
				_this.removeData('isClick');
		})
	})
	
	//修改商品(ajax添加)
	$('#edit_goods').on('click',function () {
		var _this = $(this);	
		var error_div = $('.upload_div').find('.error_div');
		var arr = {
			'goods_name':'商品名称',
			'type':'商品类型',
			'price':'商品价格',
			'hours':'学时',
			/*'quota_num':'限购数量',*/
			'startime':'有效期',
			'endtime':'有效期',
			'nav':'所属分类'
		};
		
		var data = {};
		var coverurl = $('#change_img').data('coverurl');
		//先检查封面
		if(coverurl == undefined || coverurl =='') {
			error_div.show().text('请上传封面!');
			return ;
		} else {
			data['cover_url'] = coverurl;
		}
		var info = obj ='';
		//检查表单
		$('.form_list input,.form_list select').each(function (k,v) {
			obj = $(v);
			var name = obj.attr('name');
			data[name] = obj.val();	
			if(arr[name]) {
				if(obj.hasClass('text') || obj.hasClass('time')) {
					if(obj.val() == '') {info = arr[name]+'不能为空!';return false;}
				} else if(obj.hasClass('select')) {
					if(obj.val() == '0') {info = '请选择'+arr[name]+'!';return false;}
				}
			}
		})
		
		data['mail'] = parseInt($('input[name=mail]:checked').val());
		data['stick'] =  parseInt($('input[name=stick]:checked').val());
	
		if(info) {form_error(obj,info);return ;}
		
		var attr = {};

		//检查资源扩展,必须名称和内容全部填写,要么就不填
		$('.resource_button li').each(function (k,v) {
			var element = $(this);
			var attr_name = (element.data('name') == undefined) ? '' : element.data('name');
			var content = (element.data('content') == undefined) ? '' : element.data('content');
			
			if((attr_name == '' && content !== '') || (content == '' && attr_name !== '')) {
				info = '请把商品扩展'+(k+1)+'填写完整!';
				return false;
			} else if(attr_name !=='' && content !== '') {
				attr[attr_name] = content;
			} else {
				info = '商品扩展不能为空!';
				return false;
			}
			
		})
		
		if(info) {$('.attr_list .error_list').show().find('.error_div').text(info);return ;}
		
		if(_this.data('isClick')) {
			return ;	
		} else {
			_this.data('isClick',1);
		}
		data['attrs_list'] = attr;
		data['goods_id'] = parseInt(_this.data('id'));

		$.post(_this.data('url'),data,function (res) {
				_this.removeData('isClick');
				//if(res.status) {
				window.location.href = URL.change_resource+data['goods_id'];
				//} else {
					//alert(res.info);
				//	}
		})
	})
}

//选择资源
function change_resource($,w) {
	//点击选择资源,弹出弹窗
	$('#change').on('click',function () {
		$('#dialog').show();
		$('#change_list').show();
		
		//获取第一页的数据
		reload_list(1);
	})
	
	//上一页
	$('.page_handle').on('click','button.prev',function () {
		var _this = $(this)
		var page_info = _this.siblings('span.num').text().split('/');
		var page = parseInt(page_info[0]);
		var maxNum = parseInt(page_info[1]);
		
		//如果当前页码是1,则禁止上一页
		if(page == 1 || maxNum == 1) return ;
		var page = page - 1;
		//请求列表数据
		reload_list(page);
	})
	
	
	//下一页
	$('.page_handle').on('click','button.next',function () {
		var _this = $(this)
		var page_info = _this.siblings('span.num').text().split('/');
		var page = parseInt(page_info[0]);
		var maxNum = parseInt(page_info[1]);
		
		//如果已经是最后一页则禁止请求分页属性
		if(page == maxNum || maxNum == 1) return ;
		var page = page + 1;
		reload_list(page);
	})
	
	//搜索
	$('#search').on('click',function () {
		var _this = $(this);
		var search_data = {};
		var search_field = $('select[name=search_field]');
		var search_text = $('.search_text');
		
		//如果搜索条件不为空,则放入对象
		if(search_field.val() !== '' && search_text.val() !== '') {
			search_data[search_field.val()]	= search_text.val();
		}
		
		//如果是空对象,则删除掉搜索按钮上的数据
		if(isEmptyObject(search_data)) {
			_this.removeData('search_data');
			return ;
		//否则把搜索数据记录在搜索按钮上
		} else {
			_this.data('search_data',JSON.stringify(search_data));	
		}
		
		request_list(search_data);
	})
	
	//输入框分页
	$('input.page').on('focus',function () {
		$(window).off('keydown').on('keydown',function (e){
			if(e.keyCode == 13) {
				var page_info = $('span.num').text().split('/');
				var page = parseInt(page_info[0]);
				var input_page = parseInt($('input.page').val());
				var maxNum = parseInt(page_info[1]);
				
				if(maxNum == 1) return ;
				
				//如果输入的就是当前页或者最大页数是1,则不请求
				if(input_page == page || maxNum == 1) {
					return ;
					
				//如果输入页数小于1,当1处理	
				} else if(input_page < 1) {
					input_page = 1;	
					
				//如果超过最大页数,则当最后一页处理
				} else if(input_page > maxNum) {
					input_page = maxNum;	
				}
				
				var data = {};
				if($('#search').data('search_data') !== undefined) {
					data = JSON.parse($('#search').data('search_data'));
				}
				data['page'] = input_page;
				//请求列表数据
				request_list(data);
			}	
		})
	})
	//根据页码重载页面
	function reload_list(page) {
		//删除后重载列表
		var data = {};
		if($('#search').data('search_data') !== undefined) {
			data = JSON.parse($('#search').data('search_data'));
		}
		data['page'] = page;
		request_list(data);	
	}
	
	
	//根据页码请求资源列表数据
	function request_list(data) {
		data['page'] = data['page'] ? data['page'] : 1;
		
		//记录已选择的资源ID,不会查询它们
		data['ids'] = {};
		$('a.optout').each(function (k,v) {
			data['ids'][k] = $(this).data('id');
		})
		
		$.post(URL.miniResourceList,data,function (res) {
			var response = res['data'];
			if(data['page'] == response.page) {
				//清空tbody
				$('#change_list tbody').empty();
				
				//加载列表
				$('#change_list tbody').html(load_list(response,'mini_resource_list'));
				
				//加载分页信息
				var pageNum = Math.ceil(response.count/response.pageSize);
				if(pageNum == 0) pageNum = 1;				
				$('div.page_info').text('共'+response.count+'条记录,每页'+response.pageSize+'条,共'+pageNum+'页');
				$('input.page').val(response.page);
				$('span.num').text(response.page+'/'+pageNum);
				$('#change_all').attr('checked',false);
			}
		})
	}
	
	//确定添加资源
	$('#change_list').on('click','.confirm',function () {
		var _this = $(this);
		var ids = _this.data('ids');
		if(ids !== undefined) {
			if(ids.length > 0) {
				$.post(URL.relationResource,{ids:ids,goods_id:_this.data('id')},function (res) {
					if(res.status) {
						window.location.reload();	
					} else {
						alert(res.info);	
					}	
				})
			} else {
				$('#change_list .cancel').click();	
			}
		} else {
			$('#change_list .cancel').click();
		}
	})
	
	//选中要添加的资源
	$('#change_list').on('change','div.content tbody .checkbox',function () {
		var _this = $(this);
		var id = _this.data('id');
		var button = $('#change_list').find('.confirm');
		var ids = button.data('ids');

		//选中则添加到集合
		if(_this.attr('checked')) {
			//如果没有存储的id集合,则创建对象
			if(ids == undefined) ids = [];
			ids.push(id);
			button.data('ids',ids);
		//未选中则从集合中删除
		} else {
			ids.splice($.inArray(id,ids),1); 
		}
	})
	
	//撤出资源
	$('#content').on('click','a.optout',function (res) {
		//optoutResource
		var data = {};
		data['id'] = $(this).data('id');
		data['goods_id'] = $('#change_list .confirm').data('id');
		
		$.post(URL.optoutResource,data,function (res) {
			if(res.status) {
				window.location.reload();	
			} else {
				alert(res.info);	
			}
		})
	})
	
	//已选择资源列表全选
	$('#all').on('change',function () {
		var _this = $(this);
		
		if(_this.attr('checked')) {
			$('div#content tbody .checkbox').attr('checked',true).change();	
		} else {
			$('div#content tbody .checkbox').attr('checked',false).change();		
		}
	})
	
	//弹窗全选
	$('#change_all').on('change',function () {
		var _this = $(this);
		
		if(_this.attr('checked')) {
			$('div.content tbody .checkbox').attr('checked',true).change();	
		} else {
			$('div.content tbody .checkbox').attr('checked',false).change();		
		}
	})
	
	//批量撤出资源
	$('#repeal').on('click',function () {
		var _this = $(this);
		var checked = $('div#content tbody .checkbox:checked');
		if(checked.size() == 0) return ;
		
		var ids = [];
		checked.each(function () {
			ids.push($(this).data('id'));	
		})
		
		$.post(URL.repealResource,{goods_id:_this.data('id'),ids:ids},function (res) {
			if(res.status) {
				window.location.reload();	
			} else {
				alert(res.info);	
			}	
		})
	})
}

//创建订单
function add_order($,w) {
	var list_url = URL.list_url;
	
	//上一页
	$('.page_handle').on('click','button.prev',function () {
		var _this = $(this)
		var page_info = _this.siblings('span.num').text().split('/');
		var page = parseInt(page_info[0]);
		var maxNum = parseInt(page_info[1]);
		
		//如果当前页码是1,则禁止上一页
		if(page == 1 || maxNum == 1) return ;
		var page = page - 1;
		//请求列表数据
		reload_list(page);
	})
	
	
	//下一页
	$('.page_handle').on('click','button.next',function () {
		var _this = $(this)
		var page_info = _this.siblings('span.num').text().split('/');
		var page = parseInt(page_info[0]);
		var maxNum = parseInt(page_info[1]);
		
		//如果已经是最后一页则禁止请求分页属性
		if(page == maxNum || maxNum == 1) return ;
		var page = page + 1;
		reload_list(page);
	})
	
	//搜索
	$('#search').on('click',function () {
		var _this = $(this);
		var search_data = {};
		var search_field = $('select[name=search_field]');
		var search_text = $('.search_text');
		
		//如果搜索条件不为空,则放入对象
		if(search_field.val() !== '' && search_text.val() !== '') {
			search_data[search_field.val()]	= search_text.val();
		}
		
		//如果是空对象,则删除掉搜索按钮上的数据
		if(isEmptyObject(search_data)) {
			_this.removeData('search_data');
			return ;
		//否则把搜索数据记录在搜索按钮上
		} else {
			_this.data('search_data',JSON.stringify(search_data));	
		}
		
		request_list(search_data);
	})
	
	//输入框分页
	$('input.page').on('focus',function () {
		$(window).off('keydown').on('keydown',function (e){
			if(e.keyCode == 13) {
				var page_info = $('span.num').text().split('/');
				var page = parseInt(page_info[0]);
				var input_page = parseInt($('input.page').val());
				var maxNum = parseInt(page_info[1]);
				
				if(maxNum == 1) return ;
				
				//如果输入的就是当前页或者最大页数是1,则不请求
				if(input_page == page || maxNum == 1) {
					return ;
					
				//如果输入页数小于1,当1处理	
				} else if(input_page < 1) {
					input_page = 1;	
					
				//如果超过最大页数,则当最后一页处理
				} else if(input_page > maxNum) {
					input_page = maxNum;	
				}
				
				var data = {};
				if($('#search').data('search_data') !== undefined) {
					data = JSON.parse($('#search').data('search_data'));
				}
				data['page'] = input_page;
				//请求列表数据
				request_list(data);
			}	
		})
	})
	//根据页码重载页面
	function reload_list(page) {
		//删除后重载列表
		var data = {};
		if($('#search').data('search_data') !== undefined) {
			data = JSON.parse($('#search').data('search_data'));
		}
		data['page'] = page;
		request_list(data);	
	}
	
	
	//根据页码请求资源列表数据
	function request_list(data) {
		data['page'] = data['page'] ? data['page'] : 1;
		
		//记录已选择的资源ID,不会查询它们
		data['ids'] = {};
		$('.goods_list li').each(function (k,v) {
			data['ids'][k] = $(this).data('id');
		})
		
		$.post(list_url,data,function (res) {
			var response = res['data'];
			if(data['page'] == response.page) {
				//清空tbody
				$('#change_list tbody').empty();
				
				//加载列表
				$('#change_list tbody').html(load_list(response,'mini_goods_list'));
				
				//加载分页信息
				var pageNum = Math.ceil(response.count/response.pageSize);
				if(pageNum == 0) pageNum = 1;				
				$('div.page_info').text('共'+response.count+'条记录,每页'+response.pageSize+'条,共'+pageNum+'页');
				$('input.page').val(response.page);
				$('span.num').text(response.page+'/'+pageNum);
			}
		})
	}
	
	//选择商品
	$('#change_goods').on('click',function () {
		//if($('ul.goods_list').data('status') !== 0 || $('ul.goods_list').data('status') == undefined) return ;
		$('#dialog').show();
		$('#change_list').show();
		$('div.goods_list').find('.error_list').hide();
		//获取第一页的数据
		reload_list(1);	
	})
	
	//确定添加商品
	$('#change_list').on('click','.confirm',function () {
		
		var goods_info = $(this).data('goods_info');
		
		if(goods_info !== undefined) {
			if(!isEmptyObject(goods_info)) {
				var html = '';
				$.each(goods_info,function (k,v) {
					if(v.name.length > 10) v.name = v.name.substr(0,10)+'...';	
					html += '<li title = "'+v.name+'" data-id="'+k+'" data-mail="'+v.mail+'">'+v.name;
					if(v.mail ==1) {
						$('button.handle').data('is_mail',1);
						$('input[name=receiver],input[name=telephone],input[name=address]').val('').parents('.form_list').removeClass('none');
						html += '<img src="'+URL.mail_img+'" class="mail"/>';
					}
					html += '</li>';
				})
				$('ul.goods_list').append(html);
			}
			$('#change_list .cancel').click();		
		} else {
			$('#change_list .cancel').click();	
		}
	})
	
	//删除商品
	$('ul.goods_list').on('click','img.delete',function () {
		var _this = $(this);
		//if($('ul.goods_list').data('status') !== 0 || $('ul.goods_list').data('status') == undefined) return ;
		$(this).parent().remove();
		
		if($('ul.goods_list li[data-mail="1"]').size() == 0){
			$('button.handle').data('is_mail',0);
			$('input[name=receiver],input[name=telephone],input[name=address]').val('').parents('.form_list').addClass('none');
		} else {
			$('button.handle').data('is_mail',1);
			$('input[name=receiver],input[name=telephone],input[name=address]').val('').parents('.form_list').removeClass('none');
		}
	})
	
	//选中商品
	$('#change_list').on('click','.checkbox',function () {
		var _this = $(this);
		var id = _this.data('id');
		var name = _this.data('name');
		var mail = _this.data('mail');
		var button = $('#change_list').find('.confirm');
		var goods_info = button.data('goods_info');
		
		if($('button.handle').data('is_mail') == 0 && $('button.handle').data('is_mail')!==_this.data('mail')) $('button.handle').data('is_mail',_this.data('mail'));
		
		//选中则添加到集合
		if(_this.attr('checked')) {
			var obj = {};
			
			//如果没有存储的id集合,则创建对象
			if(goods_info == undefined) goods_info = {};
			obj['name'] = name;
			obj['mail'] = mail;//
			goods_info[id] = obj;
			button.data('goods_info',goods_info);
			
		//未选中则从集合中删除
		} else {
			delete goods_info[id];
		}
	})
	
	//验证用户名
	$('input[name=username]').on('blur',function () {
		var _this = $(this);
		var username = $.trim(_this.val());	
		
		$.post(URL.check_user,{username:username},function (res) {
			if(res.data.username == username) {
				if(res.status) {
					_this.data('user_id',res.data.uid)
				} else {
					form_error(_this,res.info);
				}
			}
		})
	})

	//如果值有改变,则删除user_id
	$('input[name=username]').on('change',function () {
		$(this).removeData('user_id').removeAttr('data-user_id');
	})
	
	//移入
	$('ul.goods_list').on('mouseover','li',function () {
		var _this = $(this);
		if(_this.find('img').size() == 0) {
			_this.append('<img/>');	
		}
		_this.addClass('active').siblings().removeClass('active');
		_this.find('img').attr({'src':URL.delete_img,'class':'delete'}).show();	
	})
	//移出
	$('ul.goods_list').on('mouseout','li',function () {
		var _this = $(this);
		
		if(_this.data('mail') == 1) {
			_this.removeClass('active').find('img').attr({'src':URL.mail_img,'class':'mail'});	
		} else {
			_this.removeClass('active').find('img').removeAttr('class').hide();	
		}
	})
	
	
	//添加订单
	$('#create_order').on('click',function () {
		var _this = $(this);
		var is_mail = _this.data('is_mail');
		var ul = $('ul.goods_list');
		var goods = $('ul.goods_list li');
		
		var data = {};

		//如果没有验证用户名,则禁止往下执行
		if($('input[name=username]').data('user_id') == undefined) return ;
		data['user_id'] = $('input[name=username]').data('user_id');

		//先验证商品
		if(ul.find('li').size() == 0) {
			ul.siblings('.error_list').show().find('.error_div').text('请选择商品!');	
			return ;
		} else {
			var ids = [];
			ul.find('li').each(function (k,v) {
				ids.push($(v).data('id'));	
			})
			data['goods_ids'] = ids;
		}
		
		var info = obj ='';
		var arr = {
			'order_type':'订单类型',
			'username':'用户名',
			'receiver':'收件人',
			'telephone':'联系电话',
			'address':'收货地址',
			'orders_time':'下单地址',
			'amount':'订单金额',
			'pattern':'支付方式',
			'form':'购买来源',
			'order_explain':'订单说明'
		}
		//检查表单
		$('.form_list input,.form_list select').each(function (k,v) {
			obj = $(v);
			var name = obj.attr('name');
			data[name] = obj.val();	
			if(arr[name]) {
				if(obj.hasClass('text')) {
					//如果是邮寄订单
					if(is_mail == 1) {
						if(obj.val() == '' ) {info = arr[name]+'不能为空!';return false;}
					//如果是非邮寄订单
					} else {
						//则不验证 收件人 联系电话  收货地址的信息
						if($.inArray(name,['receiver','telephone','address']) > -1) {
							return true;
						} else {
							if(obj.val() == '' ) {info = arr[name]+'不能为空!';return false;}	
						}
					}
				} else if(obj.hasClass('select')) {
					if(obj.val() == '0') {info = '请选择'+arr[name]+'!';return false;}
				}
			}
		})
		
		if(info) {form_error(obj,info);return ;}
		if(_this.data('isClick')) {
			return ;	
		} else {
			_this.data('isClick',1);
		}
		
		
		$.post(URL.create_url,data,function (res) {
			if(res.status == 1) {
				alert(res.info);
				window.location.href=URL.back_list;
				//window.history.back();
			} else {
				alert(res.info);
				//window.location.href=URL.back_list;
			}
			_this.removeData('isClick');
		})
	})
	
	//修改订单
	$('#update_order').on('click',function () {
		var _this = $(this);
		var is_mail = _this.data('is_mail');
		var ul = $('ul.goods_list');
		var goods = $('ul.goods_list li');
		
		var data = {};

		//如果没有验证用户名,则禁止往下执行
		if($('input[name=username]').data('user_id') == undefined) return ;
		data['user_id'] = $('input[name=username]').data('user_id');

		//先验证商品
		if(ul.find('li').size() == 0) {
			ul.siblings('.error_list').show().find('.error_div').text('请选择商品!');	
			return ;
		} else {
			var arr = [];
			ul.find('li').each(function () {
				var goods_infos = {};
				var __this = $(this);
				goods_infos['goods_id'] = parseInt(__this.data('id'));
				
				goods_infos['snap_id'] = (__this.data('snap_id') == undefined) ? '' : parseInt(__this.data('snap_id'));
				arr.push(goods_infos);	
			})
			data['goods_infos'] = arr;
		}
		
		var info = obj ='';
		var arr = {
			'order_type':'订单类型',
			'username':'用户名',
			'receiver':'收件人',
			'telephone':'联系电话',
			'address':'收货地址',
			'orders_time':'下单地址',
			'amount':'订单金额',
			'pattern':'支付方式',
			'order_explain':'订单说明'
		}
		
		//检查表单
		$('.form_list input,.form_list select').each(function (k,v) {
			obj = $(v);
			var name = obj.attr('name');
			data[name] = obj.val();	
			if(arr[name]) {
				if(obj.hasClass('text')) {
					//如果是邮寄订单
					if(is_mail == 1) {
						if(obj.val() == '' ) {info = arr[name]+'不能为空!';return false;}
					//如果是非邮寄订单
					} else {
						//则不验证 收件人 联系电话  收货地址的信息
						if($.inArray(name,['receiver','telephone','address']) > -1) {
							return true;
						} else {
							if(obj.val() == '' ) {info = arr[name]+'不能为空!';return false;}	
						}
					}
				} else if(obj.hasClass('select')) {
					if(obj.val() == '0') {info = '请选择'+arr[name]+'!';return false;}
				}
			}
		})
		
		if(info) {form_error(obj,info);return ;}
		if(_this.data('isClick')) {
			return ;	
		} else {
			_this.data('isClick',1);
		}
		data['id'] = _this.data('id');
		
		$.post(URL.edit_url,data,function (res) {
			if(res.status == 1) {
				alert(res.info);
				window.location.href=URL.back_list;
				//window.history.back();
			//2为用户名错误
			} else if(res.status == 2) {
				form_error($('input[name=username]'),res.info);	
			} else {
				alert(res.info);
				window.location.href=URL.back_list;
			}
			_this.removeData('isClick');
		})
	})
}

//订单列表
function order_list($,w) {
	var list_url = URL.list_url;
	//展开更多搜索条件
	$('#more').toggle(function () {
		$('div.order_time').removeClass('none');
		$('div.amount').removeClass('none');
		$('select.pattern').removeClass('none');
		$('select.form').removeClass('none');
		$('#more').text('收起');
	},function () {
		$('div.order_time').addClass('none');
		$('div.amount').addClass('none');
		$('select.pattern').addClass('none');
		$('select.form').addClass('none');
		$('#more').text('更多');
	})
	
	//切换标签
	$('ul.order_tab').on('click','li',function () {
		var _this = $(this);
		if(_this.hasClass('active')) return ;
		_this.addClass('active').siblings().removeClass('active');
		$('.search_text').val('');
		$('#startime').val('');
		$('#endtime').val('');
		$('#min_amount').val('');
		$('#max_amount').val('');
		$('select[name=form]').val(0);
		$('select[name=pattern]').val(0);
		//$('#search').removeData('search_data');
		request_list({page:1});
	})
	
	//上一页
	$('.page_handle').on('click','button.prev',function () {
		var _this = $(this)
		var page_info = _this.siblings('span.num').text().split('/');
		var page = parseInt(page_info[0]);
		var maxNum = parseInt(page_info[1]);
		
		//如果当前页码是1,则禁止上一页
		if(page == 1 || maxNum == 1) return ;
		var page = page - 1;
		//请求列表数据
		reload_list(page);
	})
	
	
	//下一页
	$('.page_handle').on('click','button.next',function () {
		var _this = $(this)
		var page_info = _this.siblings('span.num').text().split('/');
		var page = parseInt(page_info[0]);
		var maxNum = parseInt(page_info[1]);
		
		//如果已经是最后一页则禁止请求分页属性
		if(page == maxNum || maxNum == 1) return ;
		var page = page + 1;
		reload_list(page);
	})
	
	//搜索
	$('#search').on('click',function () {
		var _this = $(this);
		var search_data = {};
		var search_field = $('select[name=search_field]');
		var search_text = $('.search_text');
		var order_time = $('div.order_time');
		var amount = $('div.amount');
		var pattern = $('select.pattern');
		
		//如果搜索条件不为空,则放入对象
		if(search_field.val() !== '' && search_text.val() !== '') {
			search_data[search_field.val()]	= search_text.val();
		}
		
		//获取下单时间order_time
		if(order_time.is(':visible') && ($('#startime').val()!=='' || $('#endtime').val()!=='')) {
			var obj = {};
			obj['startime'] = $('#startime').val();
			obj['endtime'] = $('#endtime').val();
			search_data['order_time'] = obj;
			obj = null;
		}
		
		//获取下单金额order_time
		if(amount.is(':visible') && ($('#min_amount').val()!=='' || $('#max_amount').val()!=='')) {
			var obj = {};
			obj['min_amount'] = $('#min_amount').val();
			obj['max_amount'] = $('#max_amount').val();
			search_data['amount'] = obj;
			obj = null;
		}
		
		//获取下拉搜索条件
		$('select.pattern,select.form').each(function () {
			var __this = $(this);
			var value = __this.val();
			if(value !== '0') {
				search_data[__this.attr('name')] = value;	
			}	
		})
		
		//如果是空对象,则删除掉搜索按钮上的数据
		if(isEmptyObject(search_data)) {
			_this.removeData('search_data');
			return ;
		//否则把搜索数据记录在搜索按钮上
		} else {
			_this.data('search_data',JSON.stringify(search_data));	
		}
		
		request_list(search_data);
	})
	
	//输入框分页
	$('input.page').on('focus',function () {
		$(window).off('keydown').on('keydown',function (e){
			if(e.keyCode == 13) {
				var page_info = $('span.num').text().split('/');
				var page = parseInt(page_info[0]);
				var input_page = parseInt($('input.page').val());
				var maxNum = parseInt(page_info[1]);
				
				if(maxNum == 1) return ;
				
				//如果输入的就是当前页或者最大页数是1,则不请求
				if(input_page == page || maxNum == 1) {
					return ;
					
				//如果输入页数小于1,当1处理	
				} else if(input_page < 1) {
					input_page = 1;	
					
				//如果超过最大页数,则当最后一页处理
				} else if(input_page > maxNum) {
					input_page = maxNum;	
				}
				
				var data = {};
				if($('#search').data('search_data') !== undefined) {
					data = JSON.parse($('#search').data('search_data'));
				}
				data['page'] = input_page;
				//请求列表数据
				request_list(data);
			}	
		})
	})
	
	//根据页码重载页面
	function reload_list(page) {
		//删除后重载列表
		var data = {};
		if($('#search').data('search_data') !== undefined) {
			data = JSON.parse($('#search').data('search_data'));
		}
		data['page'] = page;
		request_list(data);	
	}
	
	//根据页码请求资源列表数据
	function request_list(data) {
		//搜索分类
		data['table_type'] = $('ul.order_tab li.active').index();
		data['page'] = data['page'] ? data['page'] : 1;
		
		$.post(list_url,data,function (res) {
			var response = res['data'];
			if(data['page'] == response.page) {
				//清空tbody
				$('tbody').empty();
				
				//加载列表
				$('tbody').html(load_list(response,'order_list'));
				
				//加载分页信息
				var pageNum = Math.ceil(response.count/response.pageSize);
				if(pageNum == 0) pageNum = 1;
				$('div.page_info').text('共'+response.count+'条记录,每页'+response.pageSize+'条,共'+pageNum+'页');
				$('input.page').val(response.page);
				$('span.num').text(response.page+'/'+pageNum);
			}
		})
	}
	
	//点击操作
	$('div.list').on('change','select.handle',function () {
		var _this = $(this);
		var action = _this.val();
		var id = _this.data('id');
		var url = '';
		var info = '';
		var table = $('ul.order_tab li.active').index();
		
		switch(action) {
			case 'edit':
				window.location.href = URL.edit_url+id;
				break;
			case 'delete':
				url = URL.delete_url;
				info = '确认要删除这个订单吗?';
				break;
			case 'restore':
				url = URL.restore_url;
				info = '确认要还原这个订单吗?';
				break;
			case 'check':
				url = URL.check_url;
				info = '确认要审核这个订单吗?';
				break;
			case 'cancel':
				url = URL.cancel_url;
				info = '确认要取消审核这个订单吗?';
				break;
			default:return ;
		}
		//IE7会往下执行
		if(action =='edit') return ;
		
		if(confirm(info)) {
			$.post(url,{id:id,index:table},function (res) {
				if(res.status) {
						var page = parseInt($('span.num').text().split('/')[0]);
						reload_list(page);
				} else {
						alert(res.info);
						_this.find("option:first").attr("selected",true);	
				}	
			})
		}
	})
	request_list({page:1});
}

//邮寄订单
function mail_list($,w) {
	var list_url = URL.maiList;
	
	//上一页
	$('.page_handle').on('click','button.prev',function () {
		var _this = $(this)
		var page_info = _this.siblings('span.num').text().split('/');
		var page = parseInt(page_info[0]);
		var maxNum = parseInt(page_info[1]);
		
		//如果当前页码是1,则禁止上一页
		if(page == 1 || maxNum == 1) return ;
		var page = page - 1;
		//请求列表数据
		reload_list(page);
	})
	
	
	//下一页
	$('.page_handle').on('click','button.next',function () {
		var _this = $(this)
		var page_info = _this.siblings('span.num').text().split('/');
		var page = parseInt(page_info[0]);
		var maxNum = parseInt(page_info[1]);
		
		//如果已经是最后一页则禁止请求分页属性
		if(page == maxNum || maxNum == 1) return ;
		var page = page + 1;
		reload_list(page);
	})
	
	//搜索
	$('#search').on('click',function () {
		var _this = $(this);
		var search_data = {};
		var search_field = $('select[name=search_field]');
		var search_text = $('.search_text');
		
		//如果搜索条件不为空,则放入对象
		if(search_field.val() !== '' && search_text.val() !== '') {
			search_data[search_field.val()]	= search_text.val();
		}
		
		//获取下单时间order_time
		if(($('#startime').val()!=='' || $('#endtime').val()!=='')) {
			var obj = {};
			obj['startime'] = $('#startime').val();
			obj['endtime'] = $('#endtime').val();
			search_data['order_time'] = obj;
			obj = null;
		}
		
		//如果选择是否邮寄,则记录搜索条件
		if($('select.mail').val() !== '') {
			//获取邮寄条件
			search_data[$('select.mail').attr('name')] = $('select.mail').val();	
		}
		
		//如果是空对象,则删除掉搜索按钮上的数据
		if(isEmptyObject(search_data)) {
			_this.removeData('search_data');
			return ;
		//否则把搜索数据记录在搜索按钮上
		} else {
			_this.data('search_data',JSON.stringify(search_data));	
		}
		
		request_list(search_data);
	})
	
	//输入框分页
	$('input.page').on('focus',function () {
		$(window).off('keydown').on('keydown',function (e){
			if(e.keyCode == 13) {
				var page_info = $('span.num').text().split('/');
				var page = parseInt(page_info[0]);
				var input_page = parseInt($('input.page').val());
				var maxNum = parseInt(page_info[1]);
				
				if(maxNum == 1) return ;
				
				//如果输入的就是当前页或者最大页数是1,则不请求
				if(input_page == page || maxNum == 1) {
					return ;
					
				//如果输入页数小于1,当1处理	
				} else if(input_page < 1) {
					input_page = 1;	
					
				//如果超过最大页数,则当最后一页处理
				} else if(input_page > maxNum) {
					input_page = maxNum;	
				}
				
				var data = {};
				if($('#search').data('search_data') !== undefined) {
					data = JSON.parse($('#search').data('search_data'));
				}
				data['page'] = input_page;
				//请求列表数据
				request_list(data);
			}	
		})
	})

	//删除邮寄信息(单个)
	$('#content').on('click','.delete_mail',function () {
		var _this = $(this);
		var ids = [];
		ids.push(_this.data('id'));
		if(confirm('确定要删除这个邮寄订单的邮寄信息吗?')) {
			$.post(URL.delete_url,{ids:ids},function (res) {
				if(res.status) {
					var page = parseInt($('span.num').text().split('/')[0]);
					reload_list(page);
				} else {
					alert(res.info);
				}
			})
		}
	})

	//添加邮寄信息(单个)
	$('#content').on('click','.create_mail',function () {
		var _this = $(this);
		
		var obj = {};
		var list = [];

		obj['username'] = _this.data('username');
		obj['order_id'] = _this.data('id');
		list.push(obj);

		show_company_list(list);
	})

	//弹出物流窗口
	function show_company_list(list) {
		var html = '';
		$.each(list,function (k,v) {
			html += '<div class="company_info" data-id = "'+v.order_id+'">';
			html += '<div class="company_num">';
            html += '<label>快递单号</label>';
            html += '<input type="text" placeholder="请输入快递单号" name="company_num"/>';
            html += '</div>';
            html += '<div class="company_list">';
            html += '<label>快递公司</label>';
            html += '<select name="company">';
            html += '<option value="">请选择</option>';
            html += '<option value="1">圆通快递</option>';
            html += '<option value="2">申通快递</option>';
            html += '<option value="3">中通快递</option>';
            html += '<option value="4">韵达快递</option>';
            html += '<option value="5">顺丰快递</option>';
            html += '<option value="6">ems快递</option>';
            html += '</select>';
            html += '</div>';
			html += '<div class="username">'+v.username+'</div>';
            html += '</div>';
		})

		$('#change_list div.content').empty().html(html);
		$('#dialog').show();
		$('#change_list').show();
	}

	//保存物流信息
	$('#save_mail').on('click',function () {

		var list = [];

		$('.company_info').each(function () {

			var __this = $(this);
			var obj = {};
			var username = __this.find('div.username').html();
			var company = __this.find('select[name=company]').val();
			var company_num = __this.find('input[name=company_num]').val();

			//判断信息是否完整
			if(company == '') {
				alert('用户'+username+'的物流公司不能为空!');
				return false;
			}
			if(company_num == '') {
				alert('用户'+username+'的物流单号不能为空!');
				return false;
			}
			obj['order_id'] = __this.data('id');
			obj['company_id'] = company;
			obj['company_num'] = company_num;

			list.push(obj);
		})

		if(list.length == 0) return false;

		$.post(URL.update_url,{data:list},function (res) {
			if(res.status) {
				var page = parseInt($('span.num').text().split('/')[0]);
				reload_list(page);
				$('#change_list .cancel').click();	
			} else {
				alert(res.info);
			}
		})
	})

	//全选
	$('#all').on('change',function () {
		var _this = $(this);

		if(_this.attr('checked')) {
			$('div.list .checkbox:enabled').attr('checked',true);
			$('#delete_mail,#create_mail').removeClass('disabled');
		} else {
			$('div.list .checkbox:enabled').attr('checked',false);
			$('#delete_mail,#create_mail').addClass('disabled');
		}
	})

	//如果有选中,则不禁用按钮,如果取消选中,则禁用按钮
	$('div.list').on('change','.checkbox:enabled',function () {
		var _this = $(this);
		if(_this.attr('checked')) {
			$('#delete_mail,#create_mail').removeClass('disabled');
		} else {
			$('#delete_mail,#create_mail').addClass('disabled');
		}
	})

	//点击批量删除邮寄信息
	$('#delete_mail').on('click',function () {
		var ids = [];
		var checkboxes = $('div.list .checkbox:enabled:checked');

		if(checkboxes.size() == 0 || $(this).hasClass('disabled')) {
			return false;
		}
		checkboxes.each(function (k,v) {
			ids.push($(this).data('id'));
		})
		
		if(confirm('确定要删除这些邮寄订单的邮寄信息吗?')) {
			$.post(URL.delete_url,{ids:ids},function (res) {
				if(res.status) {
					var page = parseInt($('span.num').text().split('/')[0]);
					reload_list(page);
				} else {
					alert(res.info);
				}
			})
		}
	})
	
	//点击批量添加邮寄信息
	$('#create_mail').on('click',function () {
		var _this = $(this);
		var checkboxes = $('div.list .checkbox:enabled:checked');
		var list = [];

		if(checkboxes.size() == 0 || $(this).hasClass('disabled')) {
			return false;
		}

		checkboxes.each(function () {
			var __this = $(this);
			var obj = {};

			if(__this.data('mail_status') == 0) {
				obj['username'] = __this.parent().siblings('.username').html();
				obj['order_id'] = __this.data('id');
				list.push(obj);
			}
		})
		if(list.length == 0) return false;
		show_company_list(list);
	})
	
	//根据页码重载页面
	function reload_list(page) {
		//删除后重载列表
		var data = {};
		if($('#search').data('search_data') !== undefined) {
			data = JSON.parse($('#search').data('search_data'));
		}
		data['page'] = page;
		request_list(data);
	}
	
	//根据页码请求资源列表数据
	function request_list(data) {
		data['page'] = data['page'] ? data['page'] : 1;
		$.post(list_url,data,function (res) {
			
			var response = res['data'];
			if(data['page'] == response.page) {
				//清空tbody
				$('tbody').empty();
				
				//加载列表
				$('tbody').html(load_list(response,'mail_list'));
				
				//加载分页信息
				var pageNum = Math.ceil(response.count/response.pageSize);
				if(pageNum == 0) pageNum = 1;
				$('div.page_info').text('共'+response.count+'条记录,每页'+response.pageSize+'条,共'+pageNum+'页');
				$('input.page').val(response.page);
				$('span.num').text(response.page+'/'+pageNum);

				//取消全选
				$('#all').attr('checked',false);
			}
		})
	}
	request_list({page:1});
}

//预览商品和资源
function preview($,w) {
	//点击扩展标签显示内容
	$('.attr_list').on('click','li',function () {
		var _this = $(this);
		if(_this.hasClass('active')) return ;
		if(_this.data('name') !== undefined) {
			_this.parents('.attr_list').next().find('.name_detail').html(_this.data('name'));
		}
		
		if(_this.data('content') !== undefined) {
			$('#attr_detail').html(_this.data('content'));
		}
		_this.addClass('active').siblings().removeClass('active');
		
	})

	//默认加载第一个属性
	// if($('.resource_button li.first').size() > 0) {
	// 	$('.name_detail').text($('.resource_button li.first').data('name'));
	// 	$('#attr_detail').html($('.resource_button li.first').data('content'));
	// }
	
	$('#header').on('click','#submit_checker',function (e) {
		if($('tbody tr').size() == 0) {
			alert('此商品下没有资源,不能提交审核!');
			e.preventDefault();	
		}
	})
}