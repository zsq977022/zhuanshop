if(!window.components) {window.components = {};}
window.components.header=React.createClass({displayName: "header",
   getInitialState:function(){
        return {
              nav:[],
           
            
              headerTab:{display:"none"},
              tikuTabImgsrc:"static/images/ti_head_01.png",
              isShowWeixin:{display:"none"},
              isShowWeibo:{display:"none"},
              isShowMobileQrcode:{display:"none"},
              usernameRightClassName:"username_right_down",
              tikuTabCircleClassName:"tikutab_circle_down",
            
            };
     },
                 
    showSelectMenu:function(isShow){
            var self = this;
            if(window.AUTH == undefined || window.AUTH == null){ return false;}
            $(".dl_down").show();
            $("#triangle").addClass("username_right_up").removeClass("username_right_down");

                      },
    hideSelectMenu:function(event){
            $(".dl_down").hide();
            $("#triangle").addClass("username_right_down").removeClass("username_right_up");
    },
    //我的订单
    goToMyOrder:function(){
            window.location.assign("#order");
    },
    //查看消息
    checkMessage:function(){
            window.open("http://tiku.huatu.com/index.php?mod=message&act=message_list");
    },
    //退出
    handleExist:function(){
            var self = this;
            var url = "bootstrap.php?s=/user/logout";
            $.get(url,function(data){
                  if(data.message == "Y"){ window.location.assign("#zhibo");window.AUTH = undefined;}else{setAlert("登出失败");}
            },"json")
    },
    //加载导航数据
    componentDidMount:function(){
            var that=this;
            that.setState({nav:[{name: "砖直播",nav_id: "2",className:"nav_select"},{name: "砖图书",nav_id: "1",className:"nav_deault"}]})
    },                          
    showHeaderTab:function(){
        this.setState({
                headerTab:{display:"block"},
                tikuTabCircleClassName:"tikutab_circle_up",
                loginLayer:{display:"none"}
        })
    },
    hideHeaderTab:function(){
          this.setState({
                  headerTab:{display:"none"},
                  tikuTabCircleClassName:"tikutab_circle_down"
          })
    },
                
    mouseOnWeixin:function(event){
        var that=this;
        that.setState({isShowWeixin:{display:"block"}});
                           event.target.onmouseout=function(){
        that.setState({isShowWeixin:{display:"none"}});
      }
   },

    mouseOnWeibo:function(event){
        var that=this;
        this.setState({isShowWeibo:{display:"block"}});
    },
                             
    clickWeixin:function(event){
        event.preventDefault();
    },
    mouseOutWeibo:function(){
          this.setState({isShowWeibo:{display:"none"}})
    },
    mouseOnWeiboBox:function(){ 
          this.setState({isShowWeibo:{display:"block"}})
    },
    weiboGuanZhu:function(){
          window.open("http://weibo.com/htztk");
    },
    mouseOnMobile:function(event){
          var that=this;
              that.setState({isShowMobileQrcode:{display:"block"}});
              event.target.onmouseout=function(){
                        that.setState({isShowMobileQrcode:{display:"none"}});
              }
    },                                              
    handleNavTab:function(event){
        var $target=$(event.target),  navid=$target.data("navid"),that=this;
        $target.removeClass("nav_deault").addClass("nav_select").siblings().removeClass("nav_select").addClass("nav_deault"); 
        if(navid==1){  window.location.assign("#book"); }
        if(navid==2){  window.location.assign("#zhibo");}
    },                                               
    handleUserMenu:function(event){
        if(window.AUTH != undefined && window.AUTH != null){
                  this.setState({userSelectMenu:{display:"block"}})
        }
    },
    //导航切换事件处理
     handleNavTab:function(event){
         var $target=$(event.target),  navid=$target.data("navid"),that=this;
         $target.removeClass("nav_deault").addClass("nav_select").siblings().removeClass("nav_select").addClass("nav_deault"); 
         if(navid==1){  window.location.assign("#book"); }
         if(navid==2){  window.location.assign("#zhibo");}
    },
                            

          
     


                                           
render:function(){

     var that=this;
      return  React.createElement("div", null, 
                React.createElement("div", {className: "ti_header_top"}, 
                    React.createElement("div", {className: "header_Top"}, 
                       React.createElement("div", {className: "top_left  fl", onMouseOver: that.showHeaderTab, onMouseOut: that.hideHeaderTab}, 
                          React.createElement("a", {href: "http://tiku.huatu.com/index.php?mod=administration&act=index", target: "_blank"}, "公务员题库"), 
                          React.createElement("div", {className: that.state.tikuTabCircleClassName}, ">")
                       ), 
                       React.createElement("div", {className: "top_right fr"}, 
                            React.createElement("ul", {className: "clearfix"}, 
            [1].map(function(){
                      if(window.AUTH != undefined){
                            return  React.createElement("li", {className: "ti_user_list", id: "username_li", 
                                      onMouseOver: that.showSelectMenu, 
                                      onMouseOut: that.hideSelectMenu}, 
                                    React.createElement("div", {className: "ti_user_txt", id: "ti_user_txt"}, 
                                        React.createElement("div", {className: "username_left_circle", id: "username_circle"}), 
                                        React.createElement("span", {id: "username"}, window.AUTH?AUTH.username:""), 
                                        React.createElement("span", {className: "username_right_down", id: "triangle", style: {"display":"inline-block"}}, ">")
                                    ), 
                                    React.createElement("div", {className: "dl_down", style: {"display":"none"}, onMouseOut: that.hideSelectMenu}, 
                                          React.createElement("dl", {className: "dl_down_content"}, 
                                             React.createElement("dt", null, React.createElement("span", {onClick: that.goToMyOrder}, "我的订单")), 
                                             React.createElement("dt", null, React.createElement("span", {onClick: that.handleExist}, "退出")), 
                                             React.createElement("dt", null, React.createElement("a", {href: "http://tiku.huatu.com/index.php?mod=administration&act=changepw&ref=Administration/index", onClick: that.changePassword}, "修改密码"))
                                          )
                                    )
                             )
                      }else{

                      }
                      
                  }), 
       [1].map(function(){
            if(window.AUTH != undefined){
                   return     React.createElement("li", {className: "ti_user_info", style: {"display":"inline-block"}, onClick: that.checkMessage}, 
                                     "消息"
                              )
            }else{
                 
            }
       }), 
                              React.createElement("li", {className: "ti_app"}, 
                                React.createElement("a", {href: "http://tiku.huatu.com/index.php?mod=user&act=anddownload", target: "_blank", onMouseOver: that.mouseOnMobile}, "客户端"), 
                                React.createElement("div", {className: "mobile", style: that.state.isShowMobileQrcode}, 
                                      React.createElement("div", {className: "left"}, React.createElement("img", {src: "static/images/app.jpg", width: "55", height: "55"})), 
                                      React.createElement("div", {className: "right"}, React.createElement("p", null, "扫一扫下载"), React.createElement("p", null, "手机客户端"))
                                )
                              ), 
                              React.createElement("li", {className: "ti_weixin"}, 
                                React.createElement("a", {href: "", onMouseOver: that.mouseOnWeixin, onClick: that.clickWeixin}, "微信"), 
                                React.createElement("div", {className: "wx", style: that.state.isShowWeixin}, 
                                    React.createElement("div", {className: "left"}, React.createElement("img", {src: "static/images/new_wxma.png"})), 
                                    React.createElement("div", {className: "right"}, React.createElement("p", null, "砖题库 扫一扫"), React.createElement("p", null, "微信号:ztkgwy"))
                                )
                              ), 
                              React.createElement("li", {className: "ti_weibo", onMouseOver: that.mouseOnWeibo, onMouseOut: that.mouseOutWeibo}, 
                                 React.createElement("a", {href: "http://weibo.com/htztk", target: "_blank"}, "微博"), 
                                  React.createElement("div", {className: "wb", onMouseOver: that.mouseOnWeiboBox, style: that.state.isShowWeibo}, 
                                      React.createElement("div", {className: "left"}, React.createElement("img", {src: "static/images/new_indexhuatu.png"})), 
                                      React.createElement("div", {className: "right"}, React.createElement("p", null, "砖题库公务员考试"), React.createElement("p", null, React.createElement("img", {src: "static/images/new_guanzhu.png", onClick: that.weiboGuanZhu})))
                                 )
                              )
                          )
                        )
                    ), 
                    React.createElement("div", {className: "header_tab", style: that.state.headerTab, onMouseOver: that.showHeaderTab, onMouseOut: that.hideHeaderTab}, 
                        React.createElement("div", {className: "type_tab"}, 
                              React.createElement("a", {href: "http://tiku.huatu.com/mini/paper.php", target: "_blank", onClick: that.tikuTab}, "教师题库")
                         )
                    ), 
                             
                  React.createElement("div", {className: "ti_header"}, 
                      React.createElement("div", {className: "header_type"}, 
                          React.createElement("div", {className: "header_type_list clearfix"}, 
                                          React.createElement("div", {className: "logo"}, 
                                                React.createElement("img", {src: "static/images/tiku_logo.png"})
                                          ), 
                                          React.createElement("div", {className: "nav_right"}, 
                                                React.createElement("ul", {className: "sild_list clearfix"}, 
                                                    React.createElement("li", null, React.createElement("a", {href: "http://tiku.huatu.com"}, "首页")), 
                                                    React.createElement("li", {className: "li_act"}, React.createElement("a", {href: ""}, "砖商城")), 
                                                    React.createElement("li", {className: "sild_test"}, React.createElement("a", {href: ""}, "活动"))
                                                )
                                          )
                            )
                            ), 
                        React.createElement("div", {className: "header_menu"}, 
                           React.createElement("ul", {className: "heade_nav clearfix", id: "nav"}, 
                               that.state.nav.map(function(item,i){
                                     return  React.createElement("li", {onClick: that.handleNavTab, "data-navid": item.nav_id, className: item.className}, item.name)
                               })
                           
                         )
                      )
                    )
                  )
                 )
                            
                    }
           });

                                                        
/*-----------------------------------------------
  底部组件
-------------------------------------------------
*/
 window.components.footer=React.createClass({displayName: "footer",
              render:function(){
                 return   React.createElement("div", {className: "ma_footer"}, 
                               React.createElement("div", {className: "ma_foot"}, 
                                    React.createElement("div", {className: "foot_left"}, 
                                         React.createElement("img", {src: "static/images/act_foot_logo.png"})
                                    ), 
                                    React.createElement("div", {className: "foot_right"}, 
                                        React.createElement("div", {className: "flinks"}, 
                                          "华图公务员考试网——公务员考试培训和国家公务员考试培训第一品牌"
                                        ), 
                                        React.createElement("div", {className: "copyright"}, 
                                          "京ICP备05066753京号ICP证090387号京公网安备11010802010141电信业务审批【2009】字第233号函"
                                        )
                                    )
                                )
                          )
                             
                 }
        });
              
                                            
           
                                                  
                                        


                            
                 

                              
               
                          
               
                                                      
                          
                   
                   
           
                                   
                           
                              
                          

                    
                   
                            











                                                                                 
                                                                                 
                                                                              
                                                                              
                                                  
                                         
                                                 

                                                 
                                            
                                       









                 
