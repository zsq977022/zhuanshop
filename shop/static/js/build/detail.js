/**
 * ---------------------------------------
 *
 *    商品详情页 
 *    author:sunqi
 *    emil:sunqi_925@163.com
 *
 *
 * ----------------------------------------
 */
Zhuan.detail=React.createClass({displayName: "detail",
    getInitialState:function(){
     

        return  {
            addressLayer:{display:"none"},
            provlist:[],
            citylist:[],
            downlist:[],
            phoneStyleTip:{display:"none"},
            realNameTip:{display:"none"},
            cityTip:{display:"none"},
            addressDetailTip:{display:"none"},
            phoneTipText:"",
            checkAddress:{display:"none"},
            areaDisplay:{display:"inline-block"},
            loginLayer:{display:"none"},
            username:{display:"none"},
            password:{display:"none"},
            wrongLoginInfo:{display:"none"},
            loginSuccess:{display:"none"},
            provinceName:"请选择省/直辖市",
            isProvinceSelected:false,
            isCitySelected:false,
            isAreaSelected:false,
            cityName:"请选择城市",
            areaName:"请选择区县",
            btnText:"去付款"
        };
                  }, 
    //点击【立即参加】按钮的处理的处理器
    handleClickJoin:function(){
        var that=this;
        //是否登录
        $.get("bootstrap.php?s=/user/islogin&rnd="+Math.random(),function(response){
                         
            if(response=="Y"){ //已登录
                //已购买
                if(that.props.btn.purchased==1){
                    var snapid=that.props.btn.snapid,       //快照id
                        orderid = that.props.btn.orderid;   //订单id
                                    
  React.render(React.createElement(Zhuan.courseDetails, {goods_id: that.props.topInfo.goods_id, name: that.props.topInfo.name, snapid: snapid, orderid: orderid, type: that.props.type}),document.getElementById("content"));  
                }else{ //未购买
                            //是否已达限制人数
                            //     $.post("bootstrap.php?s=/goods/limit",{goods_id:that.props.topInfo.goods_id},function(result){
                    var  sales = parseInt(that.props.topInfo.goods_num)+parseInt(that.props.topInfo.base_number);  //销量
                    var  limit = parseInt(that.props.topInfo.quota_num);  //限购
                    if(sales == limit){
                        alert("报名人数已满");
                        return false;
                    }else{  //可以参加购买
                        if(that.props.topInfo.mail==1){ //显示收货地址
                            $.get("bootstrap.php?s=/user/getAddress",function(data){
                                                    
                                if(data.message=="Y"){//如果有地址
                                    var address=data.address,tmpStr='';
                                    if(address.area == undefined){
                                        tmpStr='';
                                    }else{
                                        tmpStr=address.area;
                                    }
                                    var str=address.province+address.city+tmpStr+address.street+" "+address.name+"(收) "+address.phone;
                                    var nextState={
                                        addressLayer:{display:"none"},
                                        checkAddress:{display:"block"},
                                        addressText:str,
                                        addressInfo:address
                                                                          
                                    };
                                    if(that.props.topInfo.price==0.00){
                                        nextState.btnText="马上学习";
                                    }
                                    that.setState(nextState);
                                                                         
                                                                        
                                }else{ //如果没有地址，弹出填写地址层
                                    that.setState({
                                        addressLayer:{display:"block"},
                                        citieslist:[],
                                        citylist:[],
                                        downlist:[],
                                        checkAddress:{display:"none"}
                                    });
                                }

                            },"json");
                        }else{
                            //点击按钮初始化订单
                            that.createOrder('N');
                        }
                    }
                                  //},'json');
                }
            }else{ //未登录
                that.setState({loginLayer:{display:"block"}})
            }

        });
    },
                   //创建订单           
    createOrder:function(mail){
              
                  var that=this;
                  var order={
                              goods_id:that.props.topInfo.goods_id,
                              price:that.props.topInfo.price
                              };
                   //初始化订单
                  
              $.post("bootstrap.php?s=/order/init",order,function(result){
                   
                  if(result.message=='Y')
                  {        //初始化订单成功
                     if(that.props.topInfo.price==0.00)
                     {     //零元订单
                        var postData={orderid:result.orderid,goodsid:that.props.topInfo.goods_id,mail:mail};
                          //是否已购买
                          $.post("bootstrap.php?s=/order/pay",postData,function(data){
                           if(data.message=='Y')
                            {   //如果已购买
                              that.setState({checkAddress:{display:"none"}});
                              React.render(React.createElement(Zhuan.courseDetails, {goods_id: that.props.topInfo.goods_id, name: that.props.topInfo.name, snapid: result.snapid, orderid: result.orderid, type: that.props.type}),document.getElementById("content"));                                        
                            }
                          },"json");
                                 
                            return; 
                      }
                                           
                                                
                                           
                        
                        //组装二维码数据                             
                        var weixinParams={
                                product_id:that.props.topInfo.goods_id, //商品id
                                body:that.props.topInfo.name,   //商品名称
                                total_fee:that.props.topInfo.price, //商品价格
                                order_id:result.orderid,   //初始化生成的订单id
                                order_number:result.order_number, //订单编号
                                mail:mail
                                  };
                                                            
                      $.post('bootstrap.php?s=/order/qrcode',weixinParams,function(data){
                                       
                                         var orderInfo={
                                                   name:that.props.topInfo.name,
                                                   price:that.props.topInfo.price,
                                                   goods_id:that.props.topInfo.goods_id,
                                                   weixinQrcode:data.weixin,
                                                   zhifubaoQrcode:data.zfb,
                                                   orderid:result.orderid,
                                                   mail:mail
                                               }; 
                                               var courseDetail={
                                                            goods_id:that.props.topInfo.goods_id,
                                                            name:that.props.topInfo.name,
                                                            snapid:result.snapid,
                                                            orderid:result.orderid,
                                                            type:that.props.type
                                               }
                            React.render(React.createElement(Zhuan.pay, {orderInfo: orderInfo, courseDetail: courseDetail}),document.getElementById("content"));

                                      },'json');
                                                       }else{
                                                          alert("初始化订单失败,请稍后再试");
                                                       }
                                                 },'json');
                   },
    //隐藏填写收货地址层
    hideAddress:function(){
                           this.setState({addressLayer:{display:"none"}});
                   },
    //隐藏确认收货地址层
    hideCheckAddress:function(){
                          this.setState({checkAddress:{display:"none"}})
                   },
    //扩展字段的切换
    handleTab:function(event){
                           
                            $(event.target).addClass('li_current').siblings().removeClass('li_current');
                            var index=$(event.target).data("index");
                            $(".ma_left_info",$("#detail-tab")).hide().eq(index).show();


                   },
                  
    componentDidMount:function(){
                          //挂载组件之后显示第一个扩展字段
                            $("#tab span").first().addClass("li_current");
                            $(".ma_left_info",$("#detail-tab")).first().show();
                          //控制导航样式
                            var nav=parseInt(this.props.topInfo.nav);
                            if(nav==2){//直播
                                     $("#nav li").eq(0).addClass("nav_select").removeClass("nav_deault").siblings().removeClass("nav_select");
                            }else if(nav==1){ //图书
                                     $("#nav li").eq(1).addClass("nav_select").removeClass("nav_deault").siblings().removeClass("nav_select");
                            }
                            //返回顶部按钮
                            var W=$("#body").width();
                            var left=(W-980)/2+980;
                            var gotoTop_html='<div id="gotoTop" title="回到顶部" style="position:fixed;width:30px;height:100px;bottom:250px;left:'+left+'px;display:none;cursor:pointer;background:url(static/images/topback.png) no-repeat;"></div>';
                            $("#body").append(gotoTop_html);
                            $("#gotoTop").click(function(){$('#body').animate({scrollTop:0},700); })
                            $(window).scroll(function(){
                                    var s=$(window).scrollTop();
                                    if(s>534){ $("#tab").addClass("extension_tab");}else{$("#tab").removeClass("extension_tab");}
                                    if(s>700){
                                          $("#gotoTop").fadeIn(400);
                                    } else {
                                          $("#gotoTop").fadeOut(400);
                                    }    
                            })
                               
                              


                   },
              
     //渲染UI
    render:function(){
                        var that=this;
                        //销量+销量基数
                        var  joinedPeople = parseInt(that.props.topInfo.goods_num)+parseInt(that.props.topInfo.base_number);
                        //限购+销量基数
                        //var  limit = parseInt(that.props.quota_num)+parseInt(that.props.topInfo.base_number);
                         var A=function(){
                                return React.createElement("div", {className: "ma_message fl"}, 
                                 React.createElement("h3", null, that.props.topInfo.name), 
                                 React.createElement("p", null, "授课老师：", that.props.topInfo.author), 
                                 React.createElement("p", null, "课时：", that.props.topInfo.hours, "课时"), 
                                 React.createElement("p", null, "有效期：", that.props.topInfo.startime), 
                                 React.createElement("p", null, "限招人数：", that.props.topInfo.quota_num==0?'不限':that.props.topInfo.quota_num+"人"), 
                                 React.createElement("p", null, "已参加人数：", joinedPeople, "人"), 
                                 React.createElement("div", null, "￥", parseFloat(that.props.topInfo.price)), 
                                 React.createElement("a", {className: "ma_join", onClick: that.handleClickJoin, style: that.props.btn.bgColor}, that.props.btn.text)
                                 )
                                
                         }
                         var B=function(){
                                   return React.createElement("div", {className: "ma_message fl"}, 
                                 React.createElement("h3", null, that.props.topInfo.name), 
                               
                                 React.createElement("p", null, "限购：", that.props.topInfo.quota_num==0?'不限':that.props.topInfo.quota_num+"本"), 
                                 React.createElement("p", null, "销量：", joinedPeople, "本"), 
                                 React.createElement("div", null, "￥", parseFloat(that.props.topInfo.price)), 
                                 React.createElement("a", {className: "ma_join", onClick: that.handleClickJoin, style: that.props.btn.bgColor}, that.props.btn.text)
                                 )
                         };
                         
          	      	    return  React.createElement("div", {className: "ma_container"}, React.createElement("div", {className: "ma_top clearfix"}, 
                        			   React.createElement("div", {className: "ma_top_img fl"}, 
                        				 React.createElement("img", {src: that.props.topInfo.cover_url})
                        			   ), 
                        			       that.props.type=='A'?A():B()
                        			
                        		     ), 
                        		       React.createElement("div", {className: "ma_bottom clearfix"}, 
                        			  React.createElement("div", {className: "ma_left fl", id: "detail-tab"}, 
                        				React.createElement("div", {className: "ma_left_tab", id: "tab"}, 	
                                  that.props.tab.map(function(item,i){
                                          return React.createElement("span", {onClick: that.handleTab, "data-index": i, key: i}, item.title)
                                  })				
                        				   ), 
                                  that.props.tab.map(function(item,i){
                                          return React.createElement("div", {key: i, className: "ma_left_info", style: {display:"none"}, dangerouslySetInnerHTML: {__html:item.content}})
                                  })
                        			), 
                        			React.createElement("div", {className: "ma_right fr"}, 
                        				React.createElement("div", {className: "ma_right_top"}, React.createElement("a", {href: "http://v.huatu.com/cla/class_detail.php?id=48816", target: "_blank"}, React.createElement("img", {src: "static/images/ad13.jpg"}))), 
                        				React.createElement("div", {className: "ma_right_bottom"}, React.createElement("a", {href: "http://v.huatu.com/cla/class_detail.php?id=48758", target: "_blank"}, React.createElement("img", {src: "static/images/ad14.jpg"})))
                        			)
                        		  ), 
                            /* 登录Html  */
                           React.createElement("div", {style: that.state.loginLayer, id: "loginLayer"}, " ", React.createElement("div", {className: "ma_cover_bg"}), 
                                    React.createElement("div", {className: "log_content_model"}, 
                                    React.createElement("div", {className: "log_left log_moedel"}, 
                                    React.createElement("img", {src: "static/images/ma_cancel.png", onClick: that.handleCancelBtn}), 
                                    React.createElement("div", {className: "log_moedel_title"}, "登录砖题库"), 
                                    React.createElement("form", {action: "", method: "post", className: "form_login"}, 
                                    React.createElement("div", {className: "error", id: "tips-username", style: that.state.username}, "请输入用户名"), 
                                    React.createElement("div", {className: "inp inp_account"}, 
                                    React.createElement("input", {type: "text", className: "account", placeholder: "请输入题库账号/教育网账号/网校账号", ref: "username"})
                                       
                                    ), 
                                    React.createElement("div", {className: "error", id: "tips-password", style: that.state.password}, "请输入密码"), 
                                    React.createElement("div", {className: "inp inp_pass"}, 
                                         React.createElement("input", {type: "password", className: "password", placeholder: "密码", ref: "password"})
                                      
                                    ), 
                                     React.createElement("div", {className: "error", id: "tips-username", style: that.state.wrongLoginInfo}, "用户名或密码错误"), 
                                    React.createElement("div", {className: "Cinput"}, 
                                        React.createElement("input", {type: "checkbox", name: "keeplogin", value: "1", id: "checkRegister", ref: "autologin", className: "checkInput", title: "同意"}), 
                                        React.createElement("div", {className: "C-zidong"}, React.createElement("label", null, "下次自动登录")), 
                                        React.createElement("div", {className: "C-forget"}, React.createElement("a", {href: "#"}, "忘记密码？"))
                                    ), 
                                   React.createElement("div", {className: "btn_login"}, React.createElement("button", {type: "submit", className: "btn_re", onClick: that.handleClickLoginBtn}, "登录")), 
                                    React.createElement("div", {className: "Cinput"}, 
                                        React.createElement("div", {className: "no_account"}, "还没有账号？立即", React.createElement("a", {href: "http://tiku.huatu.com/index.php?mod=user&act=register&ref=", className: "regist"}, "注册"))
                                    )
                                    )
                                    )
                                   )
                            ), 
                                  /* 弹窗Html */
                                      React.createElement("div", {style: that.state.loginSuccess}, 
                                          React.createElement("div", {className: "ma_cover_bg"}), 
                                               React.createElement("div", {className: "ma_confirm"}, 
                                                React.createElement("div", {className: "ma_confirm_txt"}, "登录成功"), 
                                                React.createElement("div", {className: "ma_confirm_btn clearfix"}, 
                                                  React.createElement("a", {href: "", className: "ma_confirm_know fl", id: "pay", onClick: that.closeLoginSuccessTip}, "知道了")
                                                )
                                              )
                                      ), 
                                      /* 确认收货地址html */
                                    React.createElement("div", {style: that.state.checkAddress}, 
                                     React.createElement("div", {className: "ma_cover_bg"}), 
                                    React.createElement("div", {className: "ma_modle"}, 
                                     React.createElement("img", {src: "static/images/ma_cancel.png", onClick: that.hideCheckAddress}), 
                                         React.createElement("h4", null, "请确认您的收货地址"), 
                                          React.createElement("div", null, 
                                               React.createElement("p", {style: {fontSize:"18px",color:"#505050",fontWeight:"bolder"}}, that.state.addressText, 
                                               React.createElement("span", {className: "change_address", onClick: that.useNewAddress}, "修改地址")), 
                                               React.createElement("span", {className: "ma_next paybtn", onClick: that.goToPay}, that.state.btnText)
                                          )
                                    )
                                    ), 
                                   /* 填写收货地址HTML */
                                React.createElement("div", {style: that.state.addressLayer}, 
                                  React.createElement("div", {className: "ma_cover_bg"}), 
                                        React.createElement("div", {className: "ma_modle"}, 
                                          React.createElement("h4", null, "填写收货地址"), 
                                          React.createElement("p", null, "请如实填写以下的收货信息，以便我们尽快给您寄送资料。"), 
                                          React.createElement("form", {id: "form"}, 
                                            React.createElement("img", {src: "static/images/ma_cancel.png", onClick: that.hideAddress}), 
                                            React.createElement("div", {className: "form_inp clearfix"}, 
                                              React.createElement("span", null, "收货人："), 
                                              React.createElement("input", {className: "form_person", type: "text", ref: "realName"})
                                            ), 
                                               React.createElement("div", {className: "error", style: that.state.realNameTip}, "请填写收货人姓名"), 
                                            React.createElement("div", {className: "form_inp clearfix"}, 
                                                 React.createElement("span", null, "手机号码："), 
                                                 React.createElement("input", {className: "form_phone", type: "text", ref: "phone"})
                                            ), 
                                                React.createElement("div", {className: "form-inp error", style: that.state.phoneStyleTip}, that.state.phoneTipText), 
                                               React.createElement("div", {className: "form_inp clearfix"}, 
                                               React.createElement("span", null, "地区："), 
                                         React.createElement("div", {className: "f_sel"}, 
                                         React.createElement("input", {type: "hidden", value: ""}), 
                                           React.createElement("div", {className: "sel_select sel_sheng", onClick: that.showProvincesMenu, ref: "proName"}, that.state.provinceName), 
                                             React.createElement("ul", {className: "clearfix none"}, 
                                                that.state.provlist.map(function(item,i){
                                        return React.createElement("li", {"data-value": item.id+"##"+item.name, key: i, onClick: that.pickPro}, item.name)
                                              })
                                               )
                                                ), 
                                                React.createElement("div", {className: "f_sel"}, 
                                                React.createElement("input", {type: "hidden", value: ""}), 
                                            React.createElement("div", {className: "sel_select sel_shi", onClick: that.showCityMenu, ref: "cityName"}, that.state.cityName), 
                                            React.createElement("ul", {className: "clearfix none"}, 
                                             that.state.citylist.map(function(item,i){
                                                                          return React.createElement("li", {"data-value": item.id+"##"+item.name, key: item.id, onClick: that.pickCity}, item.name)
                                                                                })
                                            )
                                          ), 
                                          React.createElement("div", {className: "f_sel", style: that.state.areaDisplay}, 

                                            React.createElement("div", {className: "sel_select sel_qu", onClick: that.showAreaMenu, ref: "areaName"}, that.state.areaName), 
                                            React.createElement("ul", {className: "clearfix none"}, 
                                             that.state.downlist.map(function(item,i){
                                                                          return React.createElement("li", {"data-value": item.id+"##"+item.name, key: item.id, onClick: that.pickArea}, item.name)
                                                                                })
                                            )
                                          )
                                           ), 
                                             React.createElement("div", {className: "error", style: that.state.cityTip}, that.state.cityTipText), 
                                            React.createElement("div", {className: "form_inp clearfix"}, 
                                              React.createElement("span", null, "街道地址："), 
                                              React.createElement("input", {className: "form_address", type: "text", ref: "street"})
                                            ), 
                                               React.createElement("div", {className: "form_inp error", style: that.state.addressDetailTip}, "请填写街道地址"), 
                                                  React.createElement("span", {className: "ma_next", onClick: that.handleFinishAddress}, "保存"), 
                                                  React.createElement("span", {className: "ma_next", onClick: that.checkAddressPop}, "返回")
                                          )
                                         )
                                          )
                                        
                                 )
          	      },
                                                       
    //弹出收货地址
    checkAddressPop:function(){
                         this.setState({
                                        addressLayer:{display:"none"},
                                        checkAddress:{display:"block"},
                                      });              
                  },
    //选择省份
    pickPro:function(event){
                          var that=this;
                          var $target=$(event.target);
                          var name=$target.text();
                          var value=$target.data("value");
                          var pro=that.refs.proName.getDOMNode();
                          pro.previousSibling.value=value;
                          that.setState({provinceName:name,isProvinceSelected:true});
                         $(pro.nextSibling).hide();

                        var tmpArr=value.split("##",2);
                            var provinceID=tmpArr[0];
                            var zhixiashiArr=["110000","120000","310000","500000"];
                            
                             if($.inArray(provinceID,zhixiashiArr)!=-1){
                                    that.setState({areaDisplay:{display:"none"}});
                             }else{
                                    that.setState({areaDisplay:{display:"inline-block"}});
                             }
                                that.refs.areaName.getDOMNode().nextSibling.style.display="none";
                                that.refs.cityName.getDOMNode().nextSibling.style.display="none";
                                 $.get("http://115.29.177.200/locations/cities/"+provinceID,function(cities){
                                     that.setState({
                                                    citylist:cities.locations,
                                                    cityName:"请选择城市",
                                                    downlist:[],
                                                    areaName:"请选择区县"
                                         })
                         },'json');
                   },

                                

                        
                       
                             
                        

    //隐藏或显示省份列表
    showProvincesMenu:function(event){
                            var that=this;
                            var $selectMenu=$(event.target.nextSibling);
                             if($selectMenu.is(":hidden")){
                                   $selectMenu.show();
                             }else{
                                   $selectMenu.hide();
                             }
                            
                         if(that.state.provlist.length == 0){
                               $.get("http://115.29.177.200/locations/provinces",function(provinces){
                                       
                                      that.setState({
                                            provlist:provinces.locations
                                           
                                      })
                                      
                               },'json');
                          }
                         
                   },
    //隐藏或显示城市列表
    showCityMenu:function(event){
                         var that=this;$selectMenu=$(event.target.nextSibling);
                          if($selectMenu.is(":hidden") && that.state.citylist.length != 0){
                                   $selectMenu.show();
                             }else{
                                   $selectMenu.hide();
                             }
                   },
                   //选择城市
                   pickCity:function(event){
                          var that=this;
                          var $target=$(event.target);
                          var name=$target.text();
                          var value=$target.data("value");
                          var city=that.refs.cityName.getDOMNode();
                          city.previousSibling.value=value;
                          that.setState({cityName:name,isCitySelected:true});
                          $(city.nextSibling).hide();//隐藏城市列表
                             
                          var  tmpArr=value.split("##",2);
                          var  cityID=tmpArr[0];
                         
                               $.get("http://115.29.177.200/locations/area/"+cityID,function(area){
                                     that.setState({   downlist:area.locations,areaName:"请选择区县"     });
                                    
                                    
                                   
                         },'json');
                          
                   },
    //显示或隐藏区县列表
    showAreaMenu:function(event){
                          var that=this;$selectMenu=$(event.target.nextSibling);
                          if($selectMenu.is(":hidden") && that.state.downlist.length != 0){
                                   $selectMenu.show();
                             }else{
                                   $selectMenu.hide();
                             }
                   },
    //选择区县
    pickArea:function(event){
                         var that=this;var $target=$(event.target);
                         var name=$target.text();
                         var value=$target.data("value");
                         that.refs.areaName.getDOMNode().nextSibling.style.display="none";
                         that.setState({areaName:name,isAreaSelected:true});
                         

                   },
    //检查填写的收货信息，并提交到数据库
    handleFinishAddress:function(){
                       var that=this,nowStateObj=that.state;
                       var phone=$.trim(that.refs.phone.getDOMNode().value);
                       var realName=$.trim(that.refs.realName.getDOMNode().value);
                       var pro=nowStateObj.provinceName;city=nowStateObj.cityName;
                       var street=$.trim(that.refs.street.getDOMNode().value);
                       var area;
                    
                     
                        //验证收货人是否空
                        if(!realName){
                                  that.setState({realNameTip:{display:"block"}});
                                  return;
                        }else{
                                  that.setState({realNameTip:{display:"none"}});
                        }
                        //验证手机号
                        if(!phone){
                              that.setState({phoneStyleTip:{display:"block"},phoneTipText:"请输入手机号码"})
                              return;
                        }else{
                                 if(!/^[1][3458][0-9]{9}$/.test(phone)){
                                  that.setState({phoneStyleTip:{display:"block"},phoneTipText:"手机号码格式不对"});
                                  return;
                               }else{
                                   that.setState({phoneStyleTip:{display:"none"}})
                               }
                            }
                            //验证省份城市区县
                           if(nowStateObj.isProvinceSelected != true){
                                      that.setState({cityTip:{display:"block"},cityTipText:"请选择省/直辖市"});
                                      return ;
                           }else if(nowStateObj.isCitySelected != true){
                                    that.setState({cityTip:{display:"block"},cityTipText:"请选择城市"});
                                    return ;
                           }else{
                                    
                                     if(that.state.areaDisplay.display=="inline-block" &&  nowStateObj.isAreaSelected != true){
                                                      that.setState({cityTip:{display:"block"},cityTipText:"请选择区县"});
                                                      return ;
                                     }else{
                                            that.setState({cityTip:{display:"none"}});
                                     }
                           }
                          //验证街道地址
                          if(!street){
                                  that.setState({addressDetailTip:{display:"block"}});
                                  return;
                          }else{
                                  that.setState({addressDetailTip:{display:"none"}});
                          }
                             //收货信息
                                   var addressInfo;
                                if(that.state.areaDisplay.display=="inline-block"){
                                     area=nowStateObj.areaName;
                                      addressInfo={
                                            province:pro,
                                            city:city,
                                            area:area,
                                            street:street,
                                            phone:phone,
                                            name:realName
                                     };

                               }else{
                                      addressInfo={
                                            province:pro,
                                            city:city,
                                            street:street,
                                            phone:phone,
                                            name:realName
                                           
                                     };
                             }
                             
                            $.post("bootstrap.php?s=/user/updateAddress",addressInfo,function(response){
                                       
                                        if(response.message=="Y"){
                                                   var tmpStr;
                                           if(addressInfo.area==undefined){
                                                    tmpStr='';
                                           }else{
                                                   tmpStr=addressInfo.area;
                                           }
                                           
                                         var str=addressInfo.province+addressInfo.city+tmpStr+addressInfo.street+" "+addressInfo.name+"(收) "+addressInfo.phone;
                                          var nextState={
                                                        addressLayer:{display:"none"},
                                                        checkAddress:{display:"block"},
                                                        addressText:str,
                                                        addressInfo:addressInfo
                                                        };
                                        if(that.props.topInfo.price==0.00){  nextState.btnText="马上学习";   }  
                                         that.setState(nextState);
                                        }
                            },"json");
                  },
                                       
                                           
    //修改地址
    useNewAddress:function(){
                              var that=this,addressInfo=that.state.addressInfo,areaDisplay;
                              var nextState={
                                         addressLayer:{display:"block"},
                                         citieslist:[],
                                         citylist:[],
                                         downlist:[],
                                         checkAddress:{display:"none"},
                                         provinceName:addressInfo.province,
                                         cityName:addressInfo.city,
                                        
                                         isProvinceSelected:true,
                                         isCitySelected:true,
                              }
                              if(addressInfo.area == undefined){

                                      nextState.areaDisplay={display:"none"};

                              }else{
                                    
                                     nextState.areaDisplay={display:"inline-block"};
                                     nextState.isAreaSelected=true;
                                     nextState.areaName=addressInfo.area;
                              }
                               that.setState(nextState);
                               that.refs.phone.getDOMNode().value=that.state.addressInfo.phone;
                               that.refs.realName.getDOMNode().value=that.state.addressInfo.name;
                               that.refs.street.getDOMNode().value=that.state.addressInfo.street;
                                       
                  },
    //去付款,或马上学习
    goToPay:function(event){
                          var that=this;
                          that.createOrder("Y");
                  },
                         
                             
    //登录验证处理
    handleClickLoginBtn:function(event){
                         event.preventDefault();
                         var that=this;
                         var username=that.refs.username.getDOMNode().value,password=that.refs.password.getDOMNode().value;
                         if(!username){that.setState({
                                 username:{display:"block"},
                                 wrongLoginInfo:{dislay:"none"},
                                 password:{display:"none"}
                             });
                              return;
                         }else{that.setState({username:{display:"none"}})}
                         if(!password){that.setState({
                                password:{display:"block"},
                                wrongLoginInfo:{dislay:"none"}
                          });
                          return;
                         }else{
                          that.setState({password:{display:"none"}})
                        }
                         var autologin=that.refs.autologin.getDOMNode().checked;
                          $.post("bootstrap.php?s=/user/dologin",{username:username,password:password,autologin:autologin},function(response){
                          if( response.message == 'Y'){
                                           var headerInfo={
                                                            username:response.username,
                                                            styleClass:"ti_user_list",
                                                            isShowUserInfoDiv:{display:"inline-block"},
                                                            isLogin:true,
                                                            head02:{display:"inline-block"},
                                                            head03:{display:"inline-block"}

                                             };
                                 React.render(React.createElement(Zhuan.header, {headerInfo: headerInfo}),document.getElementById('header'));
                                 var detail=that.props.topInfo;var tabjson=detail.attrs_list,tabArr=JSON.parse(tabjson),btn={};
                                 $.post("bootstrap.php?s=/goods/purchased",{goods_id:detail.goods_id},function(purchased){
                                                           var text;
                                                           if(purchased.message=='Y'){
                                                               if(that.props.type=='A'){text="进入教室";}else{text="查看购买";}
                                                               btn={text:text,bgColor:{backgroundColor:"#9BC83C"},purchased:1,snapid:purchased.snapid,orderid:purchased.orderid};
                                                            }else{
                                                              if(that.props.type=='A'){text="立即参加";}else{text="立即预定";}
                                                               btn={text:text,bgColor:{backgroundColor:"#FF8400"},purchased:0};
                                                            }
                                                         that.setState({
                                                                        loginLayer:{display:"none"},
                                                                        wrongLoginInfo:{display:"none"},
                                                                        username:{display:"none"},
                                                                        password:{display:"none"},
                                                                        loginSuccess:{display:"block"}
                                                                        });  
                                                   
                                    React.render(React.createElement(Zhuan.detail, {topInfo: detail, tab: tabArr, btn: btn, type: that.props.type}),document.getElementById('content')); 
                                    },'json');
                               
                                       }else{
                                            that.setState({wrongLoginInfo:{display:"block"}})
                                         
                                       }         
                                },'json');
                                },
                     
                        

                                 
    //关闭登录窗口
    handleCancelBtn:function(){
                  var that=this;
                  that.refs.password.getDOMNode().value='';
                  that.setState({
                                    loginLayer:{display:"none"},
                                    username:{display:"none"},
                                    password:{display:"none"},
                                    wrongLoginInfo:{display:"none"}
                    })
                    

          },
    //关闭提示登录成功窗口
    closeLoginSuccessTip:function(event){
                 var pur=this.props.btn.purchased;
                 if(pur==0){ this.handleClickJoin();}
                 event.preventDefault();
                 this.setState({loginSuccess:{display:"none"}});
          }

                           
  });
                                          

                                       
                                                                     
         
                              

                              

                                   
                                                
                                                  
                                               
                                                      
                                            
                                           


                                                  
                                                
                                                 
                                                 
                                             



                                     
            
                                            
                                            
                             
                                          
                                          
                                           
                                              
                                               

                               
                           
                           

                                           
                             
                            
                                            
                                                              
                                                                    

                                                                     
                                                                     
              
           
                
                                         
                                        


                              
                              



                             


                                           
                                                        
                 
                    
               




          
                       

                           
                                
                                     
                                      
                                             
                                           
                                         
                                    
                                       

                               
                                
                                     
                                           
       


                                    
                                    
                                    
                                   
                                     

                     
                                   
                                    
                                  
                           
                            

                                                               

                                                                          
                                                          

                                        
                                       
                                            
                                                  
                                               
                                             

                                        


                             
                               

                    
                                  
                           
                    
                         
                                                                                
                                            
                                      
                            
                            

                          

                      

                                             
                        
          
                      
                          
                              
                            

                        
                       
                    

                     
                  



                    
                                                
                                                    
                                        
                                                     

                                              
                                                      
                                                      

                                                      
                                               

                                                 
                                 



                                                            
                                                 
                                                 
                                               

                               
                 



      



