if(!window.components) {window.components = {};}
window.components.loginbox=React.createClass({
      displayName:"登陆弹窗",
      getInitialState:function(){
            return {
             
              username:{display:"none"},
              password:{display:"none"},
              wrongLoginInfo:{display:"none"}
            
            };
      },
      componentWillReceiveProps:function(nextProps){
            this.setState({loginLayer:nextProps.loginLayer});
      },
      handleCancel:function(){
           $("#loginBox").remove();
      },
      doLogin:function(event){
          event.preventDefault();
          var self = this;
          var username = self.refs.username.getDOMNode().value;
          var password = self.refs.password.getDOMNode().value;
          var autologin= self.refs.autologin.getDOMNode().checked;

          if($.trim(username) == ""){ self.setState({"username":{"display":"block"}});return;}else{
                    self.setState({"username":{"display":"none"}});
          }
          if($.trim(password) == ""){ self.setState({"password":{"display":"block"}});return;}else{
                   self.setState({"password":{"display":"none"}})
          }
        
          $.post("bootstrap.php?s=/user/dologin",{username:username,password:password,autologin:autologin},function(response){
               
                  if(response.message == "Y"){ 
                         // $("#username_li").show();
                         // $("#username").text(response.username);
                         $("#loginBox").remove();
                         setAlert("登录成功",function(){window.location.reload()});
                       
                    }else{ 
                       self.setState({"wrongLoginInfo":{"display":"block"}});
                     }
                 
          },"json");

      },
      render:function(){
           var self=this;
        
            return  React.createElement("div", null, 
                           React.createElement("div", {className: "ma_cover_bg"}), 
                              React.createElement("div", {className: "log_content_model"}, 
                                  React.createElement("div", {className: "log_left log_moedel"}, 
                                    React.createElement("img", {src: "static/images/ma_cancel.png", onClick: self.handleCancel, style: {"cursor":"pointer"}}), 
                                    React.createElement("div", {className: "log_moedel_title"}, "登录砖题库"), 
                                    React.createElement("form", {action: "", method: "post", className: "form_login"}, 
                                       React.createElement("div", {className: "error", id: "tips-username", style: self.state.username}, "请输入用户名"), 
                                       React.createElement("div", {className: "inp inp_account"}, 
                                           React.createElement("input", {type: "text", className: "account", placeholder: "请输入题库账号/教育网账号/网校账号", ref: "username"})
                                       ), 
                                       React.createElement("div", {className: "error", id: "tips-password", style: self.state.password}, "请输入密码"), 
                                        React.createElement("div", {className: "inp inp_pass"}, 
                                            React.createElement("input", {type: "password", className: "password", placeholder: "密码", ref: "password"})
                                        ), 
                                        React.createElement("div", {className: "error", id: "tips-username", style: self.state.wrongLoginInfo}, "用户名或密码错误"), 
                                        React.createElement("div", {className: "Cinput"}, 
                                            React.createElement("input", {type: "checkbox", name: "keeplogin", value: "1", id: "checkRegister", ref: "autologin", className: "checkInput", title: "同意"}), 
                                            React.createElement("div", {className: "C-zidong"}, React.createElement("label", null, "下次自动登录")), 
                                            React.createElement("div", {className: "C-forget"}, React.createElement("a", {href: "#"}, "忘记密码？"))
                                        ), 
                                        React.createElement("div", {className: "btn_login"}, 
                                            React.createElement("button", {type: "submit", className: "btn_re", onClick: self.doLogin, style: {"cursor":"pointer"}}, "登录")
                                        ), 
                                        React.createElement("div", {className: "Cinput"}, 
                                           React.createElement("div", {className: "no_account"}, "还没有账号？立即", React.createElement("a", {href: "http://tiku.huatu.com/index.php?mod=user&act=register&ref=", className: "regist"}, "注册"))
                                        )
                                    )
                                  )
                              )
                      )
                                   
         },
      
          handleCancelBtn:function(){
            var self = this;
                self.refs.username.getDOMNode().value='';
                self.refs.password.getDOMNode().value='';
                // self.setState({
                //       loginLayer:{display:"none"},
                //       username:{display:"none"},
                //       password:{display:"none"},
                //       wrongLoginInfo:{display:"none"}

                //     })
          },
        
   })


window.components.location = React.createClass({
       displayName:"填写地址弹窗",
       getInitialState:function(){
            return {
                    provlist:[],
                    citylist:[],
                    downlist:[],
                    phoneStyleTip:{display:"none"},
                    realNameTip:{display:"none"},
                    cityTip:{display:"none"},
                    addressDetailTip:{display:"none"},
                    phoneTipText:"",
                    checkAddress:{display:"none"},
                    areaDisplay:{display:"inline-block"},
                    provinceName:"请选择省/直辖市",
                    isProvinceSelected:false,
                    isCitySelected:false,
                    isAreaSelected:false,
                    cityName:"请选择城市",
                    areaName:"请选择区县",
                   }
       },
       componentDidMount:function(){
            var self    = this;
            var address = this.props.address;
            $(".form_person").attr("value",address.name);
            $(".form_phone").attr("value",address.phone);
            self.setState({provinceName:address.province,isProvinceSelected:true});
            self.setState({cityName:address.city,isCitySelected:true});
          
           
            if(address.area == undefined){
               self.setState({"areaDisplay":{"display":"none"},isAreaSelected:true});
             }else{
                self.setState({areaName:address.area,isAreaSelected:true});
             }
            $(".form_street").attr("value",address.street);
       },
       render:function(){
            var self = this;
            return  React.createElement("div", null, 
                          React.createElement("div", {className: "ma_cover_bg"}), 
                          React.createElement("div", {className: "ma_modle"}, 
                               React.createElement("h4", null, "填写收货地址"), 
                               React.createElement("p", null, "请如实填写以下的收货信息，以便我们尽快给您寄送资料。"), 
                                React.createElement("form", {id: "form"}, 
                                    React.createElement("img", {src: "static/images/ma_cancel.png", onClick: self.handleCancel}), 
                                            React.createElement("div", {className: "form_inp clearfix"}, 
                                              React.createElement("span", null, "收货人："), 
                                              React.createElement("input", {className: "form_person", type: "text", ref: "realName"})
                                            ), 
                                            React.createElement("div", {className: "error", style: self.state.realNameTip}, "请填写收货人姓名"), 
                                            React.createElement("div", {className: "form_inp clearfix"}, 
                                                 React.createElement("span", null, "手机号码："), 
                                                 React.createElement("input", {className: "form_phone", type: "text", ref: "phone"})
                                            ), 
                                            React.createElement("div", {className: "form-inp error", style: self.state.phoneStyleTip}, self.state.phoneTipText), 
                                            React.createElement("div", {className: "form_inp clearfix"}, 
                                               React.createElement("span", null, "地区："), 
                                               React.createElement("div", {className: "f_sel"}, 
                                               React.createElement("input", {type: "hidden", value: ""}), 
                                               React.createElement("div", {className: "sel_select sel_sheng", onClick: self.showProvincesMenu, ref: "proName"}, self.state.provinceName), 
                                               React.createElement("ul", {className: "clearfix none"}, 
                                                self.state.provlist.map(function(item,i){
                                           return React.createElement("li", {"data-value": item.id+"##"+item.name, key: i, onClick: self.pickPro}, item.name)
                                              })
                                               )
                                                ), 
                                                React.createElement("div", {className: "f_sel"}, 
                                                React.createElement("input", {type: "hidden", value: ""}), 
                                            React.createElement("div", {className: "sel_select sel_shi", onClick: self.showCityMenu, ref: "cityName"}, self.state.cityName), 
                                            React.createElement("ul", {className: "clearfix none"}, 
                                             self.state.citylist.map(function(item,i){
                                                                          return React.createElement("li", {"data-value": item.id+"##"+item.name, key: item.id, onClick: self.pickCity}, item.name)
                                                                                })
                                            )
                                          ), 
                                          React.createElement("div", {className: "f_sel area", style: self.state.areaDisplay}, 

                                            React.createElement("div", {className: "sel_select sel_qu", onClick: self.showAreaMenu, ref: "areaName"}, self.state.areaName), 
                                            React.createElement("ul", {className: "clearfix none"}, 
                                             self.state.downlist.map(function(item,i){
                                                                          return React.createElement("li", {"data-value": item.id+"##"+item.name, key: item.id, onClick: self.pickArea}, item.name)
                                                                                })
                                            )
                                          )
                                           ), 
                                             React.createElement("div", {className: "error", style: self.state.cityTip}, self.state.cityTipText), 
                                               React.createElement("div", {className: "form_inp clearfix"}, 
                                                React.createElement("span", null, "街道地址："), 
                                                 React.createElement("input", {className: "form_address form_street", type: "text", ref: "street"})
                                                ), 
                                                React.createElement("div", {className: "form_inp error", style: self.state.addressDetailTip}, "请填写街道地址"), 
                                                React.createElement("span", {className: "ma_next", onClick: self.handleFinishAddress}, "保存")
                                          )
                                    )
                              )
       },
handleCancel:function(){
      $("#loginBox").remove();
},
        //选择省份
pickPro:function(event){
      var self=this;
      var $target=$(event.target);
      var name=$target.text();
      var value=$target.data("value");
      var pro=self.refs.proName.getDOMNode();
      pro.previousSibling.value=value;
      self.setState({provinceName:name,isProvinceSelected:true});
     $(pro.nextSibling).hide();

    var tmpArr=value.split("##",2);
        var provinceID=tmpArr[0];
        var zhixiashiArr=["110000","120000","310000","500000"];
        
         if($.inArray(provinceID,zhixiashiArr)!=-1){
                self.setState({areaDisplay:{display:"none"}});
         }else{
                self.setState({areaDisplay:{display:"inline-block"}});
         }
            self.refs.areaName.getDOMNode().nextSibling.style.display="none";
            self.refs.cityName.getDOMNode().nextSibling.style.display="none";
             $.get("http://115.29.177.200/locations/cities/"+provinceID,function(cities){
                 self.setState({
                                citylist:cities.locations,
                                cityName:"请选择城市",
                                downlist:[],
                                areaName:"请选择区县"
                     })
     },'json');
},

                                

                        
                       
                             
                        

    //隐藏或显示省份列表
showProvincesMenu:function(event){
        var self=this;
        var $selectMenu=$(event.target.nextSibling);
         if($selectMenu.is(":hidden")){
               $selectMenu.show();
         }else{
               $selectMenu.hide();
         }
        
     if(self.state.provlist.length == 0){
           $.get("http://115.29.177.200/locations/provinces",function(provinces){
                   
                  self.setState({
                        provlist:provinces.locations
                       
                  })
                  
           },'json');
      }
     
},
    //隐藏或显示城市列表
showCityMenu:function(event){
   var self=this;$selectMenu=$(event.target.nextSibling);
    if($selectMenu.is(":hidden") && self.state.citylist.length != 0){
             $selectMenu.show();
       }else{
             $selectMenu.hide();
       }
},
//选择城市
pickCity:function(event){
    var self=this;
    var $target=$(event.target);
    var name=$target.text();
    var value=$target.data("value");
    var city=self.refs.cityName.getDOMNode();
    city.previousSibling.value=value;
    self.setState({cityName:name,isCitySelected:true});
    $(city.nextSibling).hide();//隐藏城市列表
       
    var  tmpArr=value.split("##",2);
    var  cityID=tmpArr[0];
   
         $.get("http://115.29.177.200/locations/area/"+cityID,function(area){
               self.setState({   downlist:area.locations,areaName:"请选择区县"     });
              
              
             
   },'json');
    
},
    //显示或隐藏区县列表
showAreaMenu:function(event){
    var self=this;$selectMenu=$(event.target.nextSibling);
    if($selectMenu.is(":hidden") && self.state.downlist.length != 0){
             $selectMenu.show();
       }else{
             $selectMenu.hide();
       }
},
    //选择区县
pickArea:function(event){
     var self=this;var $target=$(event.target);
     var name=$target.text();
     var value=$target.data("value");
     self.refs.areaName.getDOMNode().nextSibling.style.display="none";
     self.setState({areaName:name,isAreaSelected:true});
     

                   },
//检查填写的收货信息，并提交到数据库
handleFinishAddress:function(){
     var self  = this,nowStateObj=self.state;
     var phone = $.trim(self.refs.phone.getDOMNode().value);
     var realName=$.trim(self.refs.realName.getDOMNode().value);
     var pro=nowStateObj.provinceName;city=nowStateObj.cityName;
     var street=$.trim(self.refs.street.getDOMNode().value);
     var area;
                    
                     
      //验证收货人是否空
      if(!realName){
                self.setState({realNameTip:{display:"block"}});
                return;
      }else{
                self.setState({realNameTip:{display:"none"}});
      }
      //验证手机号
      if(!phone){
            self.setState({phoneStyleTip:{display:"block"},phoneTipText:"请输入手机号码"})
            return;
      }else{
               if(!/^[1][3458][0-9]{9}$/.test(phone)){
                self.setState({phoneStyleTip:{display:"block"},phoneTipText:"手机号码格式不对"});
                return;
             }else{
                 self.setState({phoneStyleTip:{display:"none"}})
             }
          }
          //验证省份城市区县
         if(nowStateObj.isProvinceSelected != true){
                    self.setState({cityTip:{display:"block"},cityTipText:"请选择省/直辖市"});
                    return ;
         }else if(nowStateObj.isCitySelected != true){
                  self.setState({cityTip:{display:"block"},cityTipText:"请选择城市"});
                  return ;
         }else{
                  
                   if(self.state.areaDisplay.display=="inline-block" &&  nowStateObj.isAreaSelected != true){
                                    self.setState({cityTip:{display:"block"},cityTipText:"请选择区县"});
                                    return ;
                   }else{
                          self.setState({cityTip:{display:"none"}});
                   }
         }
        //验证街道地址
        if(!street){
                self.setState({addressDetailTip:{display:"block"}});
                return;
        }else{
                self.setState({addressDetailTip:{display:"none"}});
        }
           //收货信息
                 var addressInfo;
              if(self.state.areaDisplay.display=="inline-block"){
                   area=nowStateObj.areaName;
                    addressInfo={
                          province:pro,
                          city:city,
                          area:area,
                          street:street,
                          phone:phone,
                          name:realName
                   };

             }else{
                    addressInfo={
                          province:pro,
                          city:city,
                          street:street,
                          phone:phone,
                          name:realName
                                               
                 };
         }
    
  $.post("bootstrap.php?s=/user/updateAddress/uid/"+AUTH.uid,addressInfo,function(response){
                 console.log(response);
                 if(response.message=="Y"){
                         var tmpStr;
                 if(addressInfo.area==undefined){
                          tmpStr='';
                 }else{
                         tmpStr=addressInfo.area;
                 }
                
               React.render(React.createElement(window.components.verifyAddress, {
                                          address: addressInfo, 
                                          btnContent: "立即支付", 
                                          root: self.props.root}),
                document.getElementById("loginBox"));
          
              }
  },"json");
  },
                                       
                                           
    //修改地址
useNewAddress:function(){
  var self=this,addressInfo=self.state.addressInfo,areaDisplay;
  var nextState={
             addressLayer:{display:"block"},
             citieslist:[],
             citylist:[],
             downlist:[],
             checkAddress:{display:"none"},
             provinceName:addressInfo.province,
             cityName:addressInfo.city,
            
             isProvinceSelected:true,
             isCitySelected:true,
  }
  if(addressInfo.area == undefined){
    nextState.areaDisplay={display:"none"};
  }else{
    nextState.areaDisplay={display:"inline-block"};
    nextState.isAreaSelected=true;
    nextState.areaName=addressInfo.area;
  }
   self.setState(nextState);
   self.refs.phone.getDOMNode().value=self.state.addressInfo.phone;
   self.refs.realName.getDOMNode().value=self.state.addressInfo.name;
   self.refs.street.getDOMNode().value=self.state.addressInfo.street;
   }
                                       


        
});


window.components.verifyAddress = React.createClass({
      displayName:"验证收货地址",
      getInitialState:function(){
        var address = this.props.address;
        var area = "";
        if(address.area != undefined){ area = address.area; }
        var str=address.province+address.city+area+address.street
                +" "+address.name+"(收) "+address.phone;
        return {"address":str}

            
           
      },
      handleCancel:function(){
           $("#loginBox").remove();
      },
      changeAddress:function(){
           React.render(React.createElement(window.components.location, {address: this.props.address}),document.getElementById("loginBox"));

      },
      goToPay:function(){
          var self = this;
          $("#loginBox").remove();
          var goodsID = self.props.root.goodsID;
          if(parseInt(self.props.root.price) == 0){
               var postData = {"goods_id": self.props.root.goods_id,
                         "price":self.props.root.price,
                         "uid":AUTH.uid,
                         "username":AUTH.username
                         };
                $.post("bootstrap.php?s=/order/init",postData,function(initResponse){
                   if(initResponse.message == "Y"){
                        var orderInfo = {"orderid":initResponse.orderid,
                                         "goodsid":self.props.root.goodsID,
                                         "mail":self.props.root.mail };
                        $.post("bootstrap.php?s=/order/pay",orderInfo,function(orderResult){
                              alert(orderResult.message);
                              if(orderResult.message=='Y'){
                                  window.location.assign("#order");
                               }
                        },"json");
                   }
               },"json");
          }else{
             var goodsInfo = {"goods_id":self.props.root.state.goods_id,
                              "price":self.props.root.state.price,
                              "name":self.props.root.state.name
                             };
             goodsInfo = encodeURIComponent(JSON.stringify(goodsInfo));
             window.location.assign("#purchase/"+goodsInfo);
         }


      },
      render:function(){
             var self = this;
             return  React.createElement("div", null, 
                          React.createElement("div", {className: "ma_cover_bg"}), 
                          React.createElement("div", {className: "ma_modle"}, 
                              React.createElement("img", {src: "static/images/ma_cancel.png", onClick: self.handleCancel}), 
                              React.createElement("h4", null, "请确认您的收货地址"), 
                              React.createElement("div", null, 
                                  React.createElement("p", {style: {fontSize:"18px",color:"#505050",fontWeight:"bolder"}}, 
                                  self.state.address, 
                                  React.createElement("span", {className: "change_address", onClick: self.changeAddress}, "修改地址")), 
                                  React.createElement("span", {className: "ma_next paybtn", onClick: self.goToPay}, "立即支付")
                              )
                            )
                      )
        }
});
                    
                                    
                                       
                                      
                                        
