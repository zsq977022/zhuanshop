if(!window.components){window.components = {};}

window.components.order = React.createClass({
	   "displayName":"用户购买记录主组件",
	   getInitialState:function(){
	   	    var self = this;
	   	    if(window.AUTH == undefined || window.AUTH == null) { window.location.assign("#zhibo"); return {};}
       		return {"content":React.createElement(window.components.orderAll, {root: self})};
	   },
	   goToAll:function(e){ 
	   	    $(e.target).addClass("li_current").siblings().removeClass("li_current");
            this.setState({"content":React.createElement(window.components.orderAll, null)})
	   },
	   goToFree:function(e){ 
            $(e.target).addClass("li_current").siblings().removeClass("li_current");
            this.setState({"content":React.createElement(window.components.orderFree, null)})
           
	   },
	   goToCharge:function(e){
	   	     $(e.target).addClass("li_current").siblings().removeClass("li_current");
	   	     this.setState({"content":React.createElement(window.components.orderCharge, null)})
	   },

	   renderNoRecords:function(){
             return React.createElement("div", {className: "ma_container"}, 
                         React.createElement("div", {className: "ma_wu"}, 
                           React.createElement("p", null, "购买成功的课程才会出现在这里，您还没有购买任何课程呦~"), 
                           React.createElement("a", {className: "ma_look", href: "#zhibo"}, "去砖商城看看")
                         )
                    )
	   },
	   componentDidMount:function(){
	   	    if(window.AUTH == undefined || window.AUTH == null) { window.location.assign("#zhibo");}
	   },
	   render:function(){
	   	     var self = this;
	   	     return React.createElement("div", {className: "ma_container"}, 
                        React.createElement("div", {className: "ma_had_title"}, "已购买商品"), 
                            React.createElement("div", {className: "ma_had"}, 
                                React.createElement("div", {className: "ma_tab"}, 
                                    React.createElement("ul", {className: "clearfix"}, 
                                        React.createElement("li", {className: "li_current", onClick: self.goToAll}, "全部"), 
                                        React.createElement("li", {onClick: self.goToFree}, "免费"), 
                                        React.createElement("li", {onClick: self.goToCharge}, "收费")
                                    )
                                ), 
                               
                                   self.state.content
                               
                            )
                     )
	   }

})


window.components.orderAll = React.createClass({
	   "displayName":"全部",
	   getInitialState:function(){
	   	   return {"items":[],"ajaxUrl":"bootstrap.php?s=/order/course/uid/"+AUTH.uid+"/type/all/page/"}
	   },
	   componentDidMount:function(){
             
	   },
       // 数据逆向流动
	   getDataByPager:function(rows){  console.log(rows);
	   	        var self = this;
				// {"orders":[
				// {"order_time":"2015\u5e749\u670822\u65e5 15:52:14",
				// "orderid":"719",
				// "snap_id":"991",
				// "name":"\u6d4b\u8bd5\u7684\u5546\u54c1(\u6d4b\u8bd5\u5b8c\u540e\u5c31\u5220\u9664)",
				// "goods_id":"74","shangtime":"2015\u5e748\u670827\u65e5",
				// "endtime":"2015\u5e748\u670831\u65e5",
				// "nav":"1",
				// "author":"\u674e\u5efa\u82f1",
				// "amount":"0.10",
				// "is_give":"0",
				// "js":{"goods_id":"74",
				// "name":"\u6d4b\u8bd5\u7684\u5546\u54c1(\u6d4b\u8bd5\u5b8c\u540e\u5c31\u5220\u9664)",
				// "startime":"1440639900",
				// "endtime":"1440987000",
				// "nav":"1"}
				// }]}
			   if(rows == undefined){ rows = []}
              
               if( rows.length == 0){ 
                self.props.root.render = self.props.root.renderNoRecords;
                self.props.root.forceUpdate();
                return;
               }
                self.setState({items:rows});
	   },
	   viewDetails:function(event){
              var goodsid = $(event.target).data("goodsid");
              var snapid  = $(event.target).data("snapid");
              var orderid = $(event.target).data("orderid");
              var resourceInfo = {"goodsid":goodsid,"snapid":snapid,"orderid":orderid};
              var data = encodeURIComponent(JSON.stringify(resourceInfo));
              window.location.assign("#resource/"+data);
	   },
	   render:function(){
	   	   var self = this;
           console.log(self.state.items);
	   	   return React.createElement("div", null, 
	   	             React.createElement("div", {className: "ma_class "}, 
		   	            React.createElement("ul", {className: "clearfix"}, 
	                      self.state.items.map(function(v){
	                             return     React.createElement("li", null, 
	                                           React.createElement("span", {className: "ma_class_name", 
	                                                 "data-goodsid": v.goods_id, 
	                                                 "data-snapid": v.snap_id, 
	                                                 onClick: self.viewDetails}, v.name), 
	                                           React.createElement("div", {className: "info"}, 
	                                              React.createElement("span", {className: "shang"}, "有效期：", v.shangtime, "-", v.endtime), 
	                                              React.createElement("span", {className: "teachers", title: v.author}, "授课老师：", v.author), 
	                                              React.createElement("span", {className: "paytime"}, v.is_give!=1?"购买时间":"赠送时间", "：", v.order_time), 
	                                              React.createElement("span", {className: "ma_check", 
	                                                    "data-goodsid": v.goods_id, 
	                                                    "data-snapid": v.snap_id, 
	                                                    "data-orderid": v.orderid, 
	                                                    onClick: self.viewDetails}, "查看详情")
	                                           )
	                                        )
	                       })
	                    )
	                 ), 
	                 React.createElement(window.components.pager, {ajaxUrl: self.state.ajaxUrl, onSendDataToParent: self.getDataByPager})
	              )

	   }
})
                                             
 window.components.orderFree = React.createClass({
	   "displayName":"免费课程",
	   getInitialState:function(){
	   	   return {"items":[],"ajaxUrl":"bootstrap.php?s=/order/course/uid/"+AUTH.uid+"/type/free/page/"}

	   },
	   componentDidMount:function(){
             
	   },
	   getDataByPager:function(rows){ 
	   	   if(rows == undefined){ rows = []}
           this.setState({"items":rows});
	   },
	   viewDetails:function(event){
           var goodsid = $(event.target).data("goodsid"); 
           window.location.assign("#resource/"+goodsid);
	   },
	   render:function(){
	   	     var self = this;
	   	     return React.createElement("div", null, 
	   	             React.createElement("div", {className: "ma_class "}, 
		   	            React.createElement("ul", {className: "clearfix"}, 
	                      self.state.items.map(function(v){
	                             return     React.createElement("li", null, 
	                                           React.createElement("span", {className: "ma_class_name", "data-goodsid": v.goods_id, onClick: self.viewDetails}, v.name), 
	                                           React.createElement("div", {className: "info"}, 
	                                              React.createElement("span", {className: "shang"}, "有效期：", v.shangtime, "-", v.endtime), 
	                                              React.createElement("span", {className: "teachers", title: v.author}, "授课老师：", v.author), 
	                                              React.createElement("span", {className: "paytime"}, v.is_give!=1?"购买时间":"赠送时间", "：", v.order_time), 
	                                              React.createElement("span", {className: "ma_check", "data-goodsid": v.goods_id, onClick: self.viewDetails}, "查看详情")
	                                           )
	                                        )
	                       })
	                    )
	                 ), 
	                 React.createElement(window.components.pager, {ajaxUrl: self.state.ajaxUrl, onSendDataToParent: self.getDataByPager})
	              )
	   }
})                                             
                                                            
                                                   
window.components.orderCharge = React.createClass({
	   "displayName":"收费课程",
	   getInitialState:function(){
	   	  return {"items":[],"ajaxUrl":"bootstrap.php?s=/order/course/uid/"+AUTH.uid+"/type/charge/page/"}

	   },
	   componentDidMount:function(){
             
	   },
	   getDataByPager:function(rows){
	   	   if(rows == undefined){ rows = []}
           this.setState({"items":rows})
	   },
	   viewDetails:function(event){
           var goodsid = $(event.target).data("goodsid"); 
           window.location.assign("#resource/"+goodsid);
	   },
	   render:function(){
	   	   var self = this;
	   	   return React.createElement("div", null, 
	   	             React.createElement("div", {className: "ma_class "}, 
		   	            React.createElement("ul", {className: "clearfix"}, 
	                      self.state.items.map(function(v){
	                             return     React.createElement("li", null, 
	                                           React.createElement("span", {className: "ma_class_name", "data-goodsid": v.goods_id, onClick: self.viewDetails}, v.name), 
	                                           React.createElement("div", {className: "info"}, 
	                                              React.createElement("span", {className: "shang"}, "有效期：", v.shangtime, "-", v.endtime), 
	                                              React.createElement("span", {className: "teachers", title: v.author}, "授课老师：", v.author), 
	                                              React.createElement("span", {className: "paytime"}, v.is_give!=1?"购买时间":"赠送时间", "：", v.order_time), 
	                                              React.createElement("span", {className: "ma_check", "data-goodsid": v.goods_id, onClick: self.viewDetails}, "查看详情")
	                                           )
	                                        )
	                       })
	                    )
	                 ), 
	                 React.createElement(window.components.pager, {ajaxUrl: self.state.ajaxUrl, onSendDataToParent: self.getDataByPager})
	              )
	   }
})                                                      		

                                                      
                                                       
                                            
