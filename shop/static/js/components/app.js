/*
   监控window.location 的hash 变化来加载不同的组件改变页面。

  
 */
window.app = React.createClass({
	 getInitialState:function(){
     	return {"route":window.location.hash.substr(1),"header": <window.components.header/>}
	 },

	 componentDidMount:function(){
     	var self = this;
     	var onHashChange = function(){
           
        	self.setState({"route":window.location.hash.substr(1)});
        }
        window.addEventListener("hashchange",onHashChange);
	 },
	 view:function(){
      	var self = this; var view = {"component":null,"props":{}};
        self.loginCheck();
     	  var route = self.state.route;
    
        if(route.length < 1){ view.component = window.components.zhibo; view.props = {};}
        if(route == "index") { view.component = window.components.index;  view.props = {};}
        if(route == "book") {view.component = window.components.book; view.props = {};}
        if(route == "zhibo") {view.component = window.components.zhibo; view.props = {};}
        if(/detail\/*/.test(route)){ view.component = window.components.detail; view.props = {} }
        if(/snap\/*/.test(route)){ view.component = window.components.detail; view.props = {} }

        if(route == "order"){ view.component = window.components.order; view.props = {};}
        if(/resource\/*/.test(route)){ view.component = window.components.resource; view.props = {};}
        if(/purchase\/*/.test(route)){ view.component = window.components.purchase;view.props = {};}
        return React.createElement(view.component,view.props);
	 },
     loginCheck:function(){
           
           document.cookie.split("; ").map(function(cookie,index){
                       try{
                           var key = cookie.split("=")[0];
                           var value = cookie.split("=")[1];
                           if(key === "ti_shop"){ 
                              
                                   var userInfo =JSON.parse(decodeURIComponent(value));
                               
                                   window.AUTH = {
                                               "username":userInfo.username,
                                               "uid":userInfo.uid

                                   }
                                   console.log(AUTH);
                                   // window.AUTH = {username: "bbc1009", uid: "7828161"}
                           }else{

                           }

                       }catch(e){
                             
                       }
           })

     },
	 render:function(){
	 	var self = this;
   
	    return (  <div>
                 {self.state.header}
                 {self.view()}
                 <window.components.footer/>
                 </div>
            )
                 
                 

	 }

})

//弹出窗口提示
//@msg   提示文本
window.setAlert = function(msg,callback){
          var maskHtml = '<div id="alert">'
                         +'<div class="ma_cover_bg" ></div>'
                         +'<div class="ma_confirm">'
                                +'<div class="ma_confirm_txt">'+msg+'</div>'
                                   +'<div class="ma_confirm_btn clearfix">'
                                   +'<a  class="ma_confirm_know fl" id="iknow">知道了</a>'
                                   +'</div>'      
                                +'</div>'
                         +'</div>';
          $(body).append(maskHtml);
          $("#iknow").on("click",function(){ $("#alert").remove();callback()})



}