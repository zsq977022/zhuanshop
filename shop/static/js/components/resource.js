if( !window.components) { window.components = {}};

window.components.resource = React.createClass({
       "displayName":"资源(课程)主组件",
       getInitialState:function(){
            var self = this;
            var hash = window.location.hash.substr(1).split("/");
            var data = JSON.parse(decodeURIComponent(hash[hash.length-1])); 
             console.log(data);
            self.goodsID = data.goodsid;
            self.snapid  = data.snapid;
            self.orderid = data.orderid;
            // var content = React.createElement(window.components.resourceAll)
       	    return {"content":window.components.resourceAll};

       },
       componentDidMount:function(){
              
       },
       viewDetails:function(event){
            var snapid = $(event.target).data("snapid"); 
                window.location.assign("#snap/"+snapid);
         
                 
          

       },
       goToAll:function(event){
            $(event.target).addClass("li_current").siblings().removeClass("li_current");
            this.setState({"content":window.components.resourceAll});
       },
       goToZhibo:function(event){
            $(event.target).addClass("li_current").siblings().removeClass("li_current");
            this.setState({"content":window.components.resourceZhibo});
       },
      goToDianbo:function(event){
            $(event.target).addClass("li_current").siblings().removeClass("li_current");
            this.setState({"content":window.components.resourceDianbo});
       },
      goToJiangyi:function(event){
            $(event.target).addClass("li_current").siblings().removeClass("li_current");
            this.setState({"content":window.components.resourceJiangyi});
       },
      goToBook:function(event){
            $(event.target).addClass("li_current").siblings().removeClass("li_current");
            this.setState({"content":window.components.resourceBook});
       },
       render:function(){
       	     var self = this;
            
       	     return <div className="ma_container">
       	          			<div className="ma_had_title">
                            	{self.props.name}
                            	<a  className="ma_look_detail" 
                                  onClick={self.viewDetails} 
                                  data-snapid={self.snapid}
                                  style={{"cursor":"pointer"}}>查看详情</a>
                        </div>
                        <div className="ma_had">
                            	<div className="ma_had_list">
                            				<ul className="clearfix">
                            					<li onClick={self.goToAll} className="li_current">全部</li>
                            					<li onClick={self.goToZhibo}>直播课</li>
                            					<li onClick={self.goToDianbo}>点播课</li>
                            					<li onClick={self.goToJiangyi}>讲义</li>
                            					<li className="li_last" onClick={self.goToBook}>图书资料</li>
                            				</ul>
                            	</div>
                            	{ React.createElement(self.state.content,{"goodsID":self.goodsID,"orderid":self.orderid})}
                        </div>
                    </div>
                              

       } 
})

window.components.resourceAll = React.createClass({
        "displayName":"全部资源",
        getInitialState:function(){
              return {"items":[]}
        },
        componentDidMount:function(){
              var self = this;
              var url = "bootstrap.php?s=/resource/get/type/all/goodsid/"+self.props.goodsID;
              $.get(url,function(data){
                    console.log(data);
                    if(data.message == "Y"){
                        self.setState({"items":data.rows})
                    }
              },"json")
        },
        componentDidUpdate:function(){
              $(".item-tiao").last().hide();
        },
        handleRightBtn:function(){
              
        },
        render:function(){
               var self = this;
               /*
                直播课  1 
                点播课  2
                讲义    3
                图示    4
                试卷    5

                */
               console.log(self.state.items);
               return   <div className="ma_class f1">
                              <h5>注：直播课结束3日内上传录播课件，请错过直播的同学3天之后到录播课中观看回放</h5> 
                              {self.state.items.map(function(v){ 
                                    var resource_type = parseInt(v.resource_type);
                                    if(resource_type == 1){
                                        return <div className="ma_boke clearfix"  >
                                                    <span className="item-circle"></span>
                                                    <span className="item-tiao"></span>
                                                    <div className="ma_boke_left fl">
                                                           <span style={{fontSize:"16px"}}>{v.duration}</span>
                                                           <p style={{fontSize:"16px",color:"#505050"}}>{v.author}--{v.name}</p>
                                                    </div>
                                                    <span className={v.btnClassName}>{v.btnText}</span>
                                               </div>     
                                    }else if(resource_type == 2){
                                          return <div className="ma_boke clearfix"  >
                                                        <span className="item-circle"></span>
                                                        <span className="item-tiao"></span>
                                                        <div className="ma_boke_left fl">
                                                               <p style={{fontSize:"16px",color:"#505050"}}>{v.duration}--{v.name}</p>
                                                                <span style={{fontSize:"14px",color:"#505050"}}>{v.author}</span>
                                                                          
                                                        </div>
                                                        <span className={v.btnClassName}>{v.btnText}</span>
                                                  </div>     
                                    }else{

                                  
                                    return <div className="ma_boke clearfix"  >
                                                <span className="item-circle"></span>
                                                <span className="item-tiao"></span>
                                                <div className="ma_boke_left fl">
                                                       <p style={{fontSize:"16px",color:"#505050"}}>{v.name}</p>
                                                </div>
                                                <span className={v.btnClassName}>{v.btnText}</span>
                                           </div>  
                                    }  
                              })}
                         </div>
        }
})

window.components.resourceZhibo = React.createClass({
        "displayName":"直播课",
        getInitialState:function(){
              return {"items":[]}
        },
        componentDidMount:function(){
              var self = this;
              var url = "bootstrap.php?s=/resource/get/type/all/goodsid/"+self.props.goodsID;
              $.get(url,function(data){
                    console.log(data);
                    if(data.message == "Y"){
                        for(var i=0;i<data.rows.length;i++){
                             if(parseInt(data.rows[i].resource_type) == 1){
                                    self.state.items.push(data.rows[i]);
                             }
                        }
                        console.log(self.items);
                        self.setState({"items":self.state.items})
                    }
              },"json")
        },
        componentDidUpdate:function(){
              $(".item-tiao").last().hide();
        },
        viewZhibo:function(event){
            if($(event.target).data("btnText")=="参加直播"){
                var resource_id=$(event.target).data("resid");
                window.open("./bootstrap.php?s=/resource/zhibo/resourceid/"+resource_id)
           }
        },
        render:function(){
               var self = this;     console.log(self.state.items); 
               console.log(self.state.items);
               return   <div className="ma_class f1">
                              <h5>注：直播课结束3日内上传录播课件，请错过直播的同学3天之后到录播课中观看回放</h5> 
                              {self.state.items.map(function(v){
                                    return <div className="ma_boke clearfix"  >
                                                <span className="item-circle"></span>
                                                <span className="item-tiao"></span>
                                                <div className="ma_boke_left fl">
                                                       <span style={{fontSize:"16px"}}>{v.duration}</span>
                                                       <p style={{fontSize:"16px",color:"#505050"}}>{v.author}--{v.name}</p>
                                                </div>
                                                <span className={v.btnClassName} 
                                                      onClick= {self.viewZhibo}
                                                      data-btnText={v.btnText}
                                                      data-resid={v.resource_id}>{v.btnText}</span>
                                           </div>     
                              })}
                         </div>
        }
})
                                 
 window.components.resourceDianbo = React.createClass({
        "displayName":"点播课",
        getInitialState:function(){
              return {"items":[]}
        },
        componentDidMount:function(){
              var self = this;
              var url = "bootstrap.php?s=/resource/get/type/all/goodsid/"+self.props.goodsID;
              $.get(url,function(data){
                    console.log(data);
                    if(data.message == "Y"){
                        for(var i=0;i<data.rows.length;i++){
                             if(parseInt(data.rows[i].resource_type) == 2){
                                    self.state.items.push(data.rows[i]);
                             }
                        }
                        console.log(self.items);
                        self.setState({"items":self.state.items})
                    }
              },"json")
        },
        componentDidUpdate:function(){
              $(".item-tiao").last().hide();
        },
        viewDianbo:function(event){
            if($(event.target).data("btnText")=="看点播课"){
                var resource_id=$(event.target).data("resid");
                window.open("./bootstrap.php?s=/resource/zhibo/resourceid/"+resource_id)
           }
        },
        render:function(){
               var self = this;
               console.log(self.state.items);
               return   <div className="ma_class f1">
                              <h5>注：直播课结束3日内上传录播课件，请错过直播的同学3天之后到录播课中观看回放</h5> 
                              {self.state.items.map(function(v){
                                    return <div className="ma_boke clearfix"  >
                                                <span className="item-circle"></span>
                                                <span className="item-tiao"></span>
                                                <div className="ma_boke_left fl">
                                                       <p style={{fontSize:"16px",color:"#505050"}}>{v.duration}--{v.name}</p>
                                                        <span style={{fontSize:"14px",color:"#505050"}}>{v.author}</span>
                                                                  
                                                </div>
                                                <span className={v.btnClassName} 
                                                      onClick= {self.viewDianbo}
                                                      data-btnText={v.btnText}
                                                      data-resid={v.resource_id}>{v.btnText}</span>
                                           </div>     
                              })}
                         </div>
        }
})                           
                           
window.components.resourceJiangyi = React.createClass({
        "displayName":"讲义",
        getInitialState:function(){
              return {"items":[]}
        },
        componentDidMount:function(){
              var self = this;
              var url = "bootstrap.php?s=/resource/get/type/all/goodsid/"+self.props.goodsID;
              $.get(url,function(data){
                    console.log(data);
                    if(data.message == "Y"){
                        for(var i=0;i<data.rows.length;i++){
                             if(parseInt(data.rows[i].resource_type) == 3){
                                    self.state.items.push(data.rows[i]);
                             }
                        }
                        console.log(self.items);
                        self.setState({"items":self.state.items})
                    }
              },"json")
        },
        componentDidUpdate:function(){
              $(".item-tiao").last().hide();
        },
        download:function(event){
            var downloadUrl=$(event.target).data('downloadurl'); 
            var filename=$(event.target).data('filename');
            var form=$("<form>");//定义一个form表单
            form.attr("style","display:none");

            form.attr("method","post");
            form.attr("action","bootstrap.php?s=/resource/download");
            var input1=$("<input>"),input2=$('<input>');
            input1.attr("type","hidden");
            input1.attr("name","url");
            input1.attr("value",downloadUrl);
            input2.attr("type","hidden");
            input2.attr("name","filename");
            input2.attr("value",filename);
            $("body").append(form);//将表单放置在web中
            form.append(input1);
            form.append(input2);
            form.submit();//表单提交 
        },
        render:function(){
               var self = this;
               console.log(self.state.items);
               return   <div className="ma_class f1">
                              <h5>注：直播课结束3日内上传录播课件，请错过直播的同学3天之后到录播课中观看回放</h5> 
                              {self.state.items.map(function(v){ 
                                    return <div className="ma_boke clearfix"  >
                                                <span className="item-circle"></span>
                                                <span className="item-tiao"></span>
                                                <div className="ma_boke_left fl">
                                                       <p style={{fontSize:"16px",color:"#505050"}}>{v.name}</p>
                                                </div>
                                                <span className={v.btnClassName}
                                                      data-downloadurl={v.download_url}
                                                      data-filename={v.name}
                                                      onClick={self.download}
                                                      >{v.btnText}</span>
                                           </div>     
                              })}
                         </div>
        }
})                                                    
                           
window.components.resourceBook = React.createClass({
        "displayName":"图书资料",
        getInitialState:function(){
              return {"items":[]}
        },
        componentDidMount:function(){
              var self = this;
              var url = "bootstrap.php?s=/resource/get/type/all/goodsid/"+self.props.goodsID;
              $.get(url,function(data){
                  
                    if(data.message == "Y"){
                        for(var i=0;i<data.rows.length;i++){
                             if(parseInt(data.rows[i].resource_type) == 4){
                                    self.state.items.push(data.rows[i]);
                             }
                        }
                         
                        self.setState({"items":self.state.items})
                    }
              },"json")
        },
        componentDidUpdate:function(){
              $(".item-tiao").last().hide();
        },
        viewlogictics:function(event){
             if($(event.target).data("btntext") !="查看物流"){ return false;}
             var orderid = $(event.target).data("orderid"); 
             $.get("bootstrap.php?s=/order/wuliu/orderid/"+orderid,function(res){
                 
                   
                    var logictics = '<div id="logistics">'
                                     +' <div class="ma_cover_bg"></div>'
                                     +' <div class="ma_modle">'
                                         +' <img src="static/images/ma_cancel.png"/>'
                                         +'  <p class="wuliu">物流单号：'+ res.company_num +'</p>'
                                         +'  <p class="wuliu">快递公司：'+ res.company_name+'</p>'
                                     +' </div>'
                                  +' </div>';
                    $("body").append(logictics);
                    $("#logistics img").on("click",function(){ $("#logistics").remove();})            
                                 
              },'json');
        },
        render:function(){
               var self = this;
               return   <div className="ma_class f1">
                              <h5>注：直播课结束3日内上传录播课件，请错过直播的同学3天之后到录播课中观看回放</h5> 
                           
                              {self.state.items.map(function(v){
                                    return <div className="ma_boke clearfix"  >
                                                <span className="item-circle"></span>
                                                <span className="item-tiao"></span>
                                                <div className="ma_boke_left fl">
                                                       <p style={{fontSize:"16px",color:"#505050"}}>{v.name}</p>
                                                </div>
                                                <span  className={v.btnClassName}
                                                       onClick={self.viewLogictics}
                                                       data-btntext={v.btnText}
                                                       data-orderid={self.props.orderid}
                                                       >{v.btnText}</span>
                                                      
                                                      
                                           </div>     
                              })}
                         </div>
        }
})                                                                                        
            
                                   
                                   
                                   

                                  
                                    
