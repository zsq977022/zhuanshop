/**
 * ---------------------------------------
 *
 *    显示商品绑定的资源页面   (直播，点播，讲义) 
 *    author:sunqi
 *    emil:sunqi_925@163.com
 *
 *
 * ----------------------------------------
 */
Zhuan.courseDetails=React.createClass({
               //初始化state
              getInitialState:function(){
                     return {resources:[],isShowBook:{display:"none"},isShowJiangyi:{display:"none"},showWuliuInfo:{display:"none"}};
              },
              //Tab切换
           	  handleTab:function(event){ 
                   var that=this,goods_id=that.props.goods_id,$target=$(event.target),resourceType=$target.data('type');
                       $target.addClass("li_current").siblings().removeClass("li_current");
                        switch(resourceType){

                                       case 0:
                                            that.setState({resources:that.state.allResource,type:0});
                                       break;
                                       case 1:
                                            that.setState({resources:that.state.zhiboArr,type:1});
                                       break;
                                       case 2:
                                            that.setState({resources:that.state.dianboArr,type:2});
                                       break;
                                       case 3:
                                            that.setState({resources:that.state.jiangyiArr,type:3});
                                       break;
                                       case 4:
                                            that.setState({resources:that.state.bookArr,type:4});
                                       break;
                                } 
                             
                            
           	    },
                //组件重新渲染更新时触发
                componentDidUpdate:function(){
                             var r=$(".ma_boke").last();
                             $(".item-tiao",r).hide();
                },
                //查看详情
                viewSnap:function(event){
                           var that=this;
                            event.preventDefault();
                            var id=$(event.target).data('id');
                        $.get("bootstrap.php?s=/order/getInfoBySnapId/id/"+id,function(data){
                                         // console.log(data);
                                         var nav=data.nav;
                                         var btn={};
                                         switch(nav){
                                             case '2':
                                             btn={text:"进入教室",bgColor:{backgroundColor:"#9BC83C"},purchased:1,snapid:id,orderid:that.props.orderid};
                                             break;
                                             case '1':
                                             btn={text:"查看购买",bgColor:{backgroundColor:"#9BC83C"},purchased:1,snapid:id,orderid:that.props.orderid};
                                             break;
                                         }
                                         var tab=JSON.parse(data.attrs_list);
                                  
                                   React.render(<Zhuan.detail  topInfo={data} tab={tab} btn={btn} type={that.props.type}/>,document.getElementById('content'));
                            },'json');

                },
                                        

               //页面UI渲染
           	   render:function(){
                        var that=this;
                        var snap=function(){
                             return   <a href="" className="ma_look_detail" onClick={that.viewSnap} data-id={that.props.snapid}>查看详情</a>
                        }
                        var jiangyi=function(item,i){
                               return   <div className="ma_boke clearfix" key={i}>
                                                                       <span className="item-circle"></span>
                                                                       <span className="item-tiao" ref="tiao"></span>
                                                                       <div className="ma_boke_left fl">
                                                                       <p style={{fontSize:"16px",color:"#505050"}}>{item.name}</p>
                                                                       </div>
                        <span className={item.btnClassName} data-href={item.url} onClick={that.dianbo} data-title={item.name} data-type={item.btnType} data-btn={item.btnText} data-download={item.download_url} data-filename={item.name} data-id={item.resource_id}>{item.btnText}</span>
                                     </div>  
                        }
                        var  zhibo=function(item,i){
                                 var type=item.resource_type,fn;
                            
                                 if(type==2){
                                      fn=function(){return   <div className="ma_boke_left fl">
                                                                <p style={{fontSize:"16px"}}>{item.author}--{item.name}</p>
                                                                    <span style={{fontSize:"14px",color:"#505050"}}>
                                                                    {item.duration}</span>
                                                                 </div>
                                                   }

                                 }else if(type==1){
                                      fn=function(){return   <div className="ma_boke_left fl">
                                                                    <span style={{fontSize:"16px"}}>{item.duration}</span>
                                                                       <p>{item.author}--{item.name}</p>
                                                                       </div>
                                                  }
                                 }else{
                                       fn=function(){return      <div className="ma_boke_left fl">
                                                                       <p style={{fontSize:"16px",color:"#505050"}}>{item.name}</p>
                                                                       </div>
                                                                     }
                                 }
                               return <div className="ma_boke clearfix" key={i} >
                                          <span className="item-circle"></span>
                                          <span className="item-tiao"></span>
                                          {fn()}
                                          <span className={item.btnClassName} data-href={item.url} 
                                                onClick={that.dianbo} data-title={item.name} data-type={item.btnType} 
                                                data-btn={item.btnText} data-download={item.download_url} 
                                                data-filename={item.name} data-id={item.resource_id}>
                                            {item.btnText}
                                          </span>
                                     </div>  
                                                                      
                                                                  
                        }
                        var dianbo=function(item,i){
                                return <div className="ma_boke clearfix" key={i}>
                                                                       <span className="item-circle"></span>
                                                                       <span className="item-tiao" ></span>
                                                                       <div className="ma_boke_left fl">
                                                                       <p style={{fontSize:"16px"}}>{item.author}---{item.name}</p>
                                                                       <span style={{fontSize:"14px",color:"#505050"}}>{item.duration}</span>
                                                                    </div>
                        <span className={item.btnClassName} data-href={item.url} onClick={that.dianbo} data-title={item.name} data-type={item.btnType} data-btn={item.btnText} data-download={item.download_url} data-filename={item.name} data-id={item.resource_id}>{item.btnText}</span>
                                     </div>  
                        }
                        // var type=that.state.type;
                        // if(type==2){
                        //   v=dianbo;
                        // }else if(type==3 || type==4){
                        //    v=jiangyi;
                        // }else{
                           v=zhibo;
                        // }
                     

                       

           	   	   return <div className="ma_container">
                               <div className="ma_had_title">
                            			 {that.props.name}
                            	    <a href="" className="ma_look_detail" onClick={that.viewSnap} data-id={that.props.snapid}>查看详情</a>
                            		</div>
                            		    <div className="ma_had">
                            			       <div className="ma_had_list">
                            				          <ul className="clearfix">
                                          					<li className="li_current" onClick={that.handleTab} data-type="0">全部</li>
                                          					<li onClick={that.handleTab} data-type="1">直播课</li>
                                          					<li onClick={that.handleTab} data-type="2">点播课</li>
                                          					<li onClick={that.handleTab} data-type="3" style={that.state.isShowJiangyi}>讲义</li>
                                          					<li className="li_last" onClick={that.handleTab} data-type="4" style={that.state.isShowBook}>图书资料</li>
                                          		</ul>
                                         </div>
                            			<div className="ma_class f1">
                            				<h5>	
                            					注：直播课结束3日内上传录播课件，请错过直播的同学3天之后到录播课中观看回放
                            				</h5>
                                        {that.state.resources.map(v)}
                            				</div>
                            				</div>

                                     <div style={that.state.showWuliuInfo}>
                                     <div className="ma_cover_bg"></div>
                                    <div className="ma_modle">
                                     <img src="images/ma_cancel.png" onClick={that.hideWuliuInfo}/>
                                         <p className="wuliu">物流单号：{that.state.company_num}</p>
                                         <p className="wuliu">快递公司：{that.state.company_name}</p>
                                    </div>
                                    </div>

                            				</div>
                                
                                                          
                                       
                                                   
           	           },
                       //关闭物流弹窗
                       hideWuliuInfo:function(){
                                this.setState({showWuliuInfo:{display:"none"}})
                       },
                       //点击不同按钮的处理
                       dianbo:function(event){
                              event.preventDefault();
                           var $target=$(event.target),type=$target.data('type'),that=this;
                            switch(type){
                                  case "zhibo":
                                    if($target.data("btn")=="参加直播"){
                                         var resource_id=$target.data("id");
                                         window.open("./bootstrap.php?s=/resource/zhibo/resourceid/"+resource_id)
                                    }
                                  break;
                                  case "dianbo":
                                       if($target.data("btn")=="看点播课"){
                                            var url=$target.data("href");
                                             if(/http:\/\/p.bokecc.com\/flash\/player.swf/.test(url)){
                                                var name=$target.data("title"),dianbo={title:name,url:url};
                                                var courseDetails={
                                                             goods_id:that.props.goods_id,
                                                             name:that.props.name,
                                                             snapid:that.props.snapid,
                                                             orderid:that.props.snapid,
                                                             type:that.props.type
                                                }
                                                var username=$("#username").text();
                                          React.render(<input type="hidden" id="username" value={username}/>,document.getElementById("header"));
                                          React.render(<Zhuan.dianbo dianbo={dianbo} returnInfo={courseDetails}/>,document.getElementById("content"));
                                          React.render(<div></div>,document.getElementById("footer"));
                                              }else{
                                             var resource_id=$target.data("id");
                                             window.open("./bootstrap.php?s=/resource/zhibo/resourceid/"+resource_id)
                                             }
                                       };
                                  break;
                                   case "download":
                                              event.preventDefault();
                                              var downloadUrl=$target.data('download');
                                              var filename=$target.data('filename');
                                              var form=$("<form>");//定义一个form表单
                                              form.attr("style","display:none");

                                              form.attr("method","post");
                                              form.attr("action","bootstrap.php?s=/resource/download");
                                              var input1=$("<input>"),input2=$('<input>');
                                              input1.attr("type","hidden");
                                              input1.attr("name","url");
                                              input1.attr("value",downloadUrl);
                                              input2.attr("type","hidden");
                                              input2.attr("name","filename");
                                              input2.attr("value",filename);
                                              $("body").append(form);//将表单放置在web中
                                              form.append(input1);
                                              form.append(input2);
                                              form.submit();//表单提交 
                                    break;
                                    case "wuliu":
                                          if($target.data("btn")=="查看物流"){
                                                var w=that.state.hasWuliuInfo;
                                                if(typeof w=="boolean" && w==true){
                                                        that.setState({showWuliuInfo:{display:"block"}})
                                                }else{

                                                    var orderid=that.props.orderid;
                                                  $.get("bootstrap.php?s=order/wuliu/orderid/"+orderid,function(res){
                                        that.setState({company_num:res.company_num,company_name:res.company_name,showWuliuInfo:{display:"block"},hasWuliuInfo:true});
                                                   },'json');
                                            }
                                          }
                                    break;

                                           
                                           
                                             



                                           
                            }
                    },
                    //组件挂载之后触发 ，获取商品课程绑定的所有资源
                    componentDidMount:function(){
                          var that=this,goods_id=that.props.goods_id,zhiboArr=[],dianboArr=[],jiangyiArr=[],bookArr=[]; 
                            $.get("bootstrap.php?s=/resource/get/type/all/goodsid/"+goods_id+"/orderid/"+that.props.orderid,function(data){
                                  if(data.message=="Y"){
                                    
                                          var r=data.rows,len=r.length;
                                          for(var i=0;i<len;i++){
                                                    switch(r[i].resource_type){
                                                          case "1":
                                                          zhiboArr[i]=r[i];
                                                          break;
                                                          case "2":
                                                          dianboArr[i]=r[i];
                                                          break;
                                                          case "3":
                                                          jiangyiArr[i]=r[i];
                                                          break;
                                                          case "4":
                                                          bookArr[i]=r[i];
                                                          break;
                                                    }
                                              }
                                               that.setState({resources:r,allResource:r,zhiboArr:zhiboArr,dianboArr:dianboArr,jiangyiArr:jiangyiArr,bookArr:bookArr});
                                                                                       
                                              //判断是否显示讲义和图书
                                              if(jiangyiArr.length>0){ that.setState({isShowJiangyi:{display:"block"}}) }
                                              if(bookArr.length>0){that.setState({isShowBook:{display:"block"}})}

                                             
                                        }else{
                                               that.setState({resources:[]});
                                              }     
                                },'json'); 

                    }
                                             
                   }); 
                                    
                               
                                           
                                                  
                                           
                                                    
                                            



               //CC点播UI
            Zhuan.dianbo=React.createClass({
                       render:function(){
                            return  <div className="video">
                                    <div className="header">
                                       <div className="gobackBtn" onClick={this.goBack}>返回</div>
                                       <div className="title">{this.props.dianbo.title}</div>
                                    </div>
                                   <div className="player"> <div id="player"></div></div>
                              </div>
                       },
                       componentDidMount:function(){
                              var that=this,swfUrl=that.props.dianbo.url;
                             
                              swfobject.embedSWF(swfUrl,"player","100%",'80%',"10.0.0","",
                                  {}, {allowfullscreen : 'true',wmode : 'opaque',allowScriptAccess:"always"});
                                
                                  window.on_cc_player_init=function(vid,objectID){
                                           
                                            var v=that.getSWF(objectID);
                                           
                                            var config={
                                                   on_player_ready:"on_player_ready"
                                            }
                                            v.setConfig(config);
                                               window.player=v;
                                  };
                                  window.get_cc_verification_code=function(vid){
                                         
                                    
                                  };
                             
         
                       },
                       getSWF:function(swfID){
                           if (window.document[ swfID ]) {
                                      return window.document[ swfID ];
                          } else if (navigator.appName.indexOf("Microsoft") == -1) {
                                      if (document.embeds && document.embeds[ swfID ]) {
                                            return document.embeds[ swfID ];
                                      }
                          } else {
                                      return document.getElementById( swfID );
                                    }
                       },
                       //返回按钮
                       goBack:function(){
                             var that=this,headerInfo;
                             var username=$("#username").val();
                                       headerInfo={
                                                  username:username,
                                                  styleClass:"ti_user_list",
                                                  isShowUserInfoDiv:{display:"inline-block"},
                                                  isLogin:true
                                              }
                              
                           React.render(<Zhuan.header headerInfo={headerInfo}/>,document.getElementById('header'));
                           React.render(<Zhuan.courseDetails goods_id={that.props.returnInfo.goods_id}  name={that.props.returnInfo.name} snapid={that.props.returnInfo.snapid} orderid={that.props.returnInfo.orderid} type={that.props.returnInfo.type} />,document.getElementById('content'));
                           React.render(<Zhuan.footer />,document.getElementById('footer'));
                       }
                           
      });  
                            
                               
                             
                          
                             
                                
                            
                         
                           
                         

                                    
                             
                          
                  
                             
                
                    
      

                                
                 

                                                       
                                            
                                          
                              
                                
                                          

                   
                                             
                                                  
                                                   
                                                      
                                                      
                                                    
                                                 
                                                      
                                              
                                          
                           
                             
                                   
                             
                                   
                             
                                
                                   
                             
                                  
                                   
                             
              
                                                                     
                                                           

                                                           

                   
                      

                    
                                       

                                          
                               
                                        
                                             




                                       
                                    
                                      
                                  
                                          
                                         
                                  
                                             
                                            
                        
 




                        

                           


                
                                        
                                       
                                        
                                          




                                            
                                    

















