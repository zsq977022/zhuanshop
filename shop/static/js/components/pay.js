if(!window.components){window.components = {};}
window.components.pay=React.createClass({
         	      getInitialState:function(){
                       //解析url hash 参数 获得微信和支付宝的支付地址，调用第三库/vendor/phpqrcode.php生成支付二维码
                       var hash = window.location.hash.split("/");
                       var payData = JSON.parse()
         	      	   return {mobile:{display:"block"},web:{display:"none"},layerPaySuccess:{display:"none"}};
         	      	   
         	      },
         	       webClick:function(event){
                        alert("请用微信支付");
         	       	    // event.target.className="current";
         	       	    // event.target.previousSibling.className="";
         	       	    // this.setState({mobile:{display:"none"},web:{display:"block"}});
         	       },
         	       mobileClick:function(event){
                       event.target.className="current";
                       event.target.nextSibling.className="";
                       this.setState({mobile:{display:"block"},web:{display:"none"}});
         	       },
                   labelClick:function(event){
                         var that=this;
                           if($('.label_radio input').length) {
                            $('.label_radio').each(function(){
                                $(that).removeClass('r_on');

                            });
                            $('.label_radio input:checked').each(function(){
                                $(that).parent('label').addClass('r_on');
                            });
                        };
                       
                  },
                  handlePayBtn:function(){
                           // alert('暂不支持该功能');
                           var that=this,postData={orderid:that.props.orderInfo.orderid,goodsid:that.props.orderInfo.goods_id};
                           $.post("bootstrap.php?s=/order/pay",postData,function(data){
                                     
                           })
                                       
                  },
                                     
                                    
                                               
                                   
                         
                  componentDidMount:function(){
                          var that=this;
                         
                          // if(that.props.orderInfo.price==0.00){
                          //          var postData={orderid:that.props.orderInfo.orderid,goodsid:that.props.orderInfo.goods_id,mail:that.props.orderInfo.mail};
                          //          $.post("bootstrap.php?s=/order/pay",postData,function(data){
                                         
                          //                 if(data.message=='Y'){
                          //                      that.setState({layerPaySuccess:{display:"block"}});
                          //                 }else{
                                               
                          //                 }
                                           
                          //                               },'json')
                                
                          // }else{


                          //在此页面轮询，是否支付成功
                         that.interval=setInterval(function(){ 
                                $.post("bootstrap.php?s=/order/haspay",{order_id:that.props.orderInfo.orderid},function(result){
                                      if(result.message=='Y'){
                                           clearInterval(that.interval);
                                           that.setState({layerPaySuccess:{display:"block"}});
                                      }
                                 
                                },'json');
                                    
                         },3000);


                        
                  },
                  //组件卸载后清除循环
                  componentWillUnmount:function(){
                         var that=this;
                         clearInterval(that.interval);
                      
                  },
                  //付款成功之后点击【马上学习】
                  handleAfterPaySuccess:function(event){
                        event.preventDefault();
                        var that = this;
                        that.setState({layerPaySuccess:{display:"none"}}),
                        React.render(<Zhuan.courseDetails  goods_id={that.props.courseDetail.goods_id} name={that.props.courseDetail.name} snapid={that.props.courseDetail.snapid} orderid={that.props.courseDetail.orderid} type={that.props.courseDetail.type}/>,document.getElementById("content"));

                  },
                            
                  render:function(){
                        var that=this;
                         //that.props.orderInfo.weixinQrcode = 'weixin:\/\/wxpay\/bizpayurl?pr=z3L6FtA';
                         //that.props.orderInfo.zhifubaoQrcode = 'https:\/\/qr.alipay.com\/bajst4f6vrebxddtd6';
                        //移动扫码支付微信和支付宝的二维码串
                        var weixinqrString = "bootstrap.php?s=/weixinqrpay/qrcode&data="+that.props.orderInfo.weixinQrcode;
                        var zhifubaoqrString = "bootstrap.php?s=/zhifubaoqrpay/qrcode&data="+that.props.orderInfo.zhifubaoQrcode;
         	       	    return  <div className="ma_container"> 
                                  <div className="ma_had_title">您的订单详情</div>
                                  <div className="ma_order_details">
                                      <p>所选课程：“{that.props.orderInfo.name}   ”需支付{parseFloat(that.props.orderInfo.price)}元<br/></p> 
                                      <h4>请选择在线支付方式（支付金额{parseFloat(that.props.orderInfo.price)}元）</h4>
                                      <div className="ma_type_tab">
                                      		<span  className="current" onClick={that.mobileClick}>移动扫码支付</span>
                                      		<span  onClick={that.webClick}>网页操作支付</span>
                                      </div>
                                      <div className="ma_order_type " style={that.state.mobile}>
                                          <div className="ma_weixin">
                                                <div className="weixin_code"><img src={weixinqrString}/></div>
                                          </div>
                                          <div className="ma_zhifub">
                                                <div className="zhifub_code"><img src={zhifubaoqrString}/></div>
                                          </div>      
                                      </div>
                                      <div className="ma_order_type" style={that.state.web}>
                                      <div className="ma_type_zhifu">
                                         <span>支付支付宝</span>
                                       <fieldset className="radios has-js">
                                            <label htmlFor="radio-01" className="label_radio r_on" onClick={that.labelClick}>
                                              <input type="radio" id="radio-01"  name="sample-radio"  style={{display:"none"}}/>
                                              <img src="static/images/img/ma_zhifubao.png"/>
                                            </label>
                                            <label htmlFor="radio-02" className="label_radio" onClick={that.labelClick}>
                                              <input type="radio" id="radio-02" name="sample-radio" style={{display:"none"}}/>
                                              <img src="static/images/img/ma_caifu.png"/>
                                            </label>
                                          <span className="ma_wangy">网银支付</span>
                                            <label htmlFor="radio-03" className="label_radio" onClick={that.labelClick}>
                                              <input type="radio" id="radio-03" name="sample-radio" style={{display:"none"}}/>
                                              <img src="static/images/img/ma_zhaoshang.png"/>
                                            </label>
                                            <label htmlFor="radio-04" className="label_radio" onClick={that.labelClick}>
                                              <input type="radio" id="radio-04" name="sample-radio" style={{display:"none"}}/>
                                              <img src="static/images/img/ma_gonghang.png"/>
                                            </label>
                                            <label htmlFor="radio-05" className="label_radio"  onClick={that.labelClick}>
                                              <input type="radio" id="radio-05" name="sample-radio" style={{display:"none"}}/>
                                              <img src="static/images/img/ma_jianshe.png"/>
                                            </label>
                                            <label htmlFor="radio-06" className="label_radio" onClick={that.labelClick}>
                                              <input type="radio" id="radio-06" name="sample-radio" style={{display:"none"}}/>
                                              <img src="static/images/img/ma_nongye.png"/>
                                            </label>
                                            <label htmlFor="radio-07" className="label_radio" onClick={that.labelClick}> 
                                              <input type="radio" id="radio-07" name="sample-radio" style={{display:"none"}}/>
                                              <img src="static/images/img/ma_zhongguo.png"/>
                                            </label>
                                            <label htmlFor="radio-08" className="label_radio" onClick={that.labelClick}>
                                              <input type="radio" id="radio-08" name="sample-radio" style={{display:"none"}}/>
                                              <img src="static/images/img/ma_jiaotong.png"/>
                                            </label>
                                            <label htmlFor="radio-09" className="label_radio" onClick={that.labelClick}>
                                              <input type="radio" id="radio-09" name="sample-radio" style={{display:"none"}}/>
                                              <img src="static/images/img/ma_youzheng.png"/>
                                            </label>
                                            <label htmlFor="radio-10" className="label_radio" onClick={that.labelClick}>
                                              <input type="radio" id="radio-10" name="sample-radio" style={{display:"none"}}/>
                                              <img src="static/images/img/ma_guangfa.png"/>
                                            </label>
                                            <label htmlFor="radio-11" className="label_radio" onClick={that.labelClick}>
                                              <input type="radio" id="radio-11" name="sample-radio" style={{display:"none"}}/>
                                              <img src="static/images/img/ma_pufa.png"/>
                                            </label>
                                            <label htmlFor="radio-12" className="label_radio" onClick={that.labelClick}>
                                              <input type="radio" id="radio-12" name="sample-radio" style={{display:"none"}}/>
                                              <img src="static/images/img/ma_guangda.png"/>
                                            </label>
                                            <label htmlFor="radio-13" className="label_radio" onClick={that.labelClick}>
                                              <input type="radio" id="radio-13" name="sample-radio" style={{display:"none"}}/>
                                              <img src="static/images/img/ma_pingan.png"/>
                                            </label>
                                            <label htmlFor="radio-14" className="label_radio" onClick={that.labelClick}>
                                              <input type="radio" id="radio-14" name="sample-radio" style={{display:"none"}}/>
                                              <img src="static/images/img/ma_xingye.png"/>
                                            </label>
                                          </fieldset>
                                       </div>
                                       <div className="ma_fukuan"  onClick={that.handlePayBtn}>付款</div>
                                      </div>
                                    </div>
                                              <div className="ma_cover_bg" style={that.state.layerPaySuccess}></div>
                                              <div className="ma_confirm" style={that.state.layerPaySuccess}>
                                                    <div className="ma_confirm_txt">支付成功</div>  
                                                    <div className="ma_confirm_btn clearfix">
                                                       <a href="" className="ma_confirm_know fl" id="pay" onClick={that.handleAfterPaySuccess}>马上学习</a>
                                                    </div>      
                                              </div>
                                    </div>
                                      	}
                                           
          });    

                              


                            
                           
                         

                
                                            
                                            
                                               
                                               
                                             
                                            

                                          

                                      

                                                   
                                              
  
                                               
                                                      
                                            
                     
                  









                                  








