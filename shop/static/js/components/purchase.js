if(!window.components) {window.components = {};}
window.components.purchase=React.createClass({
    displayName:"付款",
    getInitialState:function(){
    	  if(window.AUTH == undefined){window.location.assign("#zhibo")}
          var hashArr  = window.location.hash.split("/");
          var goodsInfo  = JSON.parse(decodeURIComponent(hashArr[hashArr.length-1])); //商品ID
          console.log(goodsInfo);
          goodsInfo.weixinQrcode   = "";
          goodsInfo.zhifubaoQrcode = "";
          return goodsInfo;

    },
    componentDidMount:function(){
    	 var self = this;
         var postData = {"goods_id": self.state.goods_id,
                         "price":self.state.price,
                         "uid":AUTH.uid,
                         "username":AUTH.username
                         };

         $.post("bootstrap.php?s=/order/init",postData,function(initResponse){
               if(initResponse.message == "Y"){
               	       // 组装二维码数据                             
                        var qrcodeParams = {
                                product_id:self.state.goods_id, //商品id
                                body:self.state.name,   //商品名称
                                total_fee:self.state.price, //商品价格
                                order_id:initResponse.orderid,   //初始化生成的订单id
                                order_number:initResponse.order_number, //订单编号
                                mail:"Y"
                                  };
                                                            
                   $.post('bootstrap.php?s=/order/qrcode',qrcodeParams , function(data){
                   	 console.log(data);
                     
                     self.setState({"weixinQrcode":data.weixin,"zhifubaoQrcode":data.zfb}) 
                     self.lunxun(initResponse.orderid);                          
                      // var courseDetail={
                      //                   goods_id:that.props.topInfo.goods_id,
                      //                   name:that.props.topInfo.name,
                      //                   snapid:result.snapid,
                      //                   orderid:result.orderid,
                      //                   type:that.props.type
                      //      }
                                       
                             
               },"json");
           }
       },"json");
    },
    lunxun:function(order_id){
    	//轮询数据库是否有购买记录
    	 var self = this;
    	 var interval = 2*60*60*1000; 
         self.interval = setInterval(function(){ 
            interval -= 1000; 
            if(interval==0){ setAlert("页面失效",function(){window.location.reload();});
                   clearInterval(self.interval); return false;
               }
             // console.log(123);
            $.post("bootstrap.php?s=/order/haspay",{order_id:order_id},function(result){
            	  // console.log(interval);
            	  // if(interval == 3000){ result.message = "Y";}
                  if(result.message=='Y'){
                       clearInterval(self.interval);
                       setAlert("支付成功",function(){window.location.assign("#order")})
                  }
             
            },'json');
                
     },1000);
    },
    render:function(){
    	var self = this;
        var weixinqrString = "bootstrap.php?s=/weixinqrpay/qrcode&data="+self.state.weixinQrcode;
        var zhifubaoqrString = "bootstrap.php?s=/zhifubaoqrpay/qrcode&data="+self.state.zhifubaoQrcode;
        return  <div className="ma_container"> 
                      <div className="ma_had_title">您的订单详情</div>
                      <div className="ma_order_details">
	                      <p>所选课程：“{self.state.name}   ”需支付{parseFloat(self.state.price)}元<br/></p> 
	                      <h4>请选择在线支付方式（支付金额{parseFloat(self.state.price)}元）</h4>
	                      <div className="ma_type_tab">
	                      		<span  className="current" onClick={self.mobileClick}>移动扫码支付</span>
	                      		<span  onClick={self.webClick}>网页操作支付</span>
	                      </div>
			              <div className="ma_order_type">
			                  <div className="ma_weixin">
			                        <div className="weixin_code"><img src={weixinqrString}/></div>
			                  </div>
			                  <div className="ma_zhifub">
			                        <div className="zhifub_code"><img src={zhifubaoqrString}/></div>
			                  </div>      
			              </div>
			              <div className="ma_order_type" style={{"display":"none"}}>
			              <div className="ma_type_zhifu">
			                 <span>支付支付宝</span>
			               <fieldset className="radios has-js">
			                    <label htmlFor="radio-01" className="label_radio r_on" onClick={self.labelClick}>
			                      <input type="radio" id="radio-01"  name="sample-radio"  style={{display:"none"}}/>
			                      <img src="static/images/img/ma_zhifubao.png"/>
			                    </label>
			                    <label htmlFor="radio-02" className="label_radio" onClick={self.labelClick}>
			                      <input type="radio" id="radio-02" name="sample-radio" style={{display:"none"}}/>
			                      <img src="static/images/img/ma_caifu.png"/>
			                    </label>
			                  <span className="ma_wangy">网银支付</span>
			                    <label htmlFor="radio-03" className="label_radio" onClick={self.labelClick}>
			                      <input type="radio" id="radio-03" name="sample-radio" style={{display:"none"}}/>
			                      <img src="static/images/img/ma_zhaoshang.png"/>
			                    </label>
			                    <label htmlFor="radio-04" className="label_radio" onClick={self.labelClick}>
			                      <input type="radio" id="radio-04" name="sample-radio" style={{display:"none"}}/>
			                      <img src="static/images/img/ma_gonghang.png"/>
			                    </label>
			                    <label htmlFor="radio-05" className="label_radio"  onClick={self.labelClick}>
			                      <input type="radio" id="radio-05" name="sample-radio" style={{display:"none"}}/>
			                      <img src="static/images/img/ma_jianshe.png"/>
			                    </label>
			                    <label htmlFor="radio-06" className="label_radio" onClick={self.labelClick}>
			                      <input type="radio" id="radio-06" name="sample-radio" style={{display:"none"}}/>
			                      <img src="static/images/img/ma_nongye.png"/>
			                    </label>
			                    <label htmlFor="radio-07" className="label_radio" onClick={self.labelClick}> 
			                      <input type="radio" id="radio-07" name="sample-radio" style={{display:"none"}}/>
			                      <img src="static/images/img/ma_zhongguo.png"/>
			                    </label>
			                    <label htmlFor="radio-08" className="label_radio" onClick={self.labelClick}>
			                      <input type="radio" id="radio-08" name="sample-radio" style={{display:"none"}}/>
			                      <img src="static/images/img/ma_jiaotong.png"/>
			                    </label>
			                    <label htmlFor="radio-09" className="label_radio" onClick={self.labelClick}>
			                      <input type="radio" id="radio-09" name="sample-radio" style={{display:"none"}}/>
			                      <img src="static/images/img/ma_youzheng.png"/>
			                    </label>
			                    <label htmlFor="radio-10" className="label_radio" onClick={self.labelClick}>
			                      <input type="radio" id="radio-10" name="sample-radio" style={{display:"none"}}/>
			                      <img src="static/images/img/ma_guangfa.png"/>
			                    </label>
			                    <label htmlFor="radio-11" className="label_radio" onClick={self.labelClick}>
			                      <input type="radio" id="radio-11" name="sample-radio" style={{display:"none"}}/>
			                      <img src="static/images/img/ma_pufa.png"/>
			                    </label>
			                    <label htmlFor="radio-12" className="label_radio" onClick={self.labelClick}>
			                      <input type="radio" id="radio-12" name="sample-radio" style={{display:"none"}}/>
			                      <img src="static/images/img/ma_guangda.png"/>
			                    </label>
			                    <label htmlFor="radio-13" className="label_radio" onClick={self.labelClick}>
			                      <input type="radio" id="radio-13" name="sample-radio" style={{display:"none"}}/>
			                      <img src="static/images/img/ma_pingan.png"/>
			                    </label>
			                    <label htmlFor="radio-14" className="label_radio" onClick={self.labelClick}>
			                      <input type="radio" id="radio-14" name="sample-radio" style={{display:"none"}}/>
			                      <img src="static/images/img/ma_xingye.png"/>
			                    </label>
			                  </fieldset>
			               </div>
			               <div className="ma_fukuan"  onClick={self.handlePayBtn}>付款</div>
			              </div>
			            </div>
			                     
			            </div>
			}
                                           
         

                              

    
 
})

