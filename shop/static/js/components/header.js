/**
 * ---------------------------------------
 *
 *    公共头部组件
 *    author:sunqi
 *    emil:sunqi_925@163.com
 *
 *
 * ----------------------------------------
 */  
if(!window.components) {window.components = {};}
    window.components.header=React.createClass({
                 
                  getInitialState:function(){
                      return {
                              userSelectMenu:{display:"none"},
                              nav:[],
                              showUserInfo:{display:'none'},
                              loginStyle:"ti_before_login",
                              loginLayer:{display:"none"},
                              headerTab:{display:"none"},
                              tikuTabImgsrc:"static/images/ti_head_01.png",
                              isShowWeixin:{display:"none"},
                              isShowWeibo:{display:"none"},
                              isShowMobileQrcode:{display:"none"},
                              usernameRightClassName:"username_right_down",
                              tikuTabCircleClassName:"tikutab_circle_down"


                            
                            };
                    },
           	      showSelectMenu:function(isShow){
                   var isLogin=this.props.headerInfo.isLogin;
                   
                    if(typeof isLogin=="boolean" &&　isLogin==true){
                              
                        this.setState({ userSelectMenu:{display:"block"},loginLayer:{display:"none"}, usernameRightClassName:"username_right_up"});
                    }else{
                         this.setState({loginLayer:{display:"none"}});
                    }
                          
           	        },
                    //隐藏用户下拉菜单
                    hideSelectMenu:function(event){
                       this.setState({ userSelectMenu:{display:"none"},usernameRightClassName:"username_right_down"})
                  },
                                                      
                          
                    //我的订单
           	        showMyShop:function(event){
                            event.preventDefault();
           	        	      React.render(<Zhuan.orderlist/>,document.getElementById("content"));
           	        },
                    //查看消息
           	        checkMessage:function(){
                          window.open("http://tiku.huatu.com/index.php?mod=message&act=message_list");
                    },
           	       
                   
                    //退出
                   handleExist:function(){
                      var that=this;
                            $.get("bootstrap.php?s=/user/loginout",function(response){ 
                                   // console.log(response);
                            that.setProps({headerInfo:{
                                       username:'',
                                       styleClass:"ti_before_login",
                                       isShowUserInfoDiv:{display:"none"},
                                       head02:{display:"none"},
                                       head03:{display:"none"},
                                       isLogin:false
                                     }
                            });
                                      
                        

                            

                            React.render(<Zhuan.live ajaxUrl="bootstrap.php?s=/goods/index/navid/2/page/" />,document.getElementById("content"));
                            $("#nav li").eq(0).addClass("nav_select").removeClass("nav_deault").siblings().removeClass("nav_select");
                             })
                           
                    },
                    //显示用户个人菜单
                    handleUserMenu:function(event){
                                    this.setState({userSelectMenu:{display:"block"}})
                    },
                    //加载导航数据
                    componentDidMount:function(){
                           var that=this;
                           that.setState({nav:[{name: "砖直播",nav_id: "2",className:"nav_select"},{name: "砖图书",nav_id: "1",className:"nav_deault"}]})
                    },
                                           
                      //导航切换事件处理
                   handleNavTab:function(event){
                             var $target=$(event.target),  navid=$target.data("navid"),that=this;
                             $target.removeClass("nav_deault").addClass("nav_select").siblings().removeClass("nav_select").addClass("nav_deault"); 
                             if(navid==1){  window.location.assign("#book"); }
                             if(navid==2){  window.location.assign("#zhibo");}
                             
                                   
                           
                              
                          

                     },
                  //点击登录     
                     showLoginLayer:function(){
                          var that=this;
                          
                         if(that.props.headerInfo.isLogin != true){
                             that.setState({
                                  loginLayer:{display:"block"}
                             })
                          }
                                  
                                  

                         
                     },
                    
                     showHeaderTab:function(){
                            this.setState({
                                   headerTab:{display:"block"},
                                   tikuTabCircleClassName:"tikutab_circle_up",
                                   loginLayer:{display:"none"}
                            })
                     },
                     hideHeaderTab:function(){
                            this.setState({
                                   headerTab:{display:"none"},
                                   tikuTabCircleClassName:"tikutab_circle_down"
                            })
                     },
                
                     mouseOnWeixin:function(event){
                           var that=this;
                           that.setState({isShowWeixin:{display:"block"}});
                           event.target.onmouseout=function(){
                                     that.setState({isShowWeixin:{display:"none"}});
                           }

                     },
                     mouseOnWeibo:function(event){
                            var that=this;
                            this.setState({isShowWeibo:{display:"block"}});
                             
                     },
                     clickWeixin:function(event){
                           event.preventDefault();
                     },
                     mouseOutWeibo:function(){
                           this.setState({isShowWeibo:{display:"none"}})
                     },
                     mouseOnWeiboBox:function(){ 
                         this.setState({isShowWeibo:{display:"block"}})
                     },
                     weiboGuanZhu:function(){
                          window.open("http://weibo.com/htztk");
                     },
                     mouseOnMobile:function(event){
                       var  that=this;
                           that.setState({isShowMobileQrcode:{display:"block"}});
                            event.target.onmouseout=function(){
                                     that.setState({isShowMobileQrcode:{display:"none"}});
                           }
                          
                     },
                            
           	        render:function(){
                                   var that=this;
           	        	    return  <div>
                                    <div className="ti_header_top">
  		                              <div className="header_Top" >
            	                        <div className="top_left  fl" onMouseOver={that.showHeaderTab} onMouseOut={that.hideHeaderTab}>
                	                     <a href="http://tiku.huatu.com/index.php?mod=administration&act=index" target="_blank">公务员题库</a>
                                           <div className={that.state.tikuTabCircleClassName}>&gt;</div>
                                      </div>
            	                         <div className="top_right fr">
                                        <ul className="clearfix">
                                        <li className={that.props.headerInfo.styleClass}
                                          onMouseOver={that.showSelectMenu } onMouseOut={that.hideSelectMenu}>
                                        <div className="ti_user_txt" id="ti_user_txt" >
                                          <span className="username_left_circle" style={that.props.headerInfo.head02}></span>
                                        <span onClick={that.showLoginLayer} id="username">{that.props.headerInfo.username?that.props.headerInfo.username:"登录"}</span>
                                          <span className={that.state.usernameRightClassName} style={that.props.headerInfo.head03}>&gt;</span>
                                        </div>
                                                <div  className="dl_down" style={that.state.userSelectMenu} onMouseOut={that.hideSelectMenu}>
                                                <dl className="dl_down_content"  >
                                                 
                                                  <dt><span onClick={that.showMyShop}>我的订单</span></dt>
                                                 
                                                  <dt><a href="http://tiku.huatu.com/index.php?mod=administration&act=changepw&ref=Administration/index" onClick={that.changePassword}>修改密码</a></dt>
                                                  <dt><span onClick={that.handleExist}>退出</span></dt>
                                                </dl>
                                                </div>
                                                  
                                        
                                        </li>
                                        <li className="ti_user_info" style={that.props.headerInfo.isShowUserInfoDiv} onClick={that.checkMessage}>
                                                  消息
                                         
                                                </li>
                                                <li className="ti_app">
                                                  <a href="http://tiku.huatu.com/index.php?mod=user&act=anddownload" target="_blank" onMouseOver={that.mouseOnMobile}>客户端</a>
                                                  <div className="mobile" style={that.state.isShowMobileQrcode}>
                                                  <div className="left"><img src="static/images/app.jpg" width="55" height="55"/></div>
                                                    <div className="right"><p>扫一扫下载</p><p>手机客户端</p></div>
                                                  </div>
                                                </li>
                                                <li className="ti_weixin">
                                                  <a href="" onMouseOver={that.mouseOnWeixin} onClick={that.clickWeixin}>微信</a>
                                                  <div className="wx" style={that.state.isShowWeixin}>
                                                  <div className="left"><img src="static/images/new_wxma.png"/></div>
                                                  <div className="right"><p>砖题库 扫一扫</p><p>微信号:ztkgwy</p></div>
                                                  </div>
                                                 

                                                </li>
                                                <li className="ti_weibo"  onMouseOver={that.mouseOnWeibo} onMouseOut={that.mouseOutWeibo}>
                                                  <a href="http://weibo.com/htztk" target="_blank">微博</a>
                                                    <div className="wb" onMouseOver={that.mouseOnWeiboBox} style={that.state.isShowWeibo}>
                                                        <div className="left"><img src="static/images/new_indexhuatu.png"/></div>
                                                        <div className="right"><p>砖题库公务员考试</p><p><img src="static/images/new_guanzhu.png" onClick={that.weiboGuanZhu}/></p></div>
                                                  </div>
                                                </li>
                                              </ul>
                                              </div>
                                                 
                                              </div>
                                                <div className="header_tab" style={that.state.headerTab} onMouseOver={that.showHeaderTab} onMouseOut={that.hideHeaderTab} >
                                                    <div className="type_tab">
                                                         
                                                          <a href="http://tiku.huatu.com/mini/paper.php" target="_blank" onClick={that.tikuTab}>教师题库</a>
                                                     </div>
                                                </div>
                                              <div className="ti_header">
                                                        <div className="header_type">
                                                                                <div className="header_type_list clearfix">
                                                                                              <div className="logo">
                                                                                                  <img  src="static/images/tiku_logo.png"/>
                                                                                                </div>
                                                                                                <div className="nav_right">
                                                                                                  <ul className="sild_list clearfix">
                                                                                                      <li><a href="http://tiku.huatu.com">首页</a></li>
                                                                                                      <li  className="li_act"><a href="">砖商城</a></li>
                                                                                                      <li className="sild_test"><a href="">活动</a></li>
                                                                                                  </ul>
                                                                                                </div>  
                                                                                  
                                                                                 </div>                      
                                                        </div>
                                                    <div className="header_menu">
                                                       <ul className="heade_nav clearfix"  id="nav">
                                                           {that.state.nav.map(function(item,i){
                                                                 return  <li onClick={that.handleNavTab} data-navid={item.nav_id} className={item.className}>{item.name}</li>
                                                           })}
                                                       
                                                     </ul>
                                                  </div>
                                                </div>
                                              </div>
           
                                            
                                       
                                              </div>

           	        }

           });

                                         
                                           
                                            
                                                  

                                          
                                          



/*
----------------------------------------------------

头部的登陆组件，这个只是头部的点击登录按钮的逻辑， 
商品详情页组件自身包含登录代码逻辑，代码重复。(后期重构);

-----------------------------------------------------
*/

// window.components.login=React.createClass({
//       getInitialState:function(){
//             return {
//               loginLayer:{display:"none"},
//               username:{display:"none"},
//               password:{display:"none"},
//               wrongLoginInfo:{display:"none"},
//               loginSuccess:{display:"none"}
//             };
//       },
//       componentWillReceiveProps:function(nextProps){
//             this.setState({loginLayer:nextProps.loginLayer});
//       },
   
//       render:function(){
//             var that=this;
//            return   <div><div style={that.state.loginLayer} id="loginLayer"> <div className="ma_cover_bg"></div>
//                                     <div className="log_content_model">
//                                     <div className="log_left log_moedel">
//                                     <img src="static/images/ma_cancel.png" onClick={that.handleCancelBtn}/>
//                                     <div className="log_moedel_title">登录砖题库</div>
//                                     <form action="" method="post" className="form_login">
//                                     <div className="error" id="tips-username" style={that.state.username}>请输入用户名</div>
//                                     <div className="inp inp_account">
//                                     <input  type="text" className="account" placeholder="请输入题库账号/教育网账号/网校账号"  ref="username" />
                                       
//                                     </div>
//                                     <div className="error" id="tips-password" style={that.state.password}>请输入密码</div>
//                                     <div className="inp inp_pass">
//                                          <input  type="password"  className="password"  placeholder="密码" ref="password"/>
                                      
//                                     </div>
//                                      <div className="error" id="tips-username" style={that.state.wrongLoginInfo}>用户名或密码错误</div>
//                                     <div className="Cinput">
//                                         <input type="checkbox" name="keeplogin" value="1" id="checkRegister" ref="autologin" className="checkInput" title="同意" />
//                                         <div className="C-zidong"><label>下次自动登录</label></div>
//                                         <div className="C-forget"><a href="#">忘记密码？</a></div>
//                                     </div>
//                                    <div className="btn_login"><button type="submit" className="btn_re" onClick={that.handleClickLoginBtn}>登录</button></div>
//                                     <div className="Cinput">
//                                         <div className="no_account">还没有账号？立即<a href="http://tiku.huatu.com/index.php?mod=user&act=register&ref=" className="regist">注册</a></div> 
//                                     </div>
//                                     </form>
//                                     </div>
//                                    </div>
                                    
//                                    </div>
//                                           <div style={that.state.loginSuccess}>
//                                           <div className="ma_cover_bg" ></div>
//                                                <div className="ma_confirm">
//                                                 <div className="ma_confirm_txt">登录成功</div>  
//                                                 <div className="ma_confirm_btn clearfix">
//                                                   <a href="" className="ma_confirm_know fl" id="pay" onClick={that.closeLoginSuccessTip}>知道了</a>
//                                                 </div>      
//                                               </div>
//                                              </div>
//                                    </div>
//       },
//         handleClickLoginBtn:function(event){

//                          var that=this;
//                          event.preventDefault();
                       
//                           var username=that.refs.username.getDOMNode().value,password=that.refs.password.getDOMNode().value;

                           
//                          if(!username){that.setState({
//                            username:{display:"block"},
//                            wrongLoginInfo:{dislay:"none"},
//                            password:{display:"none"}
//                              });
//                           return;
//                          }else{that.setState({username:{display:"none"}})}
//                          if(!password){that.setState({
//                           password:{display:"block"},
//                           wrongLoginInfo:{dislay:"none"}
//                         });
//                           return;
//                          }else{
//                           that.setState({password:{display:"none"}})
//                         }
//                            var autologin=that.refs.autologin.getDOMNode().checked;
                                
//                           $.post("bootstrap.php?s=/user/dologin",{username:username,password:password,autologin:autologin},function(response){
                                     
                                      
//                                        if( response.message == 'Y'){
//                                            var headerInfo={
//                                                             username:response.username,
//                                                             styleClass:"ti_user_list",
//                                                             isShowUserInfoDiv:{display:"inline-block"},
//                                                             isLogin:true,
//                                                             head02:{display:"inline-block"},
//                                                             head03:{display:"inline-block"}

//                                              }
                                             
                                           
                                         
//                                     that.refs.username.getDOMNode().value='';
//                                     that.refs.password.getDOMNode().value='';
                                       
//                                  React.render(<Zhuan.header headerInfo={headerInfo}/>,document.getElementById('header'));

                                  
                                
//                                 that.setState({
//                                           loginLayer:{display:"none"},
//                                           wrongLoginInfo:{display:"none"},
//                                           username:{display:"none"},
//                                           password:{display:"none"},
//                                           loginSuccess:{display:"block"}
//                                         });  
//                                 React.render(<Zhuan.live ajaxUrl="bootstrap.php?s=/goods/index/navid/2/page/"/>,document.getElementById('content'));
                                         
//                                        }else{
//                                             that.setState({wrongLoginInfo:{display:"block"}})
                                         
//                                        }         
//                                 },'json')
//                                 },
//           handleCancelBtn:function(){
//             var that=this;
//                      that.refs.username.getDOMNode().value='';
//                      that.refs.password.getDOMNode().value='';
//                      that.setState({
//                                     loginLayer:{display:"none"},
//                                     username:{display:"none"},
//                                     password:{display:"none"},
//                                     wrongLoginInfo:{display:"none"}

//                     })
//           },
//            closeLoginSuccessTip:function(event){
//                  event.preventDefault();
//                  this.setState({loginSuccess:{display:"none"}});
//           }
//    })



                                
              
                                          
                                         

                           
                                       
                                     

                                       
                          
                       

                                      
                                               
                                      

                           

                   
         
                 
                 
         




                   



                            

                                 
                            
                            
                    
                 

                                  





