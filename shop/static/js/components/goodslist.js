
/**
 * ---------------------------------------
 *
 *    商品列表页(砖直播组件  砖图书组件  分页组件   广告组件   底部组件) 
 *    author:sunqi
 *    emil:sunqi_925@163.com
 *
 *
 * ----------------------------------------
 */    
    Zhuan.live=React.createClass({
          getInitialState:function(){ return {items:[]} },
           //回调
          getDataByPager:function(rows){
                    var that=this;
                    that.setState({items:rows});
                    
          },
          componentDidMount:function(){
                  $("#nav li").eq(0).addClass("nav_select").removeClass("nav_deault").siblings().removeClass("nav_select");
          },
        
              
         //点击跳转到购买页面
        skipToDetailPage:function(event){ 
                  var id=$(event.target).data('id');
                       var that=this;
                          $.get("bootstrap.php?s=/goods/detail/id/"+id,function(detail){ console.log(detail);
                               var tabjson=detail.attrs_list,tabArr=JSON.parse(tabjson),btn={};
                                         //是否已购买该商品
                                       $.get("bootstrap.php?s=/user/islogin",function(loginInfo){
                                             if(loginInfo=='Y'){
                                                      $.post("bootstrap.php?s=/goods/purchased",{goods_id:detail.goods_id},function(purchased){
                                                           if(purchased.message=='Y'){
                                                               btn={text:"进入教室",bgColor:{backgroundColor:"#9BC83C"},purchased:1,snapid:purchased.snapid,orderid:purchased.orderid};
                                                            }else{
                                                               btn={text:"立即参加",bgColor:{backgroundColor:"#FF8400"},purchased:0};
                                                            }
                                                    React.render(<Zhuan.detail topInfo={detail} tab={tabArr}  btn={btn} type="A"/>,document.getElementById('content')); 
                                                         },'json');
                                             }else{
                                              btn={text:"立即参加",bgColor:{backgroundColor:"#FF8400"},purchased:0};
                                  React.render(<Zhuan.detail topInfo={detail} tab={tabArr}  btn={btn} type="A"/>,document.getElementById('content')); 
                                             }
                                       });
                                },'json');
                  },
        render:function(){
            var that=this; stick=function(){ return <div className="jian">荐</div>} //是否置顶
                        
                  return     <div className="ma_container">
                                         <Zhuan.advertisement/>
                                           <div className="ma_content clearfix">
                                        {that.state.items.map(function(item,i){
                                                    return  <div className="ma_test" key={item.goods_id}>
                                                             <div className="ma_test_title">
                                                                {item.stick==1?stick():''}
                                                                <h3>{item.name}</h3>
                                                              </div>
                                                              <div className="ma_img" >
                                                                <img src={item.cover_url} onClick={that.skipToDetailPage} data-id={item.goods_id} />
                                                              </div>
                                                              <div className="ma_time clearfix">
                                                                <span className="fl">{item.startime}</span>
                                                                <span className="fr">{item.hours}课时</span>
                                                              </div>
                                                              <p title={item.introduce}>{item.introduce}</p>
                                                  <span className="ma_price" onClick={that.skipToDetailPage} data-id={item.goods_id}>{'￥'+parseFloat(item.price)}</span>
                                                            </div>
                                                                 } 
                                                               )}
                                                  <Zhuan.pager ajaxUrl={that.props.ajaxUrl} onSendDataToParent={that.getDataByPager}/>
                                              </div>
                                             </div>
                     }
                                            
                                              
                   
  });





                                  
//广告组件
Zhuan.advertisement=React.createClass({
        getInitialState:function(){
               return {
                      index:0
               }
        },
         componentDidMount:function(){
                this.autoScroll();
         },

                 


         autoScroll:function(){
              var that=this,index=that.state.index;
                    that._interval=setInterval(function(){
                          index++;
                          if(index==3){
                              index=0;
                              $("#scroll").animate({left:0},1);
                              that.setState({index:0})
                          }
                         $("#controllers li").removeClass("current").eq(index).addClass("current");
                         $("#scroll").animate({left:-978*index},1);
                         that.setState({index:index});
                    },4000);
         },
         moveNext:function(){
                                var that=this;
                                 clearInterval(that._interval);
                                 var i=this.state.index+1;
                                 if(i==3){
                                    i=0;
                                    $("#scroll").animate({left:0},1);
                                      
                                 }
                                 $("#controllers li").removeClass("current").eq(i).addClass("current");
                                 $("#scroll").animate({left:-978*i},1);
                                 this.setState({index:i})
                                 that.autoScroll();
         },
         movePrev:function(){
                                var that=this;
                                clearInterval(that._interval);
                                var i=this.state.index-1;
                                 if(i<0){
                                    i=2;
                                    $("#scroll").animate({left:-978*2},1)
                                 
                                 }
                                 $("#controllers li").removeClass("current").eq(i).addClass("current");
                                 $("#scroll").animate({left:-978*i},1);
                                 this.setState({index:i});
                                   that.autoScroll();

         },
         componentWillUnmount:function(){
                clearInterval(this._interval);
         },
         clickOnSmallController:function(event){
                 var that=this;
                 clearInterval(that._interval);
                 var i=$(event.target).data("index");
                 $("#controllers li").removeClass("current").eq(i).addClass("current");
                 $("#scroll").animate({left:-978*i},1);
                 that.setState({index:i});
                 that.autoScroll();
         },
         render:function(){
             var that=this;
              return  <div className="adbar">
                                                <ul className="scroll" id="scroll">
                                                     <li><a href="http://www.huatu.com/z/2016gkzb/" target="_blank"><img src="static/images/ad1.jpg?version=4.0"/></a></li>
                                                     <li><a href="http://v.huatu.com/cla/class_detail.php?id=48816" target="_blank"><img src="static/images/ad11.jpg?version=4.0"/></a></li>
                                                       <li><a href="http://v.huatu.com/cla/class_detail.php?id=48816" target="_blank"><img src="static/images/ad12.jpg?version=4.0"/></a></li>
                                                </ul>
                                                <ul className="controllers" id="controllers" >
                                                      <li data-index="0" className="current"  onClick={that.clickOnSmallController}></li>
                                                      <li data-index="1" onClick={that.clickOnSmallController}></li>
                                                      <li data-index="2" onClick={that.clickOnSmallController}></li>
                                                 </ul>
                                                  <div className="left-controller" onClick={that.movePrev}>&lt;</div>
                                                  <div className="right-controller" onClick={that.moveNext}>&gt;</div>
                                              
                                        </div>
         }




});









/*-----------------------------------------------------
 砖图书组件
 ------------------------------------------------------
*/
    Zhuan.book=React.createClass({
          getInitialState:function(){ return {items:[]} },
           //回调
          getDataByPager:function(rows){
                    var that=this;
                    that.setState({items:rows});
          },
          skipToDetailPage:function(event){ 
                                var id=$(event.target).data('id');
                                var that=this;
                                $.get("bootstrap.php?s=/goods/detail/id/"+id,function(detail){
                                     var tabjson=detail.attrs_list,tabArr=JSON.parse(tabjson),btn={};
                                       $.get("bootstrap.php?s=/user/islogin",function(loginInfo){
                                             if(loginInfo=='Y'){
                                                      $.post("bootstrap.php?s=/goods/purchased",{goods_id:detail.goods_id},function(purchased){
                                                           if(purchased.message=='Y'){
                                                               btn={text:"查看购买",bgColor:{backgroundColor:"#9BC83C"},purchased:1,snapid:purchased.snapid,orderid:purchased.orderid};
                                                            }else{
                                                               btn={text:"立即预定",bgColor:{backgroundColor:"#FF8400"},purchased:0};
                                                            }
                                                    React.render(<Zhuan.detail topInfo={detail} tab={tabArr}  btn={btn}/>,document.getElementById('content')); 
                                                         },'json');
                                             }else{
                                                   btn={text:"立即预定",bgColor:{backgroundColor:"#FF8400"},purchased:0};
                                    React.render(<Zhuan.detail topInfo={detail} tab={tabArr}  btn={btn} type="B"/>,document.getElementById('content')); 
                                             }
                                       });
                                 },'json');
           },
          render:function(){
                  var that=this; stick=function(){ return <div className="jian">荐</div>} //是否置顶
                    return   <div className="ma_container">
                                    <Zhuan.advertisement/>
                                    <div className="ma_content clearfix">
                                    {that.state.items.map(function(item,i){
                                                    return  <div className="ma_test" key={item.goods_id}>
                                                             <div className="ma_test_title">
                                                                 {item.stick==1?stick():''}  
                                                                  <h3>{item.name}</h3>
                                                              </div>
                                                              <div className="ma_img" >
                                                                <img src={item.cover_url} onClick={that.skipToDetailPage} data-id={item.goods_id} />
                                                              </div>
                                                              <p>{item.introduce}</p>
                                                              <a className="ma_price"  onClick={that.skipToDetailPage} data-id={item.goods_id}>{'￥'+parseFloat(item.price)}</a>
                                                              </div>
                                                                 } 
                                                               )}
                      <Zhuan.pager ajaxUrl={that.props.ajaxUrl} onSendDataToParent={that.getDataByPager}/>
                                              </div>
                                              </div>
                    }
       });
                                  
                                        
                                        
                                              
                                                         
/*
  -------------------------------------
      分页组件  
      props={
            ajaxUrl:'',//数据地址
          
      }
      onSendDataToParent();   callback 把数据传给父组件
                                
  --------------------------------------
*/


                  
  Zhuan.pager=React.createClass({
       getInitialState:function(){
                 var that=this;
                 return {
                        ajaxUrl:that.props.ajaxUrl,
                        totalPages:2,
                        currentPage:1,
                        pageBase:2,
                        showFirst:{display:"none"},
                        showPrev:{display:"none"},
                        showNext:{display:"none"},
                        showLast:{display:"none"},
                        onlyOnePage:{display:"none"}
                 }
       },
      componentDidMount:function(){
               var that=this;
               $.get(that.props.ajaxUrl+"1",function(data){
                
                   if(data.totalPages>=1){    
                      that.props.onSendDataToParent(data.rows);//父组件回调
                          var  nextState={
                                          totalPages:data.totalPages,
                                          pageBase:2,
                                          currentPage:1,
                                          showFirst:{display:"none"},
                                          showPrev:{display:"none"},
                                          showNext:{display:"inline-block"},
                                          showLast:{display:"inline-block"},
                                          onlyOnePage:{display:"inline-block"}
                                      };
                              if(data.totalPages==1){
                                       nextState.showNext={display:"none"};
                                       nextState.showPrev={display:"none"};
                                       nextState.showFirst={display:"none"};
                                       nextState.showLast={display:"none"};
                                       nextState.onlyOnePage={display:"none"};
                                       nextState.totalPage={display:"none"};
                                  }
                           that.setState(nextState);
                          $("#pager div").eq(2).addClass("current").siblings().removeClass("current");
                          $("#total span").removeClass("current");
                   }        
               },'json');
       },
                      
                          




                                        
                        
                            

      render:function(){
               var that=this,page_len=that.state.totalPages,pageArr=[],base_i=that.state.pageBase,end=base_i+5;
                    if(page_len<=7){ end=page_len; }
                    for(var i=base_i-1;i<=end;i++){  pageArr[i]=i;}
                return      <div className="ma_page" id="pager">
                                                    <div onClick={that.firstPage} style={that.state.showFirst}>首页</div>
                                                    <div onClick={that.prevPage}  style={that.state.showPrev}>上一页</div>
                                                      {pageArr.map(function(i){
                                                          return <div onClick={that.handlePage} data-page={i} style={that.state.onlyOnePage}>{i}</div>
                                                      })}
                                                    <div onClick={that.nextPage} style={that.state.showNext}>下一页</div>
                                                    <div onClick={that.lastPage} style={that.state.showLast}>末页</div>
                                                   </div>
                                                   
                             
          },
    //页码
    handlePage:function(event){
        event.preventDefault();
        var $target=$(event.target),that=this, page=$target.data("page"),
        nextState={showFirst:{display:"inline-block"},showPrev:{display:"inline-block"},showLast:{display:"inline-block"},
                     showNext:{display:"inline-block"},currentPage:page};
       $target.addClass("current").siblings().removeClass("current");
       $.get(that.props.ajaxUrl+page,function(data){
      if(page==1){nextState.showFirst={display:"none"};nextState.showPrev={display:"none"};};
      if(page==that.state.totalPages){nextState.showNext={display:"none"};nextState.showLast={display:"none"};};  
      if(page+5<=that.state.totalPages && page-1>=1){nextState.pageBase=page;
            }else if(that.state.pageBase+1+5<=that.state.totalPages && that.state.pageBase+1-1>0 && that.state.pageBase>=2 && page !=1){
                nextState.pageBase=that.state.pageBase+1;}                 
                       that.props.onSendDataToParent(data.rows);
                       that.setState(nextState);
              },'json');
                 },
    //下一页
    nextPage:function(){
         var that=this,nextPage=that.state.currentPage+1,
             nextState={showFirst:{display:"inline-block"}, showPrev:{display:"inline-block"},currentPage:nextPage};
             if(nextPage==that.state.totalPages){  nextState.showLast={display:"none"}, nextState.showNext={display:"none"} }
             if(nextPage>=7 && that.state.pageBase+1+5<=that.state.totalPages){  nextState.pageBase=that.state.pageBase+1;}
             that.setState(nextState);
             $("[data-page="+nextPage+"]", $("#pager")).addClass("current").siblings().removeClass("current");
             $.get(that.props.ajaxUrl+nextPage,function(data){ that.props.onSendDataToParent(data.rows); },'json');                 
            },
     //上一页
      prevPage:function(){
                  var that=this,prevPage=that.state.currentPage-1,
                  nextState={showLast:{display:"inline-block"}, showNext:{display:"inline-block"},currentPage:prevPage};
                  if(prevPage==1){ nextState.showFirst={display:"none"}; nextState.showPrev={display:"none"}; }
                  if( that.state.pageBase>2){ nextState.pageBase=that.state.pageBase-1;}
                  that.setState(nextState);
                  $("[data-page="+prevPage+"]", $("#pager")).addClass("current").siblings().removeClass("current");
                  $.get(that.props.ajaxUrl+prevPage,function(data){ that.props.onSendDataToParent(data.rows); },'json');
              },
      //首页
        firstPage:function(){
            var that=this;
            that.setState({currentPage:1,pageBase:2,showLast:{display:"inline-block"},showNext:{display:"inline-block"},showPrev:{display:"none"},showFirst:{display:"none"}});
                                $.get(that.props.ajaxUrl+"1",function(data){
                                       that.props.onSendDataToParent(data.rows);
                                       $("#pager div").eq(2).addClass("current").siblings().removeClass("current");
                               },'json');
            },
      //末页
        lastPage:function(){
                var that=this,lastPage=that.state.totalPages;
                  nextState={currentPage:that.state.totalPages,showFirst:{display:"inline-block"},showPrev:{display:"inline-block"},showNext:{display:"none"},showLast:{display:"none"} };
                          if(that.state.totalPages>7){nextState.pageBase=that.state.totalPages-5;} that.setState(nextState);
                                  $.get(that.props.ajaxUrl+lastPage,function(data){
                                          that.props.onSendDataToParent(data.rows);
                                          $("#pager div").eq(-3).addClass("current").siblings().removeClass("current");
                               },'json');
                    }
                       
  });          

/*-----------------------------------------------
  底部组件
-------------------------------------------------
*/
    Zhuan.footer=React.createClass({
              render:function(){
                 return   <div className="ma_footer">
                               <div className="ma_foot">
                                      <div className="foot_left">
                                        <img  src="static/images/act_foot_logo.png"/>
                                      </div>
                             
                                      <div className="foot_right">
                                        <div className="flinks">
                                          华图公务员考试网——公务员考试培训和国家公务员考试培训第一品牌
                                        </div>
                                          <div className="copyright"> 
                                            京ICP备05066753京号ICP证090387号京公网安备11010802010141电信业务审批【2009】字第233号函
                                           </div>
                                      </div>   
                                </div>
                          </div>
                 }


                 
           });


                                                  
                                                          

                                              
                                                
                                            
                                   
                                                   
                                              
                             
                             

                         
                                                              
                                                                   
                                                              
                                  

                   
               
                   
                   
                   
             
                      
                       
  

                 
               
                
                   


        
                        

               

                
                 
                 

                      


                            
                         
                             
                            
                       

                                
                            

                           


                           
  
      
     
       
              
              
              
            
                             
                                  
                                 
                           
                          

        





                            


                     
                   


                                       
                                     
                                   
                                 


                                   
                             
                             



                                         


                     
                                      
                                     
                                    
                           
                                    
                                     
                           
                                     
                        
                                       



                      
                                        
                                     
                                                    

                                   
                                       

                        
                               
              

                    
                    
                     

                       
                      
                       
                       
                    


            

                                    
                              
                                     
                                    
                                     
                               
                                         
                                         
                                      
                         
                                      
                                          
                                          
                                       
                                           
                                      
                                

                                   
                                      
                         
                                         
                                        
                                         
                                        
                                         
                                        
                                    
                                  
                               
                                     
                                 
                                   
                                               
                                      
                                  
                                     
                                   
                                                 
                                                
                                                
                                                
                                                




                                  
                               
                                  
                                   

                                       
                                               

                                   
                                  

                  


                                           
                                            





                               
                            
                             
                        
                          



                                               
                        

                              

                     
                              
                              
                            
                                    
                                     
                       
                       












                                                              
                                                             
               
               
                                                
                                            
                                   
                                                   
                                              

                             
                             
                                                         

                                                  
                                                          

                                              

                                  
                                        
                                              
                  
                   
                            
                         
                   
                   