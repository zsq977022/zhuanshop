if(!window.components) {window.components = {};}
window.components.detail = React.createClass({
      getInitialState:function(){
           var self = this;
           var hashArr= window.location.hash.split("/");
          
           // var goodsID = hashArr[hashArr.length-1]; //商品ID
           if(hashArr[0]=="#detail"){ 
                self.url = "bootstrap.php?s=/goods/detail/id/"+hashArr[hashArr.length-1];
           }else if(hashArr[0]=="#snap"){
                self.url = "bootstrap.php?s=/order/snap/id/"+hashArr[hashArr.length-1];
           }

      		 return {"attrs_list":[],
                  "btn":{"text":"立即参加","bgColor":{"backgroundColor":"#FF8400","has":"no"}}};


      },
      componentDidMount:function(){
      	    var self = this;  
            // var goodsID = self.state.goodsID;
      		// goodsID = 77;
      		//"bootstrap.php?s=/goods/detail/id/77;
      	    var url = self.url; console.log(url);
      	    $.get(url,function(body){
                    if(typeof body.attrs_list == "string"){  body.attrs_list = JSON.parse(body.attrs_list);}
                       self.setState(body); console.log(body);
                       $("#tab span").first().addClass("li_current");
                       $(".ma_left_info",$("#detail-tab")).first().show(); 
                        if(window.AUTH != undefined || window.AUTH != null){ 
                             //用户已登录开始检测用户是都已购买这件商品
                             $.get("bootstrap.php?s=/goods/purchased/id/"+body.goods_id+"/uid/"+AUTH.uid,function(result){ 
                                  console.log(result);
                                    if(result.message == "Y"){ 
                                            self.snapid = result.snapid;
                                            self.setState({"btn":{"text":"进入教室","bgColor":{"backgroundColor":"#9BC83C"},"has":"yes"}});
                                    }else{
                                         
                                    }
                            },"json");
                         }else{   
                               //未登录
                         }       
      	    },"json");
                //返回顶部按钮
            var W = $("#body").width();
            var left = (W-980)/2+980;
            var gotoTop_html = '<div id="gotoTop" title="回到顶部" style="position:fixed;width:30px;height:100px;bottom:250px;left:'+left+'px;display:none;cursor:pointer;background:url(static/images/topback.png) no-repeat;"></div>';
            $("#body").append(gotoTop_html);
            $("#gotoTop").click(function(){$('#body').animate({scrollTop:0},700); })
            $(window).scroll(function(){
                    var s=$(window).scrollTop();
                     if(s>534){ $("#tab").addClass("extension_tab");}else{$("#tab").removeClass("extension_tab");}
                    if(s>700){
                          $("#gotoTop").fadeIn(400);
                    } else {
                          $("#gotoTop").fadeOut(400);
                    }    
            })
                               
                                  
                        
                  
                   
                   
                      
                  
                      

      },
     //扩展字段的切换
     handleTab:function(event){

          $(event.target).addClass('li_current').siblings().removeClass('li_current');
          var index=$(event.target).data("index");
          $(".ma_left_info",$("#detail-tab")).hide().eq(index).show();
                           
     },
     handleClickJoin:function(){
          var self = this;
          if(window.AUTH == undefined ||   window.AUTH == null){ 
                $(body).append('<div id="loginBox"></div>');
                React.render(<window.components.loginbox />,document.getElementById("loginBox"));
                
          }else{
               if(self.state.btn.has == "yes"){
                  var goodsid = self.state.goods_id;
                  var resourceInfo = {"goodsid":goodsid,"snapid":self.snapid};
                  var data = encodeURIComponent(JSON.stringify(resourceInfo));
                  window.location.assign("#resource/"+data);
               }else{ 
                  if(parseInt(self.state.mail) == 1){
                      $.get("bootstrap.php?s=/user/getAddress/uid/"+AUTH.uid,function(data){
                          $(body).append('<div id="loginBox"></div>');  //弹窗挂载点  
                             console.log(data);                  
                          if(data.message=="Y"){
                            React.render(<window.components.verifyAddress root={self} address={data.address}/>,
                                        document.getElementById("loginBox"));   
                          }else{
                            var address={"name":"","phone":"",
                                         "province":"请选择省/直辖市",
                                         "city":"请选择城市","area":"请选择区/县"}
                            React.render(<window.components.location address={address} root={self}/>,
                                          document.getElementById("loginBox"));
                                }

                          },"json");

                     

                      
                  }else{  
                          if(self.state.price == "0.00"){  //零元
                               var postData = {"goods_id": self.state.goods_id,
                                         "price":self.state.price,
                                         "uid":AUTH.uid,
                                         "username":AUTH.username
                                         };
                                $.post("bootstrap.php?s=/order/init",postData,function(initResponse){
                                  console.log(initResponse.message);
                                   if(initResponse.message == "Y"){   

                                        var orderInfo = {"orderid":initResponse.orderid,
                                                         "goodsid":self.state.goods_id,
                                                         "mail":self.state.mail }
                                        $.post("bootstrap.php?s=/order/pay",orderInfo,function(orderResult){
                                           
                                              if(orderResult.message=='Y'){
                                                  window.location.assign("#order");
                                               }
                                        },"json");
                                   }
                               },"json");
                          }else{
                               var goodsInfo = {"goods_id":self.state.goods_id,
                                  "price":self.state.price,
                                  "name":self.state.name
                                 };
                                goodsInfo = encodeURIComponent(JSON.stringify(goodsInfo));
                               window.location.assign("#purchase/"+goodsInfo);
                          }
                  }


                   


               }
                
          }
     },
 
                  
    render:function(){ 
      	     var self = this;
      	     
                  return <div className="ma_container">
      	                   <div className="ma_top clearfix">
                        	   <div className="ma_top_img fl">
                        		    <img src={self.state.cover_url}/>
                        	   </div>
                        	{["nav"].map(function(nav){
                                  var nav = parseInt(self.state.nav);
                                  if(nav == 2){
                                  	   return <div className="ma_message fl">
            				                                 <h3>{self.state.name}</h3>
            				                                 <p>授课老师：{self.state.author}</p>
            				                                 <p>课时：{self.state.hours}课时</p>
            				                                 <p>有效期：{self.state.startime}</p>
            				                                 <p>限招人数：{self.state.quota_num==0?'不限':self.state.quota_num+"人"}</p>
            				                                 <p>已参加人数：{parseInt(self.state.goods_num)+parseInt(self.state.base_number)}人</p>
            				                                 <div>￥{parseFloat(self.state.price)}</div>

            				                                 <a className="ma_join" onClick={self.handleClickJoin} style={self.state.btn.bgColor} >{self.state.btn.text}</a>
				                                        </div>

                                  }else if(nav == 1){
                                    	return    <div className="ma_message fl">
                                                     <h3>{self.state.name}</h3>
            				                                 <p>限购：{self.state.quota_num==0?'不限':self.state.quota_num+"本"}</p>
            				                                 <p>销量：{parseInt(self.state.goods_num)+parseInt(self.state.base_number)}本</p>
            				                                 <div>￥{parseFloat(self.state.price)}</div>
            				                                 <a className="ma_join"  onClick={self.handleClickJoin} style={self.state.btn.bgColor} >{self.state.btn.text}</a>
				                                        </div>
                               
                                  }
                        	})}
                         </div>
                        			
                        <div className="ma_bottom clearfix">
                        	<div className="ma_left fl" id="detail-tab">
                        		<div className="ma_left_tab" id="tab">
                                 {self.state.attrs_list.map(function(item,i){
                                        return <span onClick={self.handleTab} data-index={i} key={i}>{item.title}</span>
                                 })}
                                     
                        		</div>
                                 {self.state.attrs_list.map(function(item,i){
                                        return <div key={i} className="ma_left_info" style={{"display":"none"}} dangerouslySetInnerHTML={{__html:item.content}}></div>
                                 })}
                                      
                                  
                                
                        	 </div>
                        	  <div className="ma_right fr">
                        				<div className="ma_right_top"><a href="http://v.huatu.com/cla/class_detail.php?id=48816" target="_blank"><img src="static/images/ad13.jpg"/></a></div>
                        				<div className="ma_right_bottom"><a href="http://v.huatu.com/cla/class_detail.php?id=48758" target="_blank"><img src="static/images/ad14.jpg"/></a></div>
                        	  </div>
                        </div>
                      </div>

      }

})



