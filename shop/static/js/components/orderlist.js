/**
 * ---------------------------------------
 *
 *    用户的购买记录页面
 *    author:sunqi
 *    emil:sunqi_925@163.com
 *
 *
 * ----------------------------------------
 */    
            Zhuan.orderlist=React.createClass({

                       //初始化state   
                      getInitialState:function(){
                            return {
                                    course:[], //列表数据
                                    pages:1,   //总页数
                                    pageBase:2,  //分页码的显示基数
                                    courseType:'all', //课程类型
                                    showNext:{display:"none"},//分页按钮显示控制，下同
                                    showLast:{display:"none"},
                                    showFirst:{display:"none"},
                                    showPrev:{display:"none"},
                                    onlyOnePage:{display:"none"}
                            };
                      },
                      //组件首次挂载触发,加载已购买的所有课程 ,是否有购买记录   (这个地方有分页，所以每一次Tab切换都是一次ajax请求)
                      componentDidMount:function(){
                                  var that=this;
                                    $("#nav li").removeClass("nav_select");//去掉导航颜色
                                  $.get("bootstrap.php?s=/order/hasCourse",function(result){ 
                                       if(result=='Y'){
                                             that.ajaxTab("all");
                                       }else{
                                          that.render=that.renderNoShopRecord;
                                          that.setState({});
                                      }
                             })
                      },
                      //遍历组装需要的数据
                      traverse:function(dataArr){
                                    var expectedArr=[];
                                   if($.isArray(dataArr)){
                                       expectedArr=dataArr;
                                 }else{
                                       expectedArr=[];
                                 }
                                   return expectedArr;
                      },
                      //没有购买记录，点击去砖直播首页列表
                      goToShop:function(){
                              React.render(<Zhuan.live ajaxUrl="bootstrap.php?s=/goods/index/navid/2/page/"/>,document.getElementById('content'));
                      },
                      //Tab切换 ajax加载数据
                      ajaxTab:function(type){
                              var that=this;
                               $.get("bootstrap.php?s=/order/course/type/"+type,function(res){
                                        
                                         that._course= that.traverse(res.orders);

                                         that.setState({
                                                    course:that._course,
                                                    pages:res.totalPages,
                                                    currentPage:1,
                                                    showFirst:{display:"none"},
                                                    showPrev:{display:"none"},
                                                    showNext:{display:"inline-block"},
                                                    showLast:{display:"inline-block"},
                                                    onlyOnePage:{display:"inline-block"},
                                                    courseType:type
                                                  
                                                 });
                                                  
                                         if(res.totalPages==1){
                                             that.setState({
                                                     showFirst:{display:"none"},
                                                     showPrev:{display:"none"},
                                                     showNext:{display:"none"},
                                                     showLast:{display:"none"},
                                                     onlyOnePage:{display:"none"}
                                                    
                                             })
                                         }
                                            $("#pager div").eq(2).addClass("current").siblings().removeClass("current");
                                            $("#total span").removeClass("current");
                                   
                                 },'json');
                              
                                        
                      },
                   //Tab切换
                    handleTab:function(event){ 
                        var that=this,$target=$(event.target),type=$target.data("value");
                            if(type==="resource"){
                                 that.setState({
                                      course:[],
                                      showFirst:{display:"none"},
                                      showPrev:{display:"none"},
                                      showNext:{display:"none"},
                                      showLast:{display:"none"},
                                      onlyOnePage:{display:"none"},
                                     


                                 })
                            }
                                   switch(type){
                                         case "all":
                                         that.ajaxTab('all');
                                         break;
                                          case "free":
                                         that.ajaxTab('free');
                                         break;
                                          case "charge":
                                         that.ajaxTab('charge');
                                         break;
                                      


                                   }
                                    $target.addClass("li_current").siblings().removeClass("li_current");
                                  
                                     

                                
                                
                        },
                        
                      //分页处理  （这个没用分页组件，无法组合起来，研究过再做，所以这个组件自身包含分页的代码逻辑                    
                      handlePage:function(event){
                                event.preventDefault();
                                event.target.className="current";
                                var $target=$(event.target),that=this, page=$target.data("page");
                                $target.siblings().removeClass("current");
                                  if(page==1){
                                                that.setState({
                                                  showFirst:{display:"none"},
                                                  showPrev:{display:"none"},
                                                  showLast:{display:"inline-block"},
                                                  showNext:{display:"inline-block"}
                                                 } )
                                                 
                                                }else{
                                                    that.setState({
                                                    showFirst:{display:"inline-block"},
                                                    showPrev:{display:"inline-block"},
                                                    showLast:{display:"inline-block"},
                                                    showNext:{display:"inline-block"}
                                                  })
                                            }
                                              if(page==that.state.pages){that.setState({showNext:{display:"none"},showLast:{display:"none"}})}

                                   if(page+5<=that.state.pages && page-1>=1){
                                        that.setState({pageBase:page})
                                      }else if(that.state.pageBase+1+5<=that.state.pages && that.state.pageBase+1-1>0 && that.state.pageBase>=2 && page !=1){
                                           that.setState({pageBase:that.state.pageBase+1});
                                      }
                    
                               that.ajaxReturn(page,that.state.courseType);
                               that.setState({currentPage:page});
                   },

                  nextPage:function(){
                          var that=this,nextPage=that.state.currentPage+1;

                           if(nextPage>1 && nextPage<that.state.pages){
                              that.setState({
                                showFirst:{display:"inline-block"},
                                showPrev:{display:"inline-block"},
                                showLast:{display:"inline-block"},
                                showNext:{display:"inline-block"}

                              });}
                              if(nextPage==that.state.pages){
                                    that.setState({
                                           showLast:{display:"none"},
                                           showNext:{display:"none"},
                                           showFirst:{display:"inline-block"},
                                           showPrev:{display:"inline-block"}

                                    })
                              }

                            if(nextPage<=that.state.pages){
                                   if(nextPage>=7 && nextPage<that.state.pages){ 
                                       if(that.state.pageBase+1+5<=that.state.pages){
                                           that.setState({pageBase:that.state.pageBase+1});
                                          }
                                        that.setState({currentPage:that.state.currentPage+1});
                                            
                                            $("[data-page="+nextPage+"]", $("#pager")).addClass("current").siblings().removeClass("current");
                                            var page=$('[data-page='+nextPage+']',$('#pager')).data("page");
                        
                                          that.ajaxReturn(page,that.state.courseType);
                                }else{
                                     that.setState({currentPage:this.state.currentPage+1});
                                         $("[data-page="+nextPage+"]", $("#pager")).addClass("current").siblings().removeClass("current");
                                         var page=$('[data-page='+nextPage+']',$('#pager')).data("page");
                                         
                             
                                    that.ajaxReturn(page,that.state.courseType);

                                }
                              
                           
                               
                           }
                               
                  },
                  prevPage:function(){
                           var that=this,prevPage=that.state.currentPage-1;
                           
                           if(prevPage>0){
                                that.setState({
                                              showLast:{display:"inline-block"},
                                              showNext:{display:"inline-block"}
                                        });
                                       if(prevPage==1){
                                           that.setState({
                                                 showFirst:{display:"none"},
                                                 showPrev:{display:"none"}
                                           });
                                       }
                                    
                                       if( that.state.pageBase>2){

                                           that.setState({pageBase:that.state.pageBase-1,currentPage:that.state.currentPage-1});
                                       }else{
                                           that.setState({currentPage:prevPage})
                                       }
                                         $("[data-page="+prevPage+"]", $("#pager")).addClass("current").siblings().removeClass("current");
                                        var page= $("[data-page="+prevPage+"]", $("#pager")).data('page');
                                
                            
                                       that.ajaxReturn(page,that.state.courseType);
                                      
                           }
                  },
                   firstPage:function(){
                                var that=this;
                               
                                  
                                    that.setState({
                                      currentPage:1,
                                      pageBase:2,
                                      showLast:{display:"inline-block"},
                                      showNext:{display:"inline-block"},
                                      showPrev:{display:"none"},
                                      showFirst:{display:"none"}
                                    
                                  });
                                    $("#pager div").eq(2).addClass("current").siblings().removeClass("current");
                               
                                    that.ajaxReturn(1,that.state.courseType);

                  },
                  lastPage:function(){
                         var that=this;
                               
                                   if(that.state.pages>7){
                                    that.setState({pageBase:that.state.pages-5})
                                   }
                                   that.setState({
                                          currentPage:that.state.pages,
                                          showFirst:{display:"inline-block"},
                                          showPrev:{display:"inline-block"},
                                          showNext:{display:"none"},
                                          showLast:{display:"none"}
                                  });
                                    $("#pager div").eq(-3).addClass("current").siblings().removeClass("current");
                                  var lastPage=that.state.pages;
                                  that.ajaxReturn(that.state.pages,that.state.courseType);
                  },
                   //分页ajax取数据
                  ajaxReturn:function(page,type){
                        var that=this;
                         $.get("bootstrap.php?s=/order/course/type/"+type+"/page/"+page,function(res){
                                      if(that.isMounted()){

                                       var data=that.traverse(res.orders);
                                               that.setState({course:data});
                                    }
                               },'json');
                  },



                 

                      
                   
                    //跳转到商品拥有的资源页面
                  viewDetail:function(event){
                    
                    var $target=$(event.target),snapid=$target.data("id"),name=$target.data("name"),goodsId=$target.data('goodsid');
                    var orderid=$target.data("orderid"),nav=$target.data('nav');
                    if(nav==2){type='A'}else if(nav==1){type='B'};
                    React.render(<Zhuan.courseDetails  goods_id={goodsId} name={name} snapid={snapid} orderid={orderid} type={type}/>,document.getElementById("content"));
                                
                            
                  },
                  //没有购买记录 UI
                  renderNoShopRecord:function(){
                            return <div className="ma_container"><div className="ma_wu">
                           <p>购买成功的课程才会出现在这里，您还没有购买任何课程呦~</p>
                           <a className="ma_look" onClick={this.goToShop}>去砖商城看看</a>
                           </div></div>
                           
                  },
                
                    
                      render:function(){
                         var that=this;
                               //分页
                                var page_len=that.state.pages;
                                var pageArr=[];
                                var base_i=that.state.pageBase;
                                var end=base_i+5;
                              if(that.state.pages<=7){
                                   end=that.state.pages;
                              }
                             
                           for(var i=base_i-1;i<=end;i++){
                                   pageArr[i]=i;
                           }
                           
                           return <div className="ma_container"><div className="ma_had_title">
                                         已购买商品
                                       </div>
                                           <div className="ma_had">
                                           <div className="ma_tab">
                                            <ul className="clearfix">
                                                <li className="li_current" onClick={that.handleTab} data-value="all" >全部</li>
                                                <li  onClick={that.handleTab} data-value="free">免费</li>
                                                <li  onClick={that.handleTab} data-value="charge">收费</li>
                                                
                                            </ul>
                                        </div>
                                        <div className="ma_class ">
                                            <ul className="clearfix">
                                               {that.state.course.map(function(v){
                                                      return    <li>
                                                    <span className="ma_class_name" onClick={that.viewDetail} data-goodsid={v.goods_id} data-id={v.snap_id} data-orderid={v.orderid} data-name={v.name} data-nav={v.nav}>
                                                           {v.name}
                                                     </span>                 
                                                   
                                                  <div className="info">
                                                    <span className="shang">有效期：{v.shangtime}-{v.endtime}</span>
                                                    <span className="teachers" title={v.author}>
                                                      授课老师：{v.author}
                                                     </span>
                                                     <span className="paytime">
                                                        {v.is_give!=1?"购买时间":"赠送时间"}：{v.order_time}
                                                    </span>
                                                    <span className="ma_check" onClick={that.viewDetail} data-id={v.snap_id} data-goodsid={v.goods_id} data-orderid={v.orderid} data-name={v.name} data-nav={v.nav}>查看详情</span>
                                                       </div>
                                                </li>
                                               })}
                                            
                                            </ul>
                                                </div>

                                                     <div className="ma_page" id="pager">
                         <div onClick={that.firstPage} style={that.state.showFirst}>首页</div>
                         <div onClick={that.prevPage}  style={that.state.showPrev}>上一页</div>
                        {pageArr.map(function(i){
                         return <div onClick={that.handlePage} data-page={i} style={that.state.onlyOnePage}>{i}</div>
                        })}
                        <div onClick={that.nextPage} style={that.state.showNext}>下一页</div>
                          <div onClick={that.lastPage} style={that.state.showLast}>末页</div>
                      

                                     </div>
                                                </div>

                                                </div>
                      }

                            
                          
                        
                         
                               
                                    
            });

                          
                             

                           
                             
    
                               
                                     
                                          
                                               
                                        
                                           


                                                 
                                

                                                 

                                             


                               
                    
