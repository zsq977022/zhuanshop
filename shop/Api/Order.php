<?php
/**
 * ---------------------------------------
 *
 *    订单处理
 *    author:sunqi
 *    emil:sunqi_925@163.com
 *
 *
 * ----------------------------------------
 */

class Order{


     /*创建已付款订单并且更新商品的销量    操作表 v_order_finally_info
     * @parameters $orderId   订单id
     *             $goods_id  商品id
     *             $pat  支付方式  int   1 支付宝   2 微信
     *             $mail      是否需要邮寄
     * @return  操作成功返回true   操作失败 false
     */
	public function create($orderId,$goods_id,$pat,$mail='N'){
                   $order_id=intval($orderId);
                   $orderInfo=DB::find("select * from v_order_init_info where order_id={$order_id}");
                   $order_number=$orderInfo['order_number'];
                   $snap_ids=$orderInfo['snap_ids'];
                   $username=$orderInfo['username'];
                   $user_id=$orderInfo['user_id'];
                   $orders_time=time();
                   $amount=$orderInfo['amount'];
                   $pattern=$pat;
                   $result=DB::query("insert into v_order_finally_info(order_id,order_number,snap_ids,username,user_id,orders_time,amount,pattern) values({$order_id},'{$order_number}','{$snap_ids}','{$username}',{$user_id},{$orders_time},{$amount},{$pattern})");
                    //删除初始订单
                   $del_init_order=DB::query("delete from v_order_init_info where order_id={$order_id}");
                   //销量自增
                   $goods_id=intval($goods_id);
                   $resultArr=DB::find("select goods_num from v_goods where goods_id={$goods_id}");
                   $goods_num=intval($resultArr['goods_num'])+1;
                   $autoInc=DB::update("update v_goods set goods_num={$goods_num} where goods_id={$goods_id}");
                   if($mail=='Y'){
                       $addressInfo=DB::find("select addresses_list from v_user_info where uid={$user_id} limit 1");
                       $addressArr=json_decode(gzuncompress(base64_decode($addressInfo['addresses_list'])),true);
                       $mail_id=DB::query("insert into v_order_mail_info(order_id,receiver,telephone,address) values({$order_id},'{$addressArr['name']}','{$addressArr['phone']}','{$addressArr['address']}')");
                       if(!$mail_id){
                           return false;
                       }
                   }
                   if($result && $autoInc){
                        return true;
                   }else{
                        return false;
                   }
	  }
      /*初始化订单    操作表  v_order_init_info
      * @parameters  void
      * @return void
      */
  public function init(){
       $uid      = $_POST["uid"];
       $username = $_POST["username"];
       $goods_id = $_POST["goods_id"];

       $snap_id  = $this->createSnap($goods_id);
       $snap_ids = "[{$snap_id}]";//快照id
       $order_number = 'httk_'.date('Ymd').str_pad(mt_rand(1,99999),5,'0',STR_PAD_LEFT);//订单编号
       $creatime     =  time();//下单时间
       $amount       =  $_POST['price']; //商品价格
       $result = DB::insert("insert into v_order_init_info(order_number,snap_ids,username,user_id,orders_time,amount) values('{$order_number}','{$snap_ids}','{$username}',{$uid},{$creatime},{$amount})");
       $returnArr = array();
       if( $result ){
           $returnArr['message']      =  'Y';
           $returnArr['orderid']      =  $result;
           $returnArr['snapid']       =  $snap_id;
           $returnArr['order_number'] = $order_number;
    }else{
           $returnArr['message'] = 'N';
        
     }
     echo json_encode($returnArr);
              
              
  }
  //是否已支付
  public function haspay(){
              $order_id=intval($_POST['order_id']);
              $orderData=DB::find("select * from v_order_finally_info where order_id=$order_id");
              if($orderData){
                        $returnArr['message']='Y';
              }else{
                        $returnArr['message']='N';
              }
              echo json_encode($returnArr);
       }
	/*
	  生成快照
    @param $id  v_goods_info 表的goods_id
    @return snap_id 
	*/
	protected function createSnap($id){
                 $id=intval($id);
	  	           $resultArr=DB::find("select * from v_goods where goods_id={$id}");
                 $snap_info=base64_encode(gzcompress(json_encode($resultArr),9));
	  	           $id=DB::insert("insert into v_snap_info(snap_info,goods_id) values('{$snap_info}',{$id})");
                 if($id){
                         $result=$id;
                     }else{
                         $result=false;
                     }
                  return $result;
	    }
  //某个用户购买的课程订单 
  public function  course(){
        	      // $auth=$_COOKIE['ht_auth'];
               //  $auth = explode("\t", uc_authcode($auth));
               //  $uid=$auth[0];
               // $uid = 7828161; 
                $uid = (int)$_GET["uid"]; 
                $type=$_GET['type'];//课程类型
                if($type=="free"){
                	  $total_sql="select count(*) as total from v_order_finally_info where user_id={$uid} and amount=0";
                }else if($type=="charge"){
                	  $total_sql="select count(*) as total from v_order_finally_info where user_id={$uid} and amount>0";
                }else if($type=="all"){
                    $total_sql="select count(*) as total from v_order_finally_info where user_id={$uid}";
                }
                //分页参数

                $currentPage=isset($_GET['page'])?intval($_GET['page']):1;
          	    $totalRowsArr=DB::find($total_sql);
          	    $totalRows=$totalRowsArr['total'];
          	   	$pageSize=9;
  			        $totalPages=ceil($totalRows/$pageSize);
  			        $offset=($currentPage-1)*$pageSize;
             
  			      if($type=="all"){
                   $sql="select order_id,is_give,snap_ids,orders_time,amount from v_order_finally_info where user_id={$uid} order by orders_time desc limit {$offset},{$pageSize}";
  			      }else if($type=="free"){
  			      	   $sql="select order_id,is_give,snap_ids,orders_time,amount from v_order_finally_info where user_id={$uid} and amount=0 order by orders_time desc limit {$offset},{$pageSize}";
  			      }else if($type=="charge"){
                   $sql="select order_id,is_give,snap_ids,orders_time,amount from v_order_finally_info where user_id={$uid} and amount>0 order by orders_time desc limit {$offset},{$pageSize}";
              }
              //根据当前用户uid取出记录
          	 $orders =  DB::select($sql);
          	 if(!empty($orders)){ 
          	    $ids='';
          	   foreach($orders as $v){
                         $str=$v['snap_ids'];
                         $id=substr($str,1,-1);
                         $ids.=','.$id;
          	              }
        	     $ids=trim($ids,','); 
              
        	     $snapArr=DB::select("select snap_info,goods_id from v_snap_info where snap_id in({$ids}) order by snap_id desc");
               //组装数据
        	     foreach($snapArr as $i=>$v){
                   $tmpDate=date('Y年n月j日 H:i:s',$orders[$i]['orders_time']); //生成订单时间
                   $tmpArr['order_time']=$tmpDate;
                   $tmpArr['orderid']=$orders[$i]['order_id'];
                   $tmpArr['snap_id']=substr($orders[$i]['snap_ids'],1,-1);//为了给用户查看商品快照用的snap_id
                   $authorStr='';
                   $authorArr=DB::select("select B.author from v_goods_relation as A  inner join v_resource as B on B.resource_id=A.resource_id  where A.goods_id={$v['goods_id']}");

                  if(!empty($authorArr)){
                                      foreach($authorArr as $a){  $teachers[]=$a['author'];  }
                                       $teachers=array_unique($teachers);
                                       $authorStr=implode(',',$teachers);
                                 }

              
                   $jsonArr=DB::find("select goods_id,name,startime,endtime,nav from v_goods where goods_id={$v['goods_id']}");
                   $tmpArr["name"]=$jsonArr['name'];
                   $tmpArr['goods_id']=$jsonArr['goods_id'];
                   $tmpArr['shangtime']=date('Y年n月j日',$jsonArr['startime']);
                   $tmpArr['endtime']=date('Y年n月j日',$jsonArr['endtime']);
                   $tmpArr['nav']=$jsonArr['nav'];
                   $tmpArr['author']=$authorStr;
                   $tmpArr['amount']=$orders[$i]['amount'];
                   $tmpArr['is_give']=$orders[$i]['is_give'];
                  $tmpArr['js']=$jsonArr;
                   $newArr[]=$tmpArr;          
        	     }
        	        $Arr['rows']=$newArr;
        	        $Arr['totalPages']=$totalPages;
                    
              }else{ //如果没有数据
                       $Arr['orders']=[];
                       $Arr['totalPages']=1;
              }
              echo json_encode($Arr);
                       

            } 

	//根据快照id取出快照
	public function  snap(){
	       	           $id=$_GET['id'];
	       	           $result=DB::find("select * from v_snap_info where snap_id={$id}");
	       	           $goodsArr=json_decode(gzuncompress(base64_decode($result['snap_info'])),true);
                     $tmpArr=array();
                   if(!empty($goodsArr['attrs_list'])){ 
                               //商品对应的资源老师
                              $authorStr='';
                              $goods_id=$goodsArr['goods_id'];
                              $authorArr=DB::select("select B.author from v_goods_relation as A  inner join v_resource as B on B.resource_id=A.resource_id  where goods_id={$goods_id} limit 5");
                              if(!empty($authorArr)){
                                foreach($authorArr as $a){  $teachers[]=$a['author'];  }
                                $teachers=array_unique($teachers);
                                $authorStr=implode(',',$teachers);
                              }
                            $goods_attrs_list_arr=json_decode(gzuncompress(base64_decode($goodsArr['attrs_list'])),true);
                            foreach($goods_attrs_list_arr as $key=>$value){
                                      $tmpArr[]=array('title'=>$key,'content'=>$value);   
                                  }
                             $goodsArr['attrs_list']=json_encode($tmpArr); 
                    }else{
                            $tmpArr[0]=array('title'=>'','content'=>'');
                            $goodsArr['attrs_list']=json_encode($tmpArr);
                         }
                      //销量
                     $goodsInfo=DB::find("select goods_num,base_number from v_goods where goods_id={$goods_id}");
                     $ch_startime=date('Y年n月j日',$goodsArr['startime']);
                     $ch_endtime=date('Y年n月j日',$goodsArr['endtime']);
                     $goodsArr['startime']=$ch_startime.'-'.$ch_endtime;
                     $goodsArr['author']=$authorStr;
                     $goodsArr['goods_num']=$goodsInfo['goods_num'];
                     $goodsArr['base_number']=$goodsInfo['base_number'];
                     echo json_encode($goodsArr);
	       } 
  //验证用户是否有购买记录
  public function  hasCourse(){
                // $auth=$_COOKIE['ht_auth'];
                // $auth = explode("\t", uc_authcode($auth));
                $uid=(int)$_GET['uid'];
                $result=DB::find("select order_id from v_order_finally_info where user_id={$uid}");
                if($result){
                      echo 'Y';
                }else{
                      echo 'N';
                }
            }
  //零元自动走支付流程
  public function pay(){
                   $order_id=intval($_POST['orderid']);
                   $orderInfo=DB::find("select * from v_order_init_info where order_id={$order_id}");
                   $order_number=$orderInfo['order_number'];
                   $snap_ids=$orderInfo['snap_ids'];
                   $username=$orderInfo['username'];
                   $user_id=$orderInfo['user_id'];
                   $orders_time=time();
                   $amount=$orderInfo['amount'];
                   $pattern=2;
                   $finally_order=DB::query("insert into v_order_finally_info(order_id,order_number,snap_ids,username,user_id,orders_time,amount,pattern) values({$order_id},'{$order_number}','{$snap_ids}','{$username}',{$user_id},{$orders_time},{$amount},{$pattern})");
                    //删除初始化订单
                    $del_init_order=DB::query("delete from v_order_init_info where order_id={$order_id}");
                   //销量自增
                   $goods_id=intval($_POST['goodsid']);
                   $resultArr=DB::find("select goods_num from v_goods where goods_id={$goods_id}");
                   $goods_num=intval($resultArr['goods_num'])+1;
                   $autoInc=DB::update("update v_goods set goods_num={$goods_num} where goods_id={$goods_id}");
                   $mail=$_POST['mail'];
                   if($mail=='Y'){
                       $addressInfo=DB::find("select addresses_list from v_user_info where uid={$user_id} limit 1");
                       $addressArr=json_decode(gzuncompress(base64_decode($addressInfo['addresses_list'])),true);
                       if(isset($addressArr['area'])){
                              $province=$addressArr['province'].$addressArr['city'].$addressArr['area'].$addressArr['street'];
                       }else{
                              $province=$addressArr['province'].$addressArr['city'].$addressArr['street'];
                       }
                       $receiver=$addressArr['name'];
                       $telephone=$addressArr['phone'];
                      
                       $mail_id=DB::query("insert into v_order_mail_info(order_id,receiver,telephone,address) values({$order_id},'{$receiver}','{$telephone}','{$province}')");
                       if(!$mail_id){
                                 $returnArr['message']=$mail_id;
                                 echo json_encode($returnArr);
                                 return false;
                           }
                       }
                       if($finally_order && $autoInc){
                               $returnArr['message']='Y';
                       }else{
                               $returnArr['message']='N';
                       }
                       echo json_encode($returnArr);

    }  
  //查看物流
  public function wuliu(){
            $orderid = $_GET['orderid']; 
            $wuliu   = DB::find("select B.company_name,A.company_num  from  v_order_mail_info as A inner join v_company_info as B on A.company_id=B.company_id where A.order_id={$orderid} and A.mail_status=1");
            
            echo json_encode($wuliu);
    }  


 //生成订单扫码支付微信和支付宝的二维码字符串
  public function qrcode(){
//      $f = fopen('./result.log',"w+");
//      fwrite($f,'<h1>doxas</h1>');
//      fclose($f);exit();
        /******************测试数据***********************/
        // $_POST['order_number'] = 'http_'.date('YmdHis');
        // $_POST['total_fee'] = '1.00';
        // $_POST['body'] = '模考大同';
        // $_POST['order_id'] = '32';
        // $_POST['product_id'] = '78';
        // $_POST['mail'] = '1';
       /******************测试数据***********************/


        //获取微信扫码支付二维码码串
        $weixin_obj = new Weixinqrpay();
        $weixinqr_string = $weixin_obj->createQrcode();
        $weixinqr_arr = json_decode($weixinqr_string,true);
        // echo json_encode($weixinqr_arr);exit();
        if(isset($weixinqr_arr['result_code']) && $weixinqr_arr['result_code'] == 'SUCCESS')
        {
                $qrcode['weixin'] = $weixinqr_arr['code_url'];
        }else
        {
                $qrcode['weixin'] = '';   //出错返回空 ,页面上不会生成二维码
                 //返回了错误码，记录到业务错误日志中
                  $weixinqr_error_msg = '[微信扫码支付错误码]  '.$weixinqr_string;
                // writeLog($weixinqr_error_msg,LOG_PATH.'/'.date('Ymd').'.log');
        }
        //获取支付宝扫码支付二维码码串
        //组装支付宝扫码支付生成二维码字符串所需参数
        $zfb_parameters['out_trade_no'] = $_POST['order_number'];  //本地商城系统的订单号
        $zfb_parameters['total_amount'] = $_POST['total_fee'];     //订单金额
    
        $zfb_parameters['subject'] = $_POST['body'];               //订单标题
        $zfb_parameters['body'] = $_POST['order_id']."##".$_POST['product_id']."##".$_POST['mail']; //订单入库需要的数据(回传过来)
        $zfb_obj = new Zhifubaoqrpay();
        $zfbqr_string = $zfb_obj->createQrcodeString($zfb_parameters);
        // echo $zfbqr_string;exit();
        $zfbqr_arr = json_decode($zfbqr_string,true);
        if($zfbqr_arr['alipay_trade_precreate_response']['msg'] == 'Success'){
                     $qrcode['zfb'] = $zfbqr_arr['alipay_trade_precreate_response']['qr_code'];
         }else {
                     $qrcode['zfb'] = '';  //出错返回空 ,页面上不会生成二维码
                  //返回了错误码，记录到业务错误日志中
                  $zfbqr_error_msg = "[支付宝扫码错误码]    ".$zfbqr_string;
                  // writeLog($zfbqr_error_msg,LOG_PATH.'/'.date('Ymd').'.log');
                
                 
         }
         echo json_encode($qrcode);
       
       
   }

           
  }                 

       





           
    
  
   
  

    
  
  
       
       
             
                 
                        
   
          
        

    

                 

      
     
                  


                             


                            



       
                  

       
              
      

    
    


               
                              
                   
          
           



                                 
                                           
                            
                                  
                                



                           





               
          
                   

                   

          
         
           
      



       
    
                      
        

          
          








         
           
      

                              
                                        
                                     
                            



                           
                     




               
                                
           









      
                    
                  






















          
    
        
   
            
          
                 
           
            





                        
               

                 

           
            




             

     

           
                 


                     


           

               
                  
                  
                

          




                 


     

