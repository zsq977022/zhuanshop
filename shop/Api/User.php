<?php
/**
 * ---------------------------------------
 *
 *    砖商城用户  
 *    author:sunqi
 *    emil:sunqi_925@163.com
 *
 *
 * ----------------------------------------
 */
class User{
	  
    
    //用户登录 
  public function dologin(){

      if(!empty($_POST)){
     
        $account=$_POST['username'];//post过来的用户名
        $password=$_POST['password'];//post过来的密码
        $autologin=$_POST['autologin'];//是否勾选自动登录
        //首先到shop v_user_info表里验证登录，没有再到UC里面登录同时插入到v_user_info表里
          $shopUser=$this->shopUser($account,$password);
          if($shopUser){
                $userinfoArr=$shopUser;
          }else{
                $userinfoArr=$this->fetchUser($account,$password);
         }
          $returnArr=array();
          if($userinfoArr){
                      $data='';
//                      $cookie = uc_authcode($userinfoArr['uid'] . "\t" . $userinfoArr['username'] . "\t" . $data, 'ENCODE');
                      $expire=0; //cookie生存期
                      if(isset($autologin) && $autologin=='true'){
                              $expire=time()+3600*24*4;
                        }
//                        setcookie('ht_auth',$cookie,$expire,'/');
                          $cookie['username'] = $userinfoArr['username'];
                          $cookie['uid'] = $userinfoArr['uid'];
                          setcookie("ti_shop",json_encode($cookie),$expire,"/","",0);
                          $returnArr['message']='Y';
                          $returnArr['username']=$userinfoArr['username'];
                    }else{
                               $returnArr['message']='N';
                   }
                               echo json_encode($returnArr);

                   }
          }
                   
                              
  //商城本地的用户表验证
  public function shopUser($account,$password){
                if(preg_match('/^\d{11}$/',$account)){
                      $sql="select uid,username,password,salt from v_user_info where phone='{$account}'";
                }elseif(preg_match('/^\w+[@]\w+\.[a-zA-Z]{2,4}$/',$account)){
                      $sql="select uid,username,password,salt from v_user_info where email='{$account}'";
                }else{
                      $sql="select uid,username,password,salt from v_user_info where username='{$account}'";
                }
               $shopUser=DB::find($sql);
                if($shopUser &&  (md5($password.$shopUser['salt']) == $shopUser['password'] || md5(md5($password).$shopUser['salt']) == $shopUser['password']  || substr(md5($password),8,16) == $shopUser['password'])){
                       $time=time();
                       $ip=$_SERVER['REMOTE_ADDR'];
                       DB::update("update v_user_info set last_login_time={$time},last_login_ip='{$ip}' where uid={$shopUser['uid']}");
                       return $shopUser;         
                 }else{
                       return false;

               }

         }                   
    //用户退出
  public function logout(){
              setcookie('ti_shop','',time()-3600,"/","",0);
              $response = array("message"=>"Y");
              echo json_encode($response);
           
     }

//检测用户是否已登录
  public function islogin(){
    if(isset($_COOKIE['ht_auth'])){
     	  echo 'Y';
    }else{
     	  echo 'N';
    }
  }
                             
  //去UC验证用户
  protected  function fetchUser($account,$password){
                if(preg_match('/^\d{11}$/',$account)){
                      $sql="select A.userid as uid,B.username,B.password,B.salt from common_user_bd as A inner join uc_members as B on A.username=B.username where phone='{$account}' limit 1";
                }elseif(preg_match('/^\w+[@]\w+\.[a-zA-Z]{2,4}$/',$account)){
                      $sql="select uid,username,password,salt from uc_members where email='{$account}' limit 1";
                }else{
                      $sql="select uid,username,password,salt from uc_members where username='{$account}' limit 1";
                }

                    $link=mysqli_connect('123.103.79.90','v_ztk','ztk86Ytul1ppy',"ucentermain",3309);
                    $result=mysqli_query($link,$sql);
                    $userinfoArr=mysqli_fetch_assoc($result);

            if($userinfoArr &&  (md5($password.$userinfoArr['salt']) == $userinfoArr['password'] || md5(md5($password).$userinfoArr['salt']) == $userinfoArr['password']  || substr(md5($password),8,16) == $userinfoArr['password'])){
               
               if(preg_match('/^\d{11}$/',$account)){
                     $shop_sql="insert into v_user_info(uid,username,password,salt,phone) values({$userinfoArr['uid']},'{$userinfoArr['username']}','{$userinfoArr['password']}','{$userinfoArr['salt']}','{$account}')";
               }elseif(preg_match('/^\w+[@]\w+\.[a-zA-Z]{2,4}$/',$account)){
                     $shop_sql="insert into v_user_info(uid,username,password,salt,email) values({$userinfoArr['uid']},'{$userinfoArr['username']}','{$userinfoArr['password']}','{$userinfoArr['salt']}','{$account}')";
               }else{
                     $shop_sql="insert into v_user_info(uid,username,password,salt) values({$userinfoArr['uid']},'{$userinfoArr['username']}','{$userinfoArr['password']}','{$userinfoArr['salt']}')";
                    }
                        DB::insert($shop_sql);
                       return $userinfoArr;         
           }else{

                 return false;
           }
                 
                 }

//自动登录
 public function autologin(){
        $returnArr=array();
        if(isset($_COOKIE['ht_auth'])){
            $returnArr['message']='Y';
            // $tmpArr=json_decode($_COOKIE['info'],true);
          $auth=$_COOKIE['ht_auth'];
          $auth = explode("\t", uc_authcode($auth));
          $returnArr['username']=$auth[1];
        }else{
            $returnArr['message']='N';
        }
            echo json_encode($returnArr);
        }
             
            
                
     //取出用户表里已有收货地址
  public function getAddress(){
       
       $uid=$_GET['uid'];
       $resultArr=DB::find("select addresses_list from v_user_info where uid={$uid}");
       $responseArr=array();
               
        if(!is_null($resultArr['addresses_list']) && !empty($resultArr['addresses_list'])){
            $addressInfo=json_decode(gzuncompress(base64_decode($resultArr['addresses_list'])),true);
            $responseArr['message']='Y';
            $responseArr['address']=$addressInfo;
        }else{
            $responseArr['message']='N';  
        }
        echo json_encode($responseArr);
      }            
  //更新收货地址
   public function updateAddress(){
      $uid = (int)$_GET['uid'];
      $returnArr=[];
      if(!empty($_POST)){
        $newAddress=base64_encode(gzcompress(json_encode($_POST),9));
        $updateResult=DB::update("update v_user_info set addresses_list='{$newAddress}' where uid={$uid}");
            if($updateResult){
                $returnArr['message']='Y';
              }else{
                 $returnArr['message']='N';
              }
       }else{
                $returnArr['message']='N';
       } 
      echo   json_encode($returnArr);
 }

}
     



                       
           


                                


                      

                          
                           

                              
                         
                              
                             
                                
             
                      
               








                              
                           
              
                        

                
                 
                  
                       
   


  


             
           
          

     
  
    
          
         
           

      
   



  
    
                      
                                   
                         


               
                 
                   





                             
                  
                  

                  
                  
                

                     

                         


 
 
              
                 
                              
                
                              
                                 
                 
                              

                  



              
                   
             
                 

          







                        
                              




          
             


          
                    
                
                 
            
           
           
           
           
            



    




