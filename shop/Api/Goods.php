<?php
/**
 * ---------------------------------------
 *
 *    商品列表 详情  
 *    author:sunqi
 *    emil:sunqi_925@163.com
 *
 *
 * ----------------------------------------
 */


class Goods{
/*
-------------商品列表------------
*/
 public function index(){  
 	          //如果开启memcahe缓存
 	         if ( MEMCACHE_START &&  extension_loaded('memcache')) {
                  $m = Memca::getMemcacheInstance();
              }
                 

	  	    $currentPage=isset($_GET['page'])?intval($_GET['page']):1;
	  	    $navid=$_GET['navid'];
	  	    $cache = false;
	  	    if(isset($m) && $m == true){  
                $cache_key = 'nav'.strval($navid).'page'.strval($currentPage);
	  	        $cache = $m->get($cache_key);
	  	    }
	  	    if($cache){
                    $jsonStr = $cache;
                    // var_dump($m);
                  
	  	    }else{
	  	    	 //没有缓存或者缓存失效时，去数据库取数据
            $totalRowsArr = DB::find("select count(*) as total from v_goods where status=1 and tombstone=0 and online=1 and nav={$navid}");
			$totalRows = $totalRowsArr['total'];
			$pageSize = 9;
			$totalPages = ceil($totalRows/$pageSize);
			$offset = ($currentPage-1)*$pageSize;
			$dataArr = DB::select("select goods_id,name,cover_url,price,hours,introduce,startime,endtime,attrs_list,stick from v_goods where status=1 and tombstone=0 and online=1 and nav={$navid} order by stick desc,puawaytime desc limit {$offset},{$pageSize}");
			$newData = array();
			foreach($dataArr as $key=>$value){
				    $ch_startime = date('Y.n.j',$value['startime']);
				    $ch_endtime = date('Y.n.j',$value['endtime']);
				    $value['startime'] = $ch_startime.'-'.$ch_endtime;
				    $newData[]=$value;
			       }
			$jsonArr['rows']=$newData;
			$jsonArr['totalPages']=$totalPages;
			$jsonStr=json_encode($jsonArr);
			//写入缓存
			if(isset($m) && $m == true){  $m->set($cache_key,$jsonStr); }
			unset($jsonArr); //释放内存

			}
			echo $jsonStr; //返回json
	  }
                 
                  

                    
/*
-----------商品详情-------------
*/
  public function  detail(){
  	         //如果开启memcahe缓存
             if ( MEMCACHE_START &&  extension_loaded('memcache')) {
                   $m = Memca::getMemcacheInstance();
            }
                
	  	    $id=$_GET['id'];
            $cache = false;
            if(isset($m) && $m == true){
                  $cache_key = 'goodsid'.strval($id);
            	  $cache = $m->get($cache_key);
            }
            if($cache){
            	   $goodsInfo = $cache;
                  
            }else{
            	//没有缓存或者缓存失效时，去数据库取数据
            $dataArr=DB::find("select * from v_goods where goods_id={$id} limit 1");
            //组装老师的名字
            $authorStr='';
            $authorArr=DB::select("select B.author from v_goods_relation as A  inner join v_resource as B on B.resource_id=A.resource_id  where goods_id={$id} limit 5");
            if(!empty($authorArr)){
                   foreach($authorArr as $a){  $teachers[]=$a['author'];  }
                   $teachers=array_unique($teachers);
                   $authorStr=implode(',',$teachers);
            }
            $ch_startime=date('Y年n月j日',$dataArr['startime']);
	        $ch_endtime=date('Y年n月j日',$dataArr['endtime']);
	        $dataArr['startime']=$ch_startime.'-'.$ch_endtime;
	        $tmpArr=array();
	        //如果没有扩展信息
	        if(!empty($dataArr['attrs_list'])){ 
                  $goods_attrs_list_arr=json_decode(gzuncompress(base64_decode($dataArr['attrs_list'])),true);
	               foreach($goods_attrs_list_arr as $key=>$value){
                          $tmpArr[]=array('title'=>$key,'content'=>$value);   
	                           }
	                   $dataArr['attrs_list']=json_encode($tmpArr); 
               }else{
                   $tmpArr[0]=array('title'=>'','content'=>'');
                   $dataArr['attrs_list']=json_encode($tmpArr);
               }
	        // $dataArr['goods_num']=intval($dataArr['goods_num']) + intval($dataArr['base_number']);
	        $dataArr['author']=$authorStr;
            $goodsInfo=json_encode($dataArr);
            if(isset($m) && $m == true){$m->set($cache_key,$goodsInfo);}
            unset($dataArr);

           }

            echo $goodsInfo;
	        
	  }
              
                  



///*
// 商品是否已达到购买的上限
//*/
//     public function  limit(){
//		  $goods_id=intval($_POST['goods_id']);
//		  $result=DB::find("select goods_num,quota_num from v_goods where goods_id={$goods_id} limit 1");
//		  $goods_num=$result['goods_num'];
//		  $quota_num=$result['quota_num'];
//		  if($quota_num==0){
//			  $returnArr['message']='Y';
//		  }else{
//		      if($goods_num<$quota_num){
//				 $returnArr['message']='Y';
//		      }else{
//				 $returnArr['message']='N';
//		      }
//		  }
//	    echo json_encode($returnArr);
//	}
	
 /*
    用户是否已购买
 */
    public function purchased(){
    	            // $auth=$_COOKIE['ht_auth'];
                 //  $auth = explode("\t", uc_authcode($auth));
                 //  $uid=$auth[0];
                  $uid =intval($_GET["uid"]);
                  $goods_id=intval($_GET['id']);
                  $orders=DB::select("select order_id,snap_ids from v_order_finally_info where user_id={$uid}");
                  if(!empty($orders)){
                  	    $ids='';
                        foreach($orders as $v){
			                         $str=$v['snap_ids'];
			                         $id=substr($str,1,-1);
			                         $orderidAndSnapid[$id]=$v['order_id'];
			                         $ids.=','.$id;

          	            }
              	         $ids=trim($ids,','); //得到所有快照id  
              	         $goods_ids=DB::select("select snap_id,goods_id from v_snap_info where snap_id in({$ids})");
              	         $purchased=array();

              	         foreach($goods_ids as $goods){

                                 $purchased[$goods['snap_id']]=$goods['goods_id'];
              	         }
        	               $k=array_search($goods_id,$purchased);
              	         if($k){
              	         	   $returnArr["message"]='Y';
              	         	   $returnArr['snapid']=$k;
              	         	   $returnArr['orderid']=$orderidAndSnapid[$k];


              	         }else{
              	         	      $returnArr['message']='N';
              	         }
                  }else{
                  	       $returnArr['message']='N';
                  }
                   echo json_encode($returnArr);
    }
    


      

}
	  	    
	  	    	
	  	   

	  	    
				
			



				

				 
             


				  

                                     

                                                
                                        
                                           

                                      
                                     
                                                 
                                     
	                 
                                


			 	    

              
	      
	       
	        
	       
      