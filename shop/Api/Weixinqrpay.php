<?php
/**
 * ---------------------------------------
 *
 *    调用微信支付接口
 *    author:sunqi
 *    emil:sunqi_925@163.com
 *    
 *
 * ----------------------------------------
 */
class Weixinqrpay {

      const APPID = 'wx4e6e2dc624081b58';
	  const MCHID = '1243409802';
	  const KEY = '576d54b0d04434869aea8942cf96c3c4';
      const SSLCERT_PATH='';
      const SSLKEY_PATH='';
      protected $values=array();
      
      public function createQrcode(){
		        //传给微信的回调地址
		        $script_url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		        $base_url = strstr($script_url,'bootstrap.php',true);
		        $notify_url = $base_url.'weixin_notify.php';
//echo $notify_url;exit();
//		        $notify_url = 'http://123.103.86.69:81/weixin_notify.php';

		        $url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
      	        $total_fee=$_POST['total_fee'];  //订单金额
				$total_fee_str=strval($total_fee);
				$totalArr=explode('.',$total_fee_str);
				$total_fee=intval(implode('',$totalArr));
                
				$this->SetAppid(self::APPID);
				$this->SetMch_id(self::MCHID);
				$this->SetSpbill_create_ip($_SERVER['REMOTE_ADDR']);//终端ip
      	        $this->SetBody($_POST['body']);//设置商品的简要描述
				$this->SetAttach($_POST['order_id']."##".$_POST['product_id']."##".$_POST['mail']);
				$this->SetOut_trade_no($_POST['order_number']);  //商户系统订单号
				$this->SetTotal_fee($total_fee);
				$this->SetTime_start(date("YmdHis"));
				$this->SetTime_expire(date("YmdHis", time() + 7200));
				$this->SetGoods_tag("zhuantiku");
				$this->SetNotify_url($notify_url);
				$this->setNonce_str($this->getNonceStr());
				$this->SetTrade_type("NATIVE");
				$this->SetProduct_id($_POST['product_id']);
				$this->setSign();//设置签名
				$xml = $this->ToXml($this->values);
				$timeOut=self::getMillisecond();
				$response = $this->postXmlCurl($xml, $url, false, $timeOut);
				$result=$this->FromXml($response);
			    return json_encode($result);
				


      }
      //回调
       public function notify(){



           $xml = $GLOBALS['HTTP_RAW_POST_DATA'];

//		   		   file_put_contents('result.log',$xml);
//		          $f = fopen('./result.log',"w+");
//		              fwrite($f,$xml);
//		              fclose($f);

       	   $result = $this->FromXml($xml);
       	   if($result['return_code']=='SUCCESS'){
              $sign = $this->MakeSign($result);//生成签名
			  if($result['sign']!=$sign){   //验证签名
				  $replayNotify['return_code']='FAIL';
				  $replayNotify['return_msg']='签名失败';
			  }else{
				  if($result['result_code'] == 'SUCCESS'){
				            //写入已支付订单到数据库表v_order_finally_info
			                $orderObj=new Order;
			                list($order_id,$goods_id,$mail)=explode("##",$result['attach'],3);
			                $order=$orderObj->create($order_id,$goods_id,2,$mail);
						    if($order){
								      $replayNotify["return_code"]='SUCCESS';
						       	      $replayNotify['return_msg']='OK'; 
						     }
				          
				  }else{
                        //异步通知返回错误码,写入微信业务错误日志文件
//                  	    writeLog($xml,WEIXIN_LOG_PATH.'/'.date('Ymd').'.log');
					}
			  }
			  	$xml=$this->ToXml($replayNotify);
				echo $xml;
			  	

		   }
			  	  


				       	          
       	    
       }
       	   

				                   
       //生成微信二维码
       public function  qrcode(){
                 require_once   VENDOR_PATH.'/phpqrcode.php';
                 $url = urldecode($_GET["data"]);
                 QRcode::png($url,false,QR_ECLEVEL_L,5,1);
       }
      

       //设置随机字符串
      public  function getNonceStr($length = 32) 
	  {
		$chars = "abcdefghijklmnopqrstuvwxyz0123456789";  
		$str ="";
		for ( $i = 0; $i < $length; $i++ )  {  
			$str .= substr($chars, mt_rand(0, strlen($chars)-1), 1);  
		} 
		return $str;
	  }

	  /**
	 * 获取毫秒级别的时间戳
	 */
	private static function getMillisecond()
	{
		//获取毫秒的时间戳
		$time = explode ( " ", microtime () );
		$time = $time[1] . ($time[0] * 1000);
		$time2 = explode( ".", $time );
		$time = $time2[0];
		return $time;
	}

	/**
	 * 以post方式提交xml到对应的接口url
	 * 
	 * @param string $xml  需要post的xml数据
	 * @param string $url  url
	 * @param bool $useCert 是否需要证书，默认不需要
	 * @param int $second   url执行超时时间，默认30s
	 * @throws WxPayException
	 */
	private  function postXmlCurl($xml, $url, $useCert = false, $second = 30)
	{		
		$ch = curl_init();
		//设置超时
		curl_setopt($ch, CURLOPT_TIMEOUT, $second);
		
		
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);//严格校验
		//设置header
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		//要求结果为字符串且输出到屏幕上
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    
		if($useCert == true){
			//设置证书
			//使用证书：cert 与 key 分别属于两个.pem文件
			curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');
			curl_setopt($ch,CURLOPT_SSLCERT, self::SSLCERT_PATH);
			curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
			curl_setopt($ch,CURLOPT_SSLKEY, self::SSLKEY_PATH);
		}
		//post提交方式
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		//运行curl
		$data = curl_exec($ch);
		//返回结果
		if($data){
			curl_close($ch);
			return $data;
		} else { 
			$error = curl_errno($ch);
			curl_close($ch);
			throw new Exception("curl出错，错误码:$error");
		}
	}


          /**
	* 设置签名，详见签名生成算法
	* @param string $value 
	**/
	public function SetSign()
	{
		$valueArr=$this->values;
		$sign = $this->MakeSign($valueArr);
		$this->values['sign'] = $sign;
		return $sign;
	}
	
	/**
	* 获取签名，详见签名生成算法的值
	* @return 值
	**/
	public function GetSign()
	{
		return $this->values['sign'];
	}
	
	/**
	* 判断签名，详见签名生成算法是否存在
	* @return true 或 false
	**/
	public function IsSignSet()
	{
		return array_key_exists('sign', $this->values);
	}

	/**
	 * 输出xml字符
	 * @throws WxPayException
	**/
	public function ToXml($values)
	{
		if(!is_array($values) 
			|| count($values) <= 0)
		{
    		throw new Exception("数组数据异常！");
    	}
    	
    	$xml = "<xml>";
    	foreach ($values as $key=>$val)
    	{
    		if (is_numeric($val)){
    			$xml.="<".$key.">".$val."</".$key.">";
    		}else{
    			$xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
    		}
        }
        $xml.="</xml>";
        return $xml; 
	}
	
    /**
     * 将xml转为array
     * @param string $xml
     * @throws WxPayException
     */
	public function FromXml($xml)
	{	
		if(!$xml){
			throw new Exception("xml数据异常！");
		}
        //将XML转为array
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $values = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);		
		return $values;
	}
	
	/**
	 * 格式化参数格式化成url参数
	 */
	public function ToUrlParams($values)
	{
		$buff = "";
		foreach ($values as $k => $v)
		{
			if($k != "sign" && $v != "" && !is_array($v)){
				$buff .= $k . "=" . $v . "&";
			}
		}
		
		$buff = trim($buff, "&");
		return $buff;
	}
	
	/**
	 * 生成签名
	 * @return 签名，本函数不覆盖sign成员变量，如要设置签名需要调用SetSign方法赋值
	 */
	public function MakeSign($values)
	{
		//签名步骤一：按字典序排序参数
		ksort($values);
		$string = $this->ToUrlParams($values);
		//签名步骤二：在string后加入KEY
		$string = $string . "&key=".self::KEY;
		//签名步骤三：MD5加密
		$string = md5($string);
		//签名步骤四：所有字符转为大写
		$result = strtoupper($string);
		return $result;
	}
	
	/**
	 * 获取设置的值
	 */
	public function GetValues()
	{
		return $this->values;
	}
	/**
	* 设置微信分配的公众账号ID
	* @param string $value 
	**/
	public function SetAppid($value)
	{
		$this->values['appid'] = $value;
	}
	
	


	/**
	* 设置微信支付分配的商户号
	* @param string $value 
	**/
	public function SetMch_id($value)
	{
		$this->values['mch_id'] = $value;
	}
	


	/**
	* 设置微信支付分配的终端设备号，商户自定义
	* @param string $value 
	**/
	public function SetDevice_info($value)
	{
		$this->values['device_info'] = $value;
	}
	


	/**
	* 设置随机字符串，不长于32位。推荐随机数生成算法
	* @param string $value 
	**/
	public function SetNonce_str($value)
	{
		$this->values['nonce_str'] = $value;
	}
	
	

	/**
	* 设置商品或支付单简要描述
	* @param string $value 
	**/
	public function SetBody($value)
	{
		$this->values['body'] = $value;
	}
	


	/**
	* 设置商品名称明细列表
	* @param string $value 
	**/
	public function SetDetail($value)
	{
		$this->values['detail'] = $value;
	}
	


	/**
	* 设置附加数据，在查询API和支付通知中原样返回，该字段主要用于商户携带订单的自定义数据
	* @param string $value 
	**/
	public function SetAttach($value)
	{
		$this->values['attach'] = $value;
	}



	/**
	* 设置商户系统内部的订单号,32个字符内、可包含字母, 其他说明见商户订单号
	* @param string $value 
	**/
	public function SetOut_trade_no($value)
	{
		$this->values['out_trade_no'] = $value;
	}
	


	/**
	* 设置符合ISO 4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
	* @param string $value 
	**/
	public function SetFee_type($value)
	{
		$this->values['fee_type'] = $value;
	}



	/**
	* 设置订单总金额，只能为整数，详见支付金额
	* @param string $value 
	**/
	public function SetTotal_fee($value)
	{
		$this->values['total_fee'] = $value;
	}
	


	/**
	* 设置APP和网页支付提交用户端ip，Native支付填调用微信支付API的机器IP。
	* @param string $value 
	**/
	public function SetSpbill_create_ip($value)
	{
		$this->values['spbill_create_ip'] = $value;
	}
	


	/**
	* 设置订单生成时间，格式为yyyyMMddHHmmss，如2009年12月25日9点10分10秒表示为20091225091010。其他详见时间规则
	* @param string $value 
	**/
	public function SetTime_start($value)
	{
		$this->values['time_start'] = $value;
	}
	


	/**
	* 设置订单失效时间，格式为yyyyMMddHHmmss，如2009年12月27日9点10分10秒表示为20091227091010。其他详见时间规则
	* @param string $value 
	**/
	public function SetTime_expire($value)
	{
		$this->values['time_expire'] = $value;
	}
	


	/**
	* 设置商品标记，代金券或立减优惠功能的参数，说明详见代金券或立减优惠
	* @param string $value 
	**/
	public function SetGoods_tag($value)
	{
		$this->values['goods_tag'] = $value;
	}
	

	/**
	* 设置接收微信支付异步通知回调地址
	* @param string $value 
	**/
	public function SetNotify_url($value)
	{
		$this->values['notify_url'] = $value;
	}
	


	/**
	* 设置取值如下：JSAPI，NATIVE，APP，详细说明见参数规定
	* @param string $value 
	**/
	public function SetTrade_type($value)
	{
		$this->values['trade_type'] = $value;
	}
	


	/**
	* 设置trade_type=NATIVE，此参数必传。此id为二维码中包含的商品ID，商户自行定义。
	* @param string $value 
	**/
	public function SetProduct_id($value)
	{
		$this->values['product_id'] = $value;
	}
	


	/**
	* 设置trade_type=JSAPI，此参数必传，用户在商户appid下的唯一标识。下单前需要调用【网页授权获取用户信息】接口获取到用户的Openid。 
	* @param string $value 
	**/
	public function SetOpenid($value)
	{
		$this->values['openid'] = $value;
	}
	
	
	   
	 
}
				       	         


               
       	     
       	   
                     
       	            
       	            

       	          	
       	 

       	     
       	       




