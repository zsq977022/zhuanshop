<?php
/**
 * ---------------------------------------
 *
 *    调用支付宝接口
 *    author:sunqi
 *    emil:sunqi_925@163.com
 *
 *
 * ----------------------------------------
 */
class Zhifubaoqrpay {
	  //配置
	  //合作身份者id，以2088开头的16位纯数字
	   protected $partner = '2088912716783062';
     //收款支付宝账号
	  // protected $seller_email = 'zhuantiku@163.com';
	 //安全校验码
	  // protected $key = 'hlmagkoxjd0nh6jtba04y8npq8d697ln';
	 //异步通知页面地址
	  protected $notify_url = "http://123.103.86.69:81/zfb_notify.php";
     //页面跳转同步通知页面路径
      protected $return_url  = '';     

      /**
	 * 生成签名
	 */
	public function makeSign($values) 
	  { 
	   
	    while (list ($key, $val) = each ($values)) {
		    if($key == 'sign'  ||  $val == ""){
			      continue;
		     }else{
			      $para_filter[$key] = $values[$key];
		     }
	     }

		ksort($para_filter);    //排序
		$buff = "";
		foreach ($para_filter as $k => $v)
		{
			
	          $buff .= $k . "=" . $v . "&";
		}
			
		//去掉首尾的多余字符
		$buff = trim($buff, "&");
		// echo $buff;exit();
		//生成签名
		$rsaPrivateKey_path = KEY_PATH.'/rsa_private_key.pem';
		/* 获取私钥PEM文件内容，$rsaPrivateKey是指向私钥PEM文件的路径 */
		$prikey = file_get_contents($rsaPrivateKey_path);
		/* 从PEM文件中提取私钥 */
		$res = openssl_get_privatekey($prikey);
		/* 对数据进行签名 */
		openssl_sign($buff,$sign,$res);

        // $pubkey = __DIR__.'/../Key/rsa_public_key.pem';
        // $pubkey_path = file_get_contents($pubkey);
        // $pubkey_id = openssl_get_publickey($pubkey_path);
        // var_dump($pubkey_id);exit();
        // $check = openssl_verify($buff,$sign,$pubkey_id);
        // var_dump($check);exit();

		//释放资源
		openssl_free_key($res); 
		/* 对签名进行Base64编码，变为可读的字符串 */
		$mysign = base64_encode($sign);
		// $mysign = md5($buff); 
		//签名结果与签名方式加入请求提交参数组中
		$para_filter['sign'] = $mysign;
		return $para_filter;
		
	
		

      
	}
	//把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串，并对字符串做urlencode编码
	 public function  paramsToString($params){
        $arg = ''; 
        foreach($params as $k=>$v){
		  	   $arg.=$k.'='.urlencode($v).'&';
		  	    // $paramsUrlencode[$k] = urlencode($v);
		  }
		$arg = trim($arg,'&');
		return $arg;
	 }

	/*
	   调用支付宝扫码支付预下单接口 获取支付宝二维码字符串
	 */
	public function  createQrcodeString($params){
		 
	   //支付宝网关地址（新）
	    $alipay_gateway_new = 'https://openapi.alipay.com/gateway.do';
        
        //业务数据
        $biz_content = array(
                            "out_trade_no"	=> $params['out_trade_no'], //商户订单号
							"subject"	=> $params['subject'], //订单名称
							"total_amount"	=> $params['total_amount'], //订单总金额
							"body"	=> $params['body'], //订单描述
							'time_expire'=>date("Y-m-d H:i:s",time()+3600*2)//订单有效期


							
             	  );
        $biz_content =json_encode($biz_content);
        $timestamp = date("Y-m-d H:i:s");
		//支付宝回调地址
		$script_url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$base_url = strstr($script_url,'bootstrap.php',true);
		$notify_url = $base_url.'zfb_notify.php';

		//公共参数
	    $parameter = array(
							"app_id"    => "2015081700219364",
							"method"    => 'alipay.trade.precreate',  //接口名称 预下单接口
                            "charset"   => 'utf-8',    //编码字符集
                            "sign_type"=> 'RSA',
							"timestamp" => $timestamp,
                            "version"  => '1.0',
                            "notify_url" =>$notify_url,
                            "biz_content" =>$biz_content  //业务数据
					);
	   //生成签名

         $postDataArr = $this->makeSign($parameter);
         // echo 1;exit();
         $postDataUrlencodedString = $this->paramsToString($postDataArr);
       
        //curl post请求 调用支付宝预下单接口
         $re = $this->curl_http_post($alipay_gateway_new,$postDataUrlencodedString);
         return $re; 
	}
		
     //调用phpqrcoe类生成支付宝二维码
       public function  qrcode(){
                 require_once   VENDOR_PATH.'/phpqrcode.php';
                 $url = urldecode($_GET["data"]);
                 QRcode::png($url,false,QR_ECLEVEL_L,5,1);
       } 
	
       
      
     
           
    
    //支付宝服务器的异步通知请求处理
	public function  notify(){

//		 $f = fopen('result.log','a');
//		 $now= date('YmdHis')."\r\n";
//		 fwrite($f,$now);fclose($f)
//		 $post = file_get_contents("php://input");
//       $log_content = json_encode($_POST);
//       DB::insert("insert into v_log_info(content) values('{$log_content}')");

//		exit();


         //验证是否是支付宝服务器的请求
		 $notify_id  = $_POST['notify_id'];  //通知校验ID
         $verify_url = 'https://mapi.alipay.com/gateway.do?service=notify_verify&partner='.$this->partner.'&notify_id='.$notify_id;
         $verify_result = $this->curl_http_get($verify_url);


         if($verify_result ){
         	//异步返回  验证签名


         	$buff= '';
            foreach($_POST as $key => $value){
         	        
                    	   if($key == 'sign' || $key == 'sign_type'){
                    	   	   continue;
                    	   }else{
                    	       $notify_params[$key] = urldecode($value);
//
                    	   }
             }
            //字典排序
            ksort($notify_params);
            //待签名字串
            foreach($notify_params as $k=>$v){
				     $buff.= $k.'='.$v.'&';
			}
			 $buff = trim($buff,'&');
//             echo $buff; exit();
			 //签名base64解码为字节码串
            $sign = base64_decode($_POST['sign']);
            //获取支付宝公钥文件内容
            $alipay_public_key_path = file_get_contents(KEY_PATH.'/alipay_rsa_public_key.pem');
            //提取支付宝公钥
            $alipay_pubkey_id = openssl_get_publickey($alipay_public_key_path);
//			 var_dump($alipay_pubkey_id);exit();
            //验证签名    
             $isSign = openssl_verify($buff,$sign,$alipay_pubkey_id);
//			 $json_isSign =  json_encode($isSign);
//			 DB::insert("insert into v_log_info(content) values('{$json_isSign}')");
//			 var_dump($isSign); exit();

            $isSign = 1;
            if($isSign == 1){
                /*************签名正确，开始业务逻辑****************/
                //原样返回的业务参数
//                $biz_content = json_decode($_POST['biz_content']);
				if(isset($_POST['trade_status'] ) && $_POST['trade_status'] == 'TRADE_SUCCESS' ){
               	     //买家付款成功 

                    $orderObj = new Order;
                   //解析订单id,   商品id  , 是否要邮寄
				   list($order_id,$goods_id,$mail) =  explode("##",$_POST['body'],3);
                    //已支付订单写入到数据库表v_order_finally_info
				   $order=$orderObj->create($order_id,$goods_id,1,$mail);

				}else{
                        //记录异步通知出错码
                        $notify_msg = json_encode($_POST);
                        writeLog($notify_msg,LOG_PATH.'/'.date('Ymd').'.log');  
                }

            } 
              //释放资源
              openssl_free_key($alipay_pubkey_id);
            }
               echo 'success';
      }	

     //即时到账接口  TODO
     public function  directPayByUser(){
   /**************************请求参数**************************/

        //支付类型
        $payment_type = "1";
        //必填，不能修改
        //服务器异步通知页面路径
        $notify_url = "http://商户网关地址/create_direct_pay_by_user-PHP-UTF-8/notify_url.php";
        //需http://格式的完整路径，不能加?id=123这类自定义参数

        //页面跳转同步通知页面路径
        $return_url = "http://商户网关地址/create_direct_pay_by_user-PHP-UTF-8/return_url.php";
        //需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/

        //商户订单号
        $out_trade_no = $_POST['WIDout_trade_no'];
        //商户网站订单系统中唯一订单号，必填

        //订单名称
        $subject = $_POST['WIDsubject'];
        //必填

        //付款金额
        $total_fee = $_POST['WIDtotal_fee'];
        //必填

        //订单描述

        $body = $_POST['WIDbody'];
        //商品展示地址
        $show_url = $_POST['WIDshow_url'];
        //需以http://开头的完整路径，例如：http://www.商户网址.com/myorder.html

        //防钓鱼时间戳
        $anti_phishing_key = "";
        //若要使用请调用类文件submit中的query_timestamp函数

        //客户端的IP地址
        $exter_invoke_ip = "";
        //非局域网的外网IP地址，如：221.0.0.1


/************************************************************/

//构造要请求的参数数组，无需改动
$parameter = array(
		"service" => "create_direct_pay_by_user",
		"partner" => trim($this->partner),

		"payment_type"	=> $payment_type,
		"notify_url"	=> $notify_url,
		"return_url"	=> $return_url,
		"out_trade_no"	=> $out_trade_no,
		"subject"	=> $subject,
		"total_fee"	=> $total_fee,
		"body"	=> $body,
	    "sign_type" =>'RSA',
		"show_url"	=> $show_url,
		"anti_phishing_key"	=> $anti_phishing_key,
		"exter_invoke_ip"	=> $exter_invoke_ip,
//		"_input_charset"	=> trim(strtolower($alipay_config['input_charset']))
);
   //
     //去除请求参数中的空值和签名参数
     $args = '';
     foreach($parameter as $key => $value){
     	    if($key == 'sign' || $key == 'sign_type' || $value == ''){
                       continue;
     	    }else{
                  $tmpArr[$key] = $value;
     	    }
         }
       //排序
       ksort($tmpArr);
       //把数组元素按照 "参数=参数值"的模式用'&'符号连接成字符串
       foreach($tmpArr as $key=>$value){
       	      $args.= $key.'='.$key.'&';
       }
       //过滤掉多余的字符
       $args = trim($args,'&');
       //生成签名 
       $sign = md5($args);
       //签名方式和签名结果提交到请求参数数组中 
       $tmpArr['sign'] = $sign;
       $tmpArr['sign_type'] ='MD5'; //必须大写 
       $url = 'https://mapi.alipay.com/gateway.do?';
       $alipay_response = $this->curl_http_post($url,$tmpArr);
       //TODO-------------------------
       
       



     }						  






               	           
               
              
              	     
              	     
             
                     	     
                     	     
                     	     
           
           
            



                  
                     
                    

                    	    
                    	   	

               
               

           
             
            

	 //curl  post请求
	 public function curl_http_post($url,$postData){
           $ch = curl_init();
		   curl_setopt($ch,CURLOPT_URL, $url);
		 
		  
		//要求结果为字符串且输出到屏幕上
		  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		  curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);  // https请求 不验证证书和hosts
          curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);
          curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
	      curl_setopt($ch,CURLOPT_SSLKEY,  __DIR__.'/rsa_private_key.pem');
		 //post提交方式
		  curl_setopt($ch, CURLOPT_POST, TRUE);
		  curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
		  $headers = array('content-type: application/x-www-form-urlencoded;charset=utf-8');
		  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		//运行curl
		$data = curl_exec($ch);
		//返回结果
		if($data){
			curl_close($ch);
			return $data;
		} else { 
			$error = curl_errno($ch);
			// var_dump($error);
			curl_close($ch);
			// throw new Exception("curl出错，错误码:$error");
		}
		  
	}
	//curl get 请求 
	public function curl_http_get($url){
		     $ch = curl_init();
		     curl_setopt($ch,CURLOPT_URL,$url);
		     curl_setopt($ch,CURLOPT_HEADER,FALSE);
		     curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);
		     $response = curl_exec($ch);
		     curl_close($ch);
		     return $response;

	}
		 




}
	
		
		
		
	    

  
							
							
						
							
						
							
							
