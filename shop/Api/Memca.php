<?php
/**
 * ---------------------------------------
 *
 *    Memcache 缓存类 
 *    author:sunqi
 *    emil:sunqi_925@163.com
 *
 *
 * ----------------------------------------
 */

class Memca {
	    public $handler = null;
	    protected $config = [];
	    private function __construct(){
                    $_instance = new Memcache;
                    $memcache_config =  include CONF_PATH.'/memcache.php';
                    $this->config = $memcache_config;
                    //连接memcache server
                    $isConnected = $_instance->connect($memcache_config['MEMCACHE_HOST'],$memcache_config['MEMCACHE_PORT']);
                    if($isConnected){
                         $this->handler = $_instance ;
                        
                    }
	   }
	   //获取memcache对象
	   public static function getMemcacheInstance(){
                      $m = new Memca();
                      if($m->handler){
                      	   return $m;
                      }else{
                      	   return null;
                      }
                    	  
	   }
	   //设置缓存
	   public function set($key,$value,$expire=null){
	   	        if(!is_null($expire)){
                     $this->config['MEMCACHE_EXPIRE'] = $expire;
	   	        }

	   	        $this->handler->set($this->config['MEMCACHE_PREFIX'].$key,$value,0,$this->config['MEMCACHE_EXPIRE']);
	   }
	   //获取缓存
	   public function get($key){
	   	      return $this->handler->get($this->config['MEMCACHE_PREFIX'].$key);
	   }
	  

}